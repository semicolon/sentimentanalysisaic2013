# git Repository
https://bitbucket.org/semicolon/sentimentanalysisaic2013.git
 
# Basic operation system
Ubuntu 13.04

# Required software
git: sudo apt-get install git
Java 7 JDK: sudo apt-get install openjdk-7-jdk
Apache Maven 3: sudo apt-get install maven
	Maven is in the Universe repository under Ubuntu, which is not enabled by default.
	See https://help.ubuntu.com/community/Repositories/Ubuntu for more information.
	Don't forget to "sudo apt-get update" after enabling Universe.
Tomcat 7: sudo apt-get install tomcat7

# Setup instructions
1. Clone the git repository and change into the folder
	git clone https://bitbucket.org/semicolon/sentimentanalysisaic2013.git
	cd sentimentanalysisaic2013
2. If are not running under Ubuntu/Tomcat you might need to change the property "ConnectionString" in src/main/resources/objectdb.properties 
	to a writeable file (e.g. c:/temp/twitter.odb)
3. Inside the git repository, execute:
	mvn package
4. Copy the aic13-1.0.0-BUILD-SNAPSHOT.war from the 'target' folder to /var/lib/tomcat7/webapps (requires superuser permissions)
	sudo cp target/aic13-1.0.0-BUILD-SNAPSHOT.war /var/lib/tomcat7/webapps
5. Edit /etc/default/tomcat7, to give more heap space to the program. Change the line starting with JAVA_OPTS to for example 
JAVA_OPTS="-Djava.awt.headless=true -Xmx256m -XX:+UseConcMarkSweepGC"
	sudo nano /etc/default/tomcat7
6. (Re)Start tomcat
	sudo service tomcat7 restart

# Available features
# Demonstration GUI
	open browser and enter: http://localhost:8080/aic13-1.0.0-BUILD-SNAPSHOT/
# REST Interface available at http://localhost:8080/aic13-1.0.0-BUILD-SNAPSHOT/api/getsentiment
	Parameters: 'searchterm' and 'classifier=weka' or 'classifier=sentic'
	Example usage: http://localhost:8080/aic13-1.0.0-BUILD-SNAPSHOT/api/getsentiment?searchterm=apple&classifier=sentic
	Example JSON response: {"searchTerm":"apple","classifier":"sentic","numTweets":1000,"time":14,"val":0.5753942131996155}

