package at.ac.tuwien.infosys.aic13.util;

import static org.junit.Assert.fail;

import java.util.regex.Pattern;

import org.junit.Test;

import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.Utils;

public class TwitterStatusExclusionTest {

	private static String regex = Utils.regex;
	
	@Test
	public void testRegex() {
		
		String[] testStrings = {
				"hello", "how are you? wut?!? lol123 #yo' ", "Österreich Straße", 
				"-.- >.< o_O °.°", ".?!!![ :)) deine mama",
				"1234567890!\"§$%&/()=??ß`´+#''öüäö<<>--__+**",
				"\n \r \t\t ererer,", "@Swagger #yolo http://t.co/awesomeStuff"
				
		};
		
		for(int i = 0; i < testStrings.length; i++){
			if(!Pattern.matches(regex, testStrings[i])){
				fail("failed at string: "+testStrings[i]);
			}
		}
	}
	
}