package at.ac.tuwien.infosys.aic13.util;

import static org.junit.Assert.*;

import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.junit.Test;

public class MeanTest
{
	@Test
	public void testIncrement()
	{
		Mean mean = new Mean();
		for(int i = 0; i<5; i++)
			mean.increment(i);
		
		assertEquals(2, mean.getResult(), 0);
	}

	@Test
	public void testEvaluate()
	{
		Mean mean = new Mean();
		mean.increment(Double.MAX_VALUE);
		assertEquals(2, mean.evaluate(new double[]{0, 1, 3, 4}), 0);
	}
}
