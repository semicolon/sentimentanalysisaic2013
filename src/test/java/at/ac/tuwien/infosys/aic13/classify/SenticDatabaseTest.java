package at.ac.tuwien.infosys.aic13.classify;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Ignore;
import org.junit.Test;

public class SenticDatabaseTest {

	@Test
	public void test() {

		String searchTerm = "classy";
		LinkedList<String> sem = SenticDatabase.instance().getSemantics(searchTerm);
		
		assertTrue(sem.contains("elegant"));
		assertTrue(sem.contains("use binoculars"));
		assertTrue(sem.contains("sophisticate"));
		assertTrue(sem.contains("pretty"));
		assertTrue(sem.contains("female person"));
		
	}
	
	@Test
	@Ignore
	public void test2() {
		String searchTerm = "classy";
		LinkedList<String> sem = SenticDatabase.instance().getSemantics(searchTerm);
		
		assertTrue(sem.contains("female human"));
		assertTrue(sem.contains("relate"));
	}

}
