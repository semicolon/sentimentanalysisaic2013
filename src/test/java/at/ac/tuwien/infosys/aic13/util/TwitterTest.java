package at.ac.tuwien.infosys.aic13.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import twitter4j.FilterQuery;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.StatusAdapter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

public class TwitterTest
{

	@Test(timeout = 10 * 1000)
	public void streamingTest() throws InterruptedException
	{
		final Object notifier = new Object();

		final TwitterStream twitterStream = new TwitterStreamFactory().getInstance();

		try
		{
			final FilterQuery filterQuery = new FilterQuery();
			filterQuery.language(new String[] { "en" });
			filterQuery.track(new String[] { "and" });

			twitterStream.addListener(new StatusAdapter() {
				@Override
				public void onStatus(Status status)
				{
					checkStatus(status);

					synchronized (notifier)
					{
						notifier.notify();
					}
				}
			});

			synchronized (notifier)
			{
				twitterStream.filter(filterQuery);
				notifier.wait();
			}
		} finally
		{
			twitterStream.shutdown();
		}
	}
	
	private void checkStatus(Status status)
	{
		assertNotNull(status);
		assertFalse(0L == status.getId());
		assertNotNull(status.getCreatedAt());
		assertNotNull(status.getText());
		
		assertEquals("en", status.getIsoLanguageCode());
		
		assertNotNull(status.getUser());
		assertFalse(0L == status.getUser().getId());
		assertNotNull(status.getUser().getScreenName());
	}
	
	@Test
	public void searchTest() throws TwitterException
	{
		final Query query = new Query();
		query.setLang("en");
		query.setQuery("microsoft");
		query.setCount(1);
		
		final QueryResult result = TwitterFactory.getSingleton().search(query);
		assertEquals(1, result.getTweets().size());
		
		final Status status = result.getTweets().get(0);
		
		checkStatus(status);
	}
}
