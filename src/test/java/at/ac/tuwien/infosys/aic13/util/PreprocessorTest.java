package at.ac.tuwien.infosys.aic13.util;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Ignore;
import org.junit.Test;

import at.ac.tuwien.infosys.aic13.web.Preprocessor.Preprocessor;

public class PreprocessorTest
{
	@Test
	@Ignore
	public void test() throws IOException
	{
		final String regex = ".?USERNAME test";
		final String input = "@Swos_cross test.";		
		final String output = new Preprocessor().preprocessDocument(input);
		
		if(!input.matches(regex))
		{
			fail("Expected \"" + regex + "\" but got \"" + output +"\"");
		}		
	}
}
