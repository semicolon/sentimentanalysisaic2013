package at.ac.tuwien.infosys.aic13.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import at.ac.tuwien.infosys.aic13.db.DBWrapperFactory;
import at.ac.tuwien.infosys.aic13.db.StatusImpl;

public class DatabaseTest {

	@Test
	public void wrapperFactoryTest() {
		assertNotNull(DBWrapperFactory.getInstance());
	}
	
	@Test
	@Ignore
	public void saveTest() throws TwitterException
	{
		//TODO: like can't handle _ in the query. report to objectdb
		saveTestHelper("tuw_aic13");		
	}
	
	@Test
	@Ignore
	public void saveTest2() throws TwitterException
	{
		saveTestHelper("tuwaic13");

	}
	
	private void saveTestHelper(String queryString) throws TwitterException
	{
		final Query query = new Query();
		query.setQuery(queryString);
		query.setCount(1);
		
		final QueryResult result = TwitterFactory.getSingleton().search(query);
		assertEquals(1, result.getTweets().size());
		
		final Status status = result.getTweets().get(0);
		
		DBWrapperFactory.getInstance().save(StatusImpl.fromStatus(status), true);
		
		
		assertTrue(DBWrapperFactory.getInstance().getAll().size() > 0);
		for(Status stat : DBWrapperFactory.getInstance().getAll())
		{
			//System.out.println(stat);
		}
		System.out.println();
		assertTrue(DBWrapperFactory.getInstance().getAllStatusWithText(queryString,  1).size() > 0);
	}
}
