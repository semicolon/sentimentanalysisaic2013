<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:f="http://java.sun.com/jsf/core"
	xmlns:h="http://java.sun.com/jsf/html">

<h:head>
	<link href="<c:url value="/resources/css/style.css" />"
		rel="stylesheet" />
	<title>Twitter Sentiment Analysis</title>
</h:head>

<h:body>
	<div style="padding: 10px">
		<h1>Twitter Sentiment Analysis</h1>

		<form:form action="" method="get">

			<div class="row" style="width: 60%; padding: 0 0 20px 0">
				<div class="col-lg-6">
					<div class="input-group">
						<input type="text" class="form-control" name="searchValue">
							<span class="input-group-btn">
								<button class="btn btn-default" type="submit">Analyse</button>
						</span> <!--  Could be commented in if we did not reject tweets containing links. -->
							<!-- span class="input-group-addon"> <input type="checkbox"
								name="showThumbnails"> Show Thumbnails </span-->
					</div><div style="margin: 10px 0 0 0"><strong>Classifier</strong></div>
							<div class="radio">
								<label> <input type="radio" name="classifier"
									id="sentic" value="sentic" checked> Sentic </label>
							</div>
							<div class="radio">
								<label> <input type="radio" name="classifier" id="weka"
									value="weka"> Weka (may be slow) </label>
							</div>
				</div>
			</div>
		</form:form>

		<c:if test="${not empty resultString}">
   			<strong>${resultString}</strong>
			<hr />
		</c:if>

		<c:forEach var="image" items="${images}">
			<a href="${image.originalLink}" target="_blank"><img
				src="${image.resolvedLink}" title="${image.userScreenName}" /></a>
		</c:forEach>

		<p>
			<c:forEach var="tweet" items="${tweets}">
					<c:out value="${tweet}" escapeXml="false" />
			</c:forEach>
		</p>
		<hr />
		<span style="font-size: 10px">Created by <em>Martin Dietl,
				Florian Rohrer, Andreas Wimmer, Christoph Zimmel</em></span>

	</div>
</h:body>
</html>
