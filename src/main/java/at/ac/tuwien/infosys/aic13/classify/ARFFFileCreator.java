package at.ac.tuwien.infosys.aic13.classify;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Locale;
import java.util.Scanner;

public class ARFFFileCreator {

	public static void main(String[] args) throws Exception{
		new ARFFFileCreator();
	}
	public ARFFFileCreator() throws Exception{
		
//		String query = "asus";
//		
//		 DBWrapper db = DBWrapperFactory.getInstance();
//		 List<Status> stats = db.getAllStatusWithText(query, 200);
//		 
//		 FileWriter writer = new FileWriter(new File("C:/Users/Flo/Desktop/"+query+".txt"));
//		 
//		 for(int i = 0; i < stats.size(); i++) {
//			 writer.write(stats.get(i).getText().replace("\n", " "));
//			 writer.write("\n");
//		 }
//		 writer.flush();
//		 writer.close();
		
		FileWriter writer = new FileWriter("c:/users/flo/desktop/brandsTop500.txt");
		BufferedReader reader = new BufferedReader(new FileReader("c:/users/flo/desktop/BSB12-Official-Top-500-yvd1Ux.txt"));
		String s;
		while((s = reader.readLine()) != null){
			if(s.equals("") || s.equals("OFFICIAL TOP 500 2012") || s.equals("BRAND") || 
					s.equals("RANK INDEX") || s.equals("CATEGORY") || s.equals("continues...") ||
					s.equals("Powered by")) continue;
			
			Scanner sc = new Scanner(s);
			sc.useLocale(Locale.ENGLISH);
			if(sc.hasNextInt() || sc.hasNextDouble()) continue;
			
			String brand = "";
			boolean flag = false;
			while(sc.hasNext()){
				String curr = sc.next();
				
				if(curr.equals(curr.toUpperCase())){
					if(!flag)
						brand = "";
					
					flag = true;
					
					if(brand.equals("")){
						brand = curr;
					} else {
						brand += " "+curr;
					}
				} else 
					flag = false;
			}
			
			if(brand.equals("&") || brand.equals("") || brand.equals("-") || brand.equals("- B2B") ||
					brand.equals("- PR") || brand.equals("/")) continue;
			
			if(brand.contains("(")){
				brand = brand.substring(0, brand.indexOf("(")-1);
			}
			
			writer.write(brand+"\n");
			
		}
		writer.flush();
		writer.close();
		reader.close();
		
	}
	
}
