package at.ac.tuwien.infosys.aic13.analyze;

import java.util.ArrayList;


class Arc{

	public int v1;
	public int v2;

	public int w;

	public int d;

	public Arc(int v1, int v2, int w, int d){
		this.v1 = v1;
		this.v2 = v2;
		this.w = w;
		this.d = d;
	}
}

class Entry{
	public int d; // delay.
	public Entry p;
	public int w; // weight.

	public int v;
	
	public Entry(int v, Entry p, int d, int w){
		this.v = v;
		this.d = d;
		this.p = p;
		this.w = w;
	}
}


public class Algo {

	public static void main(String[] args){
		long[][] mat = new long[][]{
				{0,2,1,Integer.MAX_VALUE,Integer.MAX_VALUE},
				{Integer.MAX_VALUE,0,Integer.MAX_VALUE,7,Integer.MAX_VALUE},
				{Integer.MAX_VALUE,3,0,Integer.MAX_VALUE,4},
				{Integer.MAX_VALUE,-3,Integer.MAX_VALUE,0,4},
				{Integer.MAX_VALUE,5,-2,Integer.MAX_VALUE,0}
		};
		//floyd_warshall(5,mat);
		//constShortPath(11);
		//sortNums7Comp(new int[]{5,11,2,7,1});
		sortNums7Comp(new int[]{5,1,11,2,7});
	}

	public static void floyd_warshall(int n, long[][] mat){

		int[][] pred_mat = new int[n][n];
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++)
				pred_mat[i][j] = 0;

		for(int k=0;k<n;k++){
			long[][] tmat = new long[n][n];
			for(int i=0;i<n;i++){
				for(int j=0;j<n;j++){
					if(mat[i][k] + mat[k][j] < mat[i][j]){
						tmat[i][j] = mat[i][k] + mat[k][j];
						if(tmat[i][j]>=Integer.MAX_VALUE/2)
							tmat[i][j] = Integer.MAX_VALUE;
						else{
							pred_mat[i][j] = k+1;
						}
					}else{
						tmat[i][j] = mat[i][j];
					}
					/*if(mat[k][i] + mat[j][k] < mat[j][i]){
						mat[j][i] = mat[k][j] + mat[j][k];
						if(mat[j][i]>=Integer.MAX_VALUE)
							mat[j][i] = Integer.MAX_VALUE;
					}*/
				}	
			}
			String res = "";
			for(int i=0;i<n;i++){
				for(int j=0;j<n;j++){
					if(tmat[i][j] == Integer.MAX_VALUE)
						res+=" INF";
					else
						res+=" "+tmat[i][j];
				}
				res+="\n";
			}
			mat = tmat;
			System.out.println(res);
		}

		String res = "";
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				res+=" "+pred_mat[i][j];
			}
			res+="\n";
		}
		System.out.println(res);

	}
	
	public static void swap(int[] a, int i, int j){
		int t = a[i];
		a[i] = a[j];
		a[j] = t;
	}
	
	public static void shiftr(int[] a, int s, int l){
		for(int i = s+l-1;i>=s;i--){
			a[i+1] = a[i];  
		}
	}
	
	public static void sortNums7Comp(int[] a){
		
		if(a[0]>a[1])
			swap(a,0,1);
		if(a[2]>a[3])
			swap(a,2,3);

		for(int i=0;i<4;i++)
			System.out.print(a[i]+",");
		System.out.println("\n----");
		
		if(a[1]>a[3]){
			swap(a, 1,3);
			swap(a, 1,2);
			if(a[0]>a[1])
				swap(a,0,1);
		}else if(a[2]<a[0]){
			swap(a,0,2);
			swap(a,1,2);
		}else if(a[2]<a[1]){
			swap(a,1,2);
		}

		// a[0-3] sorted.
		for(int i=0;i<4;i++)
			System.out.print(a[i]+",");
		System.out.println("\n----");
		
		// a[0] a[1] a[2] a[3] a[4]
		
		if(a[4]>a[1]){
			if(a[4]<a[2]){
				swap(a,2,4);
				swap(a,3,4);
				//a[0],a[1],a[4],a[2],a[3].
			}else if(a[4]<a[3]){
				swap(a,3,4);
				//a[0],a[1],a[2],a[4],a[3].
			}else{
				//a[0],a[1],a[2],a[3],a[4].
			}
		}else{
			if(a[4]<a[0]){
				int t = a[4];
				shiftr(a,0,4);
				a[0] = t;
				//a[4],a[0],a[1].....;
			}else{
				int t = a[4];
				shiftr(a,0,4);
				a[0] = t;
				swap(a,0,1);
				// a[0],a[4],a[1].....;
			}
		}
		
		/*
		for(int i=0;i<5;i++)
			System.out.print(a[i]+",");
		System.out.println("\n----");*/
	}


	public static void constShortPath(int D){

		int s = 0;
		int t = 4;

		ArrayList<Arc> A = new ArrayList<Arc>();
		A.add(new Arc(0,1,5,1));
		A.add(new Arc(0,2,3,3));
		A.add(new Arc(1,2,2,1));
		A.add(new Arc(2,3,2,2));
		A.add(new Arc(1,3,3,2));
		A.add(new Arc(3,4,1,1));
		A.add(new Arc(0,3,1,10));
		
		ArrayList<Integer> F = new ArrayList<Integer>(); 

		Entry tab[][] = new Entry[5][D+1];

		tab[s][0] = new Entry(s,null,0,0);
		F.add(s);
		
		for(int i=1;i<=D;i++){
			for(int j=0;j<A.size();j++){
				Arc a = A.get(j);

				int ca = 0;

				if(F.contains(a.v1) && F.contains(a.v2)){
					// New path to v2.
					ca = 1;
				} else if(F.contains(a.v1)){
					// v2 not visited.
					ca = 2;
				}else{
					// Edge not reachable.
				}

				if(ca != 0){
					if(i-a.d >= 0){
						Entry e = tab[a.v1][i-a.d];
						if(e != null){
							if((e.d+a.d) <= i){
								// Edge inside delay.
								//System.out.println("D="+i+" arc:<"+a.v1+" "+a.v2+"> d:"+a.d+" ("+(e.d+a.d)+")");
								Entry com = new Entry(a.v2,e, e.d+a.d, e.w+a.w);
								if(tab[a.v2][i] != null){
									if(tab[a.v2][i].w>com.w){
										// Better path found.
										tab[a.v2][i] = com;
									}
								}else{
									tab[a.v2][i] = com;
								}
								if(ca == 2)
									F.add(a.v2);
							}
						}
					}
				}
			}
		}
		
		Entry sc = null;
		
		// Find shortest path.
		for(int i=1;i<=D;i++){
			if(tab[t][i] != null)
				if(sc == null)
					sc = tab[t][i];
				else if(sc.w>tab[t][i].w)
					sc = tab[t][i];
		}
		
		if(sc == null){
			System.out.println("no path exists");
			return;
		}
		
		Entry cur = sc;
		
		do{
			System.out.println(cur.v);
			cur = cur.p;
		}while(cur != null);
		
		
		String res ="";
		for(int j=0;j<D+1;j++){
			res += "D["+j+"]:";
			for(int i=0;i<5;i++){
				Entry e = tab[i][j];
				if(e == null)
					res+=" X";
				else
					res += " "+e.w;
			}
			res+="\n";
		}
		System.out.println(res);

	}


	/*

	a[0-4]

	if(a[0]>a[1])
		swap(a[0],a[1]);
	if(a[2]>a[3])
		swap(a[2],a[3]);

	if(a[1]>a[3]){
		swap(a[1],a[3]);
		if(a[2]<a[0]){
			swap(a[1],a[3]);
		}else{
			swap(a[1],a[2]);
		}
	}else if(a[2]<a[0]){
		swap(a[0],a[2]);
		swap(a[1],a[2]);
	}

	// a[0-3] sorted.

	if(a[4]>a[1]){
		if(a[4]<a[2]){
			a[0],a[1],a[4],a[2],a[3].
		}else if(a[4]<a[3]){
			a[0],a[1],a[2],a[4],a[3].
		}else{
			a[0],a[1],a[2],a[3],a[4].
		}
	}else{
		if(a[4]<a[0]){
			a[4],a[0],a[1].....;
		}else{
			a[0],a[4],a[1].....;
		}
	}



	getConstraintShortestPath(G,s,t,D)

	s .... start vertex.
	t .... target vertex.
	D .... Delay constraint.
	F[] .. Visited Vertices.
	A .... Arcs

	F.add(s); // Visit start vertex.

	for(i = 1 ... D){

		for(j = 1 ... |A|){
			<v1,v2> a = A[j]
			N = {}
			if(F.contains(v1) && F.contains(v2)){
				// Edge already visted.
				int v2d = v2.tab[d-a.d];
				if(v1.tab[d]>v2d+a.w)
					v1.tab[d] = v2d+a.w;
			}else if(F.contains(v1)){
				// new vertex v2.
				v2.parent = v1; 
				v2.tab[d] = v1.tab[d-a.d]+a.w;
				N.add(v2);
			}else if(F.contains(v2)){
				// new vertex v1.
				v1.parent = v2; 
				v1.tab[d] = v2.tab[d-a.d]+a.w;
				N.add(v1);
			}else{
				// Edge not reachable yet.
			}
			F.add(N);
		}
	}

	int min_t = INF;
	for(i = 1 ... |D|){
		if(t.tab[i]<min_t)
			min_t = t.tab[i];
	}

	if(min_t == INF)
		return; // No shortest path with delay <=D exists.

	// Traverse parents.

	 */

}
