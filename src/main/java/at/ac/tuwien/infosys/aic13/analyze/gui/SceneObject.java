package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.Rectangle;

/**
 * Base scene object.
 */
public abstract class SceneObject {
	
	/**
	 * The objects instance id.
	 * Must be unique under all objects belonging to the same document.
	 */
	protected long instance_id;
	
	/**
	 * The bounds of the object.
	 */
	protected Rectangle bounds; 
	
	/**
	 * Selection state.
	 */
	protected boolean selected;
	
	/**
	 * CTor.
	 */
	public SceneObject(){
		bounds = new Rectangle();
	}
	
	/**
	 * Get bounds.
	 * @return
	 */
	public Rectangle getBounds(){
		return bounds;
	}
	
	/**
	 * Set object translation.
	 * @param x
	 * @param y
	 */
	public void setTranslation(float x, float y){
		this.bounds.x = (int)x;
		this.bounds.y = (int)y;
	}
	
	/**
	 * Translate object.
	 */
	public void translate(float x, float y){
		this.bounds.x += x;
		this.bounds.y += y;
	}
	
	/**
	 * Set bounds.
	 * @param bounds
	 */
	public void setBounds(Rectangle bounds){
		this.bounds = bounds;
	}
	
	/**
	 * Check if objects bounds contain given point.
	 * @return True if contains, else false.
	 */
	public boolean contains(float x, float y){
		return bounds.contains(x,y);
	}
	
	public void setSelected(boolean selected){
		this.selected = selected;
	}
	
	public boolean getSelected(){
		return selected;
	}
	
	/**
	 * Layout object.
	 */
	public abstract void layout();
	
	/**
	 * On mouse down event.
	 */
	public boolean onMouseDown(CanvasMouseEventArgs args){return false;}
	
	/**
	 * Render object onto canvas.
	 * @param canvas
	 */
	public void render(CanvasRenderArgs args){} 
	
	
	/**
	 * Get object instance id.
	 * @return
	 */
	public long getInstanceId(){
		return instance_id;
	}
	
	/**
	 * Set instance id.
	 * @param id
	 */
	public void setInstanceId(long id){
		this.instance_id = id;
	}
	
	/**
	 * Returns true if selectable, else false.
	 * @return
	 */
	public boolean isSelectable(){
		return true;
	}
	
	
}
