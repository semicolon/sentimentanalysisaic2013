package at.ac.tuwien.infosys.aic13.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

//Helper for twitter4j.properties
//Modified from http://consultingblogs.emc.com/nileeshabojjawar/archive/2010/03/18/twitter4j-oauth-generating-the-access-token.aspx
public class TokenGenerator {
	private static final Logger logger = LoggerFactory.getLogger(TokenGenerator.class);
	
	private final static String consumerKey = "u7O7ojYbg4ffNsJkaUAqeg";
	private final static String consumerSecret = "JqxqYVLA2mAwcY3tJXPsEHYqgYPNpaUkhX3juIyQgE";

	public static void main(String args[]) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		Twitter twitter = new TwitterFactory().getInstance();
		
		final File config = new File("./src/main/resources/twitter4j.properties");
		
		try
		{
			twitter.setOAuthConsumer(consumerKey, consumerSecret);		
		}
		catch(final IllegalStateException _)
		{
			//Consumer is already set in twitter4j.properties
		}
		
		RequestToken requestToken;
		try
		{
			
			requestToken = twitter.getOAuthRequestToken();			
		}
		catch(final IllegalStateException _)
		{
			logger.debug("Auth info already exists, aborting...");			
			
			//----------------------------------			
			/*System.err.println("Warning: twitter4j.properties detected. If you continue, all content of it will be lost");
			
			String choice = "";
			
			while(! (choice.equals("y") || choice.equals("n")))
			{
				System.err.print("Continue? (y/n): ");
				choice = br.readLine().toLowerCase(Locale.US);
			}
			
			if(choice.equals("n"))*/
			{
				//System.err.println("Aborting...");
				return;
			}
		
			//twitter.setOAuthAccessToken(null); //twitter4j refuses to generate a new token if one is already set
			//requestToken = twitter.getOAuthRequestToken();
		}
		
		System.out.println("\n\n----------------------------------------");
		System.out.println("Twitter Token Generator");
		System.out.println("----------------------------------------");
		
		AccessToken accessToken = null;	

		while (null == accessToken) {
			System.out.println("Open the following URL and grant access to your account:");
			System.out.println(requestToken.getAuthorizationURL());
			System.out.print("Enter the PIN (if aviailable) or just hit enter.\n[PIN]:");

			String pin = br.readLine();

			try {
				if (pin.length() > 0) {
					
					accessToken = twitter
							.getOAuthAccessToken(requestToken, pin);

				} else {

					accessToken = twitter.getOAuthAccessToken();

				}

			} catch (TwitterException te) {

				if (401 == te.getStatusCode()) {
					System.out.println("Unable to get the access token.");
				} else {
					te.printStackTrace();
				}

			}

		}
		
		final PrintWriter configWriter = new PrintWriter(new BufferedWriter(new FileWriter("./src/resources/twitter4j.properties", false)));
		
		configWriter.write("debug=false");	
		configWriter.write("\noauth.consumerKey=" + consumerKey);	
		configWriter.write("\noauth.consumerSecret=" + consumerSecret);	
		configWriter.write("\noauth.accessToken=" + accessToken.getToken());	
		configWriter.write("\noauth.accessTokenSecret=" + accessToken.getTokenSecret());
	
		configWriter.flush();	
		configWriter.close();
		
		System.out.println("twitter4j.properties successfully written.");
		System.out.println("----------------------------------------");
	}
}
