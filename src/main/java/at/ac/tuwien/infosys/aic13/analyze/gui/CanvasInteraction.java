package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.Point;

/**
 * Canvas interaction class.
 */
public class CanvasInteraction extends Interaction{

	private Point init_pos;
	
	private Canvas canvas;
	
	/**
	 * CTor.
	 */
	public CanvasInteraction(Canvas canvas, Point pos){
		this.canvas = canvas;
		this.init_pos = pos;
	}
	
	@Override
	public void onMouseDown(InteractionMouseEventArgs arg){
		
	}
	
	@Override
	public void onMouseUp(InteractionMouseEventArgs arg){
		
	}
	
	@Override
	public void onMouseMove(InteractionMouseEventArgs arg){
		
		Point np = arg.me.getPoint();
		
		float dx = np.x-init_pos.x;
		float dy = np.y-init_pos.y;
		
		canvas.getActiveCamera().translate(dx, dy);
		
		init_pos = np;
		
		canvas.invalidate();
		canvas.repaint();
	}
	
}
