package at.ac.tuwien.infosys.aic13.receive.StatusAdapter;

public class Utils {
	
	private static final String allowedChars = "a-zA-Z0-9öüäÖÜÄß\\^!@#°\\$§&%\\*\\(\\)+=\\-\\[\\]/\\{\\}\\|:<>\\?,\\.´`'_\\ \"\r\n\t";
	public static final String regex = "^["+allowedChars+"]*$";
}
 