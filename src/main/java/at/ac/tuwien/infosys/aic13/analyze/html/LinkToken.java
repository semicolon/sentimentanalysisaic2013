package at.ac.tuwien.infosys.aic13.analyze.html;

/**
 * Link token.
 */
public class LinkToken extends TextToken{

	/**
	 * Link.
	 */
	protected String link;
	
	/**
	 * Ctor.
	 * @param text
	 * @param link
	 */
	public LinkToken(String text, String link) {
		super(text);
		this.link = link;
	}

	
	public String getLink(){
		return link;
	}
	
	public void setLink(String link){
		this.link = link;
	}
}
