package at.ac.tuwien.infosys.aic13.receive.StatusAdapter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.regex.Pattern;

import twitter4j.Status;

public class LocalFilter
{
	private static HashSet<String> notAllowed;
	
	static {
		notAllowed = new HashSet<String>();
		notAllowed.addAll(Arrays.asList(new String[] {
				"http", "news", "retweet", "bieber", "belieber"
		}));
		
	}
	
	public static boolean useTweet(Status status)
	{
		return status.getIsoLanguageCode().equals("en") 
				&& Pattern.matches(Utils.regex, status.getText())
				&& !LocalFilter.isProbablyNewsOrSpam(status);
	}
	
	private static boolean isProbablyNewsOrSpam(Status status) {
		String text = status.getText().toLowerCase();
		
		for(String no: notAllowed){
			if(text.contains(no))
				return true;
		}		
		return false;
	}
}
