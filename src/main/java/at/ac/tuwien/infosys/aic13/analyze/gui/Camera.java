package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Stack;

/**
 * Camera object.
 */
public class Camera {

	/**
	 * Camera position.
	 */
	protected Point2D.Float position;
	
	/**
	 * Cameras zoom.
	 */
	protected double zoom; 
	
	/**
	 * Inverse zoom.
	 */
	protected double inv_zoom;
	
	/**
	 * Transformation stack.
	 */
	protected Stack<AffineTransform> stack;
	
	
	/**
	 * Ctor.
	 */
	public Camera(){
		position = new Point2D.Float(10,10);
		stack = new Stack<AffineTransform>();
		this.zoom = 1;
		this.inv_zoom = 1;
	}
	
	/**
	 * Translate camera.
	 * @param x
	 * @param y
	 */
	public void translate(float x, float y){
		position.x+=x;
		position.y+=y;
	}

	/**
	 * Get point on screen.
	 * @param pt
	 * @return
	 */
	public Point2D.Float getPoS(Point pt){
		float nx = pt.x*(float)zoom+position.x;
		float ny = pt.y*(float)zoom+position.y;
		Point2D.Float r = new Point2D.Float(nx,ny);
		return r;
	}
	
	/**
	 * Get point on canvas.
	 * @return
	 */
	public Point2D.Float getPoC(Point pt){
		float nx = (pt.x-position.x)*(float)inv_zoom;
		float ny = (pt.y-position.y)*(float)inv_zoom;
		Point2D.Float r = new Point2D.Float(nx,ny);
		return r;
	}
	
	/**
	 * Get camera position.
	 * @return
	 */
	public Point2D.Float getPosition(){
		return position;
	}
	
	/**
	 * Set zoom.
	 * @param zoom
	 */
	public void setZoom(float zoom){
		this.zoom = zoom;
		this.inv_zoom = 1f/zoom;
	}

	/**
	 * Get zoom.
	 * @return
	 */
	public float getZoom(){
		return (float)this.zoom;
	}
	
	
	public void push(Graphics g){
		Graphics2D g2 = (Graphics2D)g;
		AffineTransform at = g2.getTransform();
		stack.push(at);
		g2.translate(position.x, position.y);
		g2.scale(zoom, zoom);
	}
	
	public void pop(Graphics g){
		Graphics2D g2 = (Graphics2D)g;
		AffineTransform at = stack.pop();
		g2.setTransform(at);
	}
}
