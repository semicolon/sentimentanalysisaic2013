package at.ac.tuwien.infosys.aic13.analyze;

/**
 * Result of a search term operation.
 */
public class SearchResult {

	/**
	 * The search term.
	 */
	public String search_term;
	
	/**
	 * Ctor.
	 */
	public SearchResult(){
	}
	
}
