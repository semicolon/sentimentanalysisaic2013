package at.ac.tuwien.infosys.aic13.receive.StatusAdapter;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Status;
import twitter4j.StatusAdapter;
import at.ac.tuwien.infosys.aic13.classify.SenticClassifier;
import at.ac.tuwien.infosys.aic13.web.Preprocessor.Preprocessor;

public class ClassifyingAdapterSentic extends StatusAdapter {
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	private Preprocessor proc;
	
	public ClassifyingAdapterSentic()
	{
		try
		{
			proc = new Preprocessor();
		} catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void onStatus(Status status)
	{
		if (!LocalFilter.useTweet(status))
			return;

		try
		{
			System.out.println("   sentic: " + SenticClassifier.print(SenticClassifier.classify(proc.preprocessDocument(status.getText()))));	
		} catch (Exception e)
		{
			log.error("unable to classify status " + status.getId(), e);
		}
	}
}
