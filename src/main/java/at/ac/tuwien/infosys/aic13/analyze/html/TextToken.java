package at.ac.tuwien.infosys.aic13.analyze.html;

/**
 * Text token.
 */
public class TextToken{

	/**
	 * Text content.
	 */
	protected String text;
	
	/**
	 * Ctor.
	 * @param text
	 */
	public TextToken(String text){
		this.text = text;
	}
	
	/**
	 * Set content.
	 * @param text
	 */
	public void setText(String text){
		this.text = text;
	}
	
	/**
	 * Get content.
	 */
	public String getText(){
		return text;
	}
	
	@Override
	public String toString(){
		return text;
	}
	
}
