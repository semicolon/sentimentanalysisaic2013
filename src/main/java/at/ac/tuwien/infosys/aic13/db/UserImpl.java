package at.ac.tuwien.infosys.aic13.db;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import javax.jdo.annotations.Index;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.URLEntity;
import twitter4j.User;

@Entity
final class UserImpl implements User
{
	private static final long serialVersionUID = -6345893237975349030L;

	@SuppressWarnings("deprecation")
	public static UserImpl fromStatus(User orig)
	{
		final UserImpl user = new UserImpl();
		user.createdAt = orig.getCreatedAt();
		user.description = orig.getDescription();
		user.descriptionURLEntities = orig.getDescriptionURLEntities();
		user.id = orig.getId();
		user.isFollowRequestSent = orig.isFollowRequestSent();
		user.friendsCount = orig.getFriendsCount();
		user.isContributorsEnabled = orig.isContributorsEnabled();
		user.isFollowRequestSent = orig.isFollowRequestSent();
		user.isGeoEnabled = orig.isGeoEnabled();
		user.isProtected = orig.isProtected();
		user.isVerified = orig.isVerified();
		user.favouritesCount = orig.getFavouritesCount();
		user.followersCount = orig.getFollowersCount();
		user.lang = orig.getLang();
		user.listedCount = orig.getListedCount();
		user.location = orig.getLocation();
		user.name = orig.getName();
		user.profileBackgroundColor = orig.getProfileBackgroundColor();
		user.profileBackgroundImageUrl = orig.getProfileBackgroundImageURL();
		user.profileBackgroundImageUrlHttps = orig.getProfileBackgroundImageUrlHttps();
		user.profileBackgroundTiled = orig.isProfileBackgroundTiled();
		// user.profileBannerImageUrl= // can't be accessed from orig
		user.profileLinkColor = orig.getProfileLinkColor();
		user.profileSidebarBorderColor = orig.getProfileSidebarBorderColor();
		user.profileSidebarFillColor = orig.getProfileSidebarFillColor();
		user.profileTextColor = orig.getProfileTextColor();
		user.profileImageUrl = orig.getProfileImageURL();	
		user.profileImageUrlHttps = orig.getProfileImageUrlHttps().toString();
		user.profileUseBackgroundImage = orig.isProfileUseBackgroundImage();
		user.screenName = orig.getScreenName();
		user.showAllInlineMedia = orig.isShowAllInlineMedia();
		user.status = StatusImpl.fromStatus(orig.getStatus());
		user.statusesCount = orig.getStatusesCount();
		user.timeZone = orig.getTimeZone();
		user.translator = orig.isTranslator();
		user.url = orig.getURL();
		user.urlEntity = orig.getURLEntity();
		user.utcOffset = orig.getUtcOffset();

		return user;
		// return convert(orig, UserImpl.class);
	}

	@Index
	private Date createdAt;

	private String description;

	@Transient
	private URLEntity[] descriptionURLEntities;

	private int favouritesCount;

	@Index
	private int followersCount;

	private int friendsCount;

	@Id
	private long id;

	private boolean isContributorsEnabled;

	private boolean isFollowRequestSent;

	private boolean isGeoEnabled;

	private boolean isProtected;

	private boolean isVerified;

	private String lang;

	private int listedCount;

	@Index
	private String location;

	@Index
	private String name;

	private String profileBackgroundColor;

	private String profileBackgroundImageUrl;

	private String profileBackgroundImageUrlHttps;

	private boolean profileBackgroundTiled;

	private String profileBannerImageUrl;

	private String profileImageUrl;

	private String profileImageUrlHttps;

	private String profileLinkColor;

	private String profileSidebarBorderColor;

	private String profileSidebarFillColor;

	private String profileTextColor;

	private boolean profileUseBackgroundImage;

	@Index
	private String screenName;

	private boolean showAllInlineMedia;

	private Status status;

	@Index
	private int statusesCount;

	private String timeZone;

	private boolean translator;

	private String url;

	@Transient
	private URLEntity urlEntity;

	private int utcOffset;

	@Override
	public int compareTo(User that)
	{
		return (int) (this.id - that.getId());
	}
	@Override
	public boolean equals(Object obj)
	{
		if (null == obj)
		{
			return false;
		}
		if (this == obj)
		{
			return true;
		}
		return obj instanceof User && ((User) obj).getId() == this.id;
	}
	@Override
	public int getAccessLevel()
	{
		throw new UnsupportedOperationException();
	}
	@Override
	public String getBiggerProfileImageURL()
	{
		return toResizedURL(profileImageUrl, "_bigger");
	}
	@Override
	public String getBiggerProfileImageURLHttps()
	{
		return toResizedURL(profileImageUrlHttps, "_bigger");
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date getCreatedAt()
	{
		return createdAt;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDescription()
	{
		return description;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public URLEntity[] getDescriptionURLEntities()
	{
		return descriptionURLEntities;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFavouritesCount()
	{
		return favouritesCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFollowersCount()
	{
		return followersCount;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFriendsCount()
	{
		return friendsCount;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getId()
	{
		return id;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLang()
	{
		return lang;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getListedCount()
	{
		return listedCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLocation()
	{
		return location;
	}
	@Override
	public String getMiniProfileImageURL()
	{
		return toResizedURL(profileImageUrl, "_mini");
	}
	@Override
	public String getMiniProfileImageURLHttps()
	{
		return toResizedURL(profileImageUrlHttps, "_mini");
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName()
	{
		return name;
	}
	@Override
	public String getOriginalProfileImageURL()
	{
		return toResizedURL(profileImageUrl, "");
	}
	@Override
	public String getOriginalProfileImageURLHttps()
	{
		return toResizedURL(profileImageUrlHttps, "");
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProfileBackgroundColor()
	{
		return profileBackgroundColor;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProfileBackgroundImageUrl()
	{
		return getProfileBackgroundImageURL();
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProfileBackgroundImageURL()
	{
		return profileBackgroundImageUrl;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProfileBackgroundImageUrlHttps()
	{
		return profileBackgroundImageUrlHttps;
	}
	@Override
	public String getProfileBannerIPadRetinaURL()
	{
		return profileBannerImageUrl != null ? profileBannerImageUrl + "/ipad_retina" : null;
	}
	@Override
	public String getProfileBannerIPadURL()
	{
		return profileBannerImageUrl != null ? profileBannerImageUrl + "/ipad" : null;
	}
	@Override
	public String getProfileBannerMobileRetinaURL()
	{
		return profileBannerImageUrl != null ? profileBannerImageUrl + "/mobile_retina" : null;
	}
	@Override
	public String getProfileBannerMobileURL()
	{
		return profileBannerImageUrl != null ? profileBannerImageUrl + "/mobile" : null;
	}
	@Override
	public String getProfileBannerRetinaURL()
	{
		return profileBannerImageUrl != null ? profileBannerImageUrl + "/web_retina" : null;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProfileBannerURL()
	{
		return profileBannerImageUrl != null ? profileBannerImageUrl + "/web" : null;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProfileImageURL()
	{
		return profileImageUrl;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public URL getProfileImageUrlHttps()
	{
		try
		{
			return new URL(profileImageUrlHttps);
		} catch (MalformedURLException e)
		{
			return null;
		}
	}
	@Override
	public String getProfileImageURLHttps()
	{
		return profileImageUrlHttps;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProfileLinkColor()
	{
		return profileLinkColor;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProfileSidebarBorderColor()
	{
		return profileSidebarBorderColor;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProfileSidebarFillColor()
	{
		return profileSidebarFillColor;
	}
	@Override
	public String getProfileTextColor()
	{
		return profileTextColor;
	}

	// /**
	// * Get URL Entities from JSON Object.
	// * returns URLEntity array by entities/[category]/urls/url[]
	// *
	// * @param json user json object
	// * @param category entities category. e.g. "description" or "url"
	// * @return URLEntity array by entities/[category]/urls/url[]
	// * @throws JSONException
	// * @throws TwitterException
	// */
	// private static URLEntity[] getURLEntitiesFromJSON(JSONObject json, String
	// category) throws JSONException, TwitterException {
	// throw new UnsupportedOperationException();
	// }

	@Override
	public RateLimitStatus getRateLimitStatus()
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getScreenName()
	{
		return screenName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Status getStatus()
	{
		return status;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getStatusesCount()
	{
		return statusesCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTimeZone()
	{
		return timeZone;
	}

	public String getUrl()
	{
		return url;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getURL()
	{
		return url;
	}

	public URLEntity getUrlEntity()
	{
		return urlEntity;
	}

	@Override
	public URLEntity getURLEntity()
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getUtcOffset()
	{
		return utcOffset;
	}

	@Override
	public int hashCode()
	{
		return (int) id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isContributorsEnabled()
	{
		return isContributorsEnabled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isFollowRequestSent()
	{
		return isFollowRequestSent;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isGeoEnabled()
	{
		return isGeoEnabled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isProfileBackgroundTiled()
	{
		return profileBackgroundTiled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isProfileUseBackgroundImage()
	{
		return profileUseBackgroundImage;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isProtected()
	{
		return isProtected;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isShowAllInlineMedia()
	{
		return showAllInlineMedia;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isTranslator()
	{
		return translator;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isVerified()
	{
		return isVerified;
	}

	public void setContributorsEnabled(boolean isContributorsEnabled)
	{
		this.isContributorsEnabled = isContributorsEnabled;
	}

	public void setCreatedAt(Date createdAt)
	{
		this.createdAt = createdAt;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setDescriptionURLEntities(URLEntity[] descriptionURLEntities)
	{
		this.descriptionURLEntities = descriptionURLEntities;
	}

	public void setFavouritesCount(int favouritesCount)
	{
		this.favouritesCount = favouritesCount;
	}

	public void setFollowersCount(int followersCount)
	{
		this.followersCount = followersCount;
	}

	public void setFollowRequestSent(boolean isFollowRequestSent)
	{
		this.isFollowRequestSent = isFollowRequestSent;
	}

	public void setFriendsCount(int friendsCount)
	{
		this.friendsCount = friendsCount;
	}

	public void setGeoEnabled(boolean isGeoEnabled)
	{
		this.isGeoEnabled = isGeoEnabled;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public void setLang(String lang)
	{
		this.lang = lang;
	}

	public void setListedCount(int listedCount)
	{
		this.listedCount = listedCount;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setProfileBackgroundColor(String profileBackgroundColor)
	{
		this.profileBackgroundColor = profileBackgroundColor;
	}

	public void setProfileBackgroundImageUrl(String profileBackgroundImageUrl)
	{
		this.profileBackgroundImageUrl = profileBackgroundImageUrl;
	}

	public void setProfileBackgroundImageUrlHttps(String profileBackgroundImageUrlHttps)
	{
		this.profileBackgroundImageUrlHttps = profileBackgroundImageUrlHttps;
	}

	public void setProfileBackgroundTiled(boolean profileBackgroundTiled)
	{
		this.profileBackgroundTiled = profileBackgroundTiled;
	}

	public void setProfileImageUrlHttps(String profileImageUrlHttps)
	{
		this.profileImageUrlHttps = profileImageUrlHttps;
	}

	public void setProfileLinkColor(String profileLinkColor)
	{
		this.profileLinkColor = profileLinkColor;
	}

	public void setProfileSidebarBorderColor(String profileSidebarBorderColor)
	{
		this.profileSidebarBorderColor = profileSidebarBorderColor;
	}

	public void setProfileSidebarFillColor(String profileSidebarFillColor)
	{
		this.profileSidebarFillColor = profileSidebarFillColor;
	}

	public void setProfileTextColor(String profileTextColor)
	{
		this.profileTextColor = profileTextColor;
	}

	public void setProfileUseBackgroundImage(boolean profileUseBackgroundImage)
	{
		this.profileUseBackgroundImage = profileUseBackgroundImage;
	}

	public void setProtected(boolean isProtected)
	{
		this.isProtected = isProtected;
	}

	public void setScreenName(String screenName)
	{
		this.screenName = screenName;
	}

	public void setShowAllInlineMedia(boolean showAllInlineMedia)
	{
		this.showAllInlineMedia = showAllInlineMedia;
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public void setStatusesCount(int statusesCount)
	{
		this.statusesCount = statusesCount;
	}

	public void setTimeZone(String timeZone)
	{
		this.timeZone = timeZone;
	}

	public void setTranslator(boolean translator)
	{
		this.translator = translator;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public void setUrlEntity(URLEntity urlEntity)
	{
		this.urlEntity = urlEntity;
	}

	// @Override
	// public String toString() {
	// return getScreenName();
	// }

	public void setUtcOffset(int utcOffset)
	{
		this.utcOffset = utcOffset;
	}

	public void setVerified(boolean isVerified)
	{
		this.isVerified = isVerified;
	}

	private String toResizedURL(String originalURL, String sizeSuffix)
	{
		if (null != originalURL)
		{
			int index = originalURL.lastIndexOf("_");
			int suffixIndex = originalURL.lastIndexOf(".");
			int slashIndex = originalURL.lastIndexOf("/");
			String url = originalURL.substring(0, index) + sizeSuffix;
			if (suffixIndex > slashIndex)
			{
				url += originalURL.substring(suffixIndex);
			}
			return url;
		}
		return null;
	}

	@Override
	public String toString()
	{
		return "UserJSONImpl{" + "id=" + id + ", name='" + name + '\'' + ", screenName='" + screenName + '\'' + ", location='" + location + '\'' + ", description='" + description + '\''
				+ ", isContributorsEnabled=" + isContributorsEnabled + ", profileImageUrl='" + profileImageUrl + '\'' + ", profileImageUrlHttps='" + profileImageUrlHttps + '\'' + ", url='" + url
				+ '\'' + ", isProtected=" + isProtected + ", followersCount=" + followersCount + ", status=" + status + ", profileBackgroundColor='" + profileBackgroundColor + '\''
				+ ", profileTextColor='" + profileTextColor + '\'' + ", profileLinkColor='" + profileLinkColor + '\'' + ", profileSidebarFillColor='" + profileSidebarFillColor + '\''
				+ ", profileSidebarBorderColor='" + profileSidebarBorderColor + '\'' + ", profileUseBackgroundImage=" + profileUseBackgroundImage + ", showAllInlineMedia=" + showAllInlineMedia
				+ ", friendsCount=" + friendsCount + ", createdAt=" + createdAt + ", favouritesCount=" + favouritesCount + ", utcOffset=" + utcOffset + ", timeZone='" + timeZone + '\''
				+ ", profileBackgroundImageUrl='" + profileBackgroundImageUrl + '\'' + ", profileBackgroundImageUrlHttps='" + profileBackgroundImageUrlHttps + '\'' + ", profileBackgroundTiled="
				+ profileBackgroundTiled + ", lang='" + lang + '\'' + ", statusesCount=" + statusesCount + ", isGeoEnabled=" + isGeoEnabled + ", isVerified=" + isVerified + ", translator="
				+ translator + ", listedCount=" + listedCount + ", isFollowRequestSent=" + isFollowRequestSent + '}';
	}

}
