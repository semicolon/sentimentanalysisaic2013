package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;

import at.ac.tuwien.infosys.aic13.analyze.Term;
import at.ac.tuwien.infosys.aic13.analyze.gui.Canvas.RenderChannel;


/**
 * Simple word object.
 */
public class WordObject extends SceneObject{

	/**
	 * The term.
	 */
	private Term term;

	/**
	 * 
	 */
	private ArrayList<WordObject> related;
	
	/**
	 * CTor.
	 * @param term
	 */
	public WordObject(Term term){
		this.term = term;
		related = new ArrayList<WordObject>();
	}

	public void addRelated(WordObject wo){
		related.add(wo);
	}
	
	public Term getTerm(){
		return term;
	}

	public void setTerm(Term term){
		this.term = term;
	}

	@Override
	public void layout(){
	}

	@Override
	public void render(CanvasRenderArgs args){
		Graphics2D g2 = (Graphics2D)args.graphics;
		
		//FontMetrics fm = g2.getFontMetrics();
		//Rectangle2D str_b = fm.getStringBounds(term.term, g2);
		float cx = this.bounds.x+this.bounds.width/2.0f;
		float cy = this.bounds.y+this.bounds.height/2.0f;

		if(args.channel == RenderChannel.OBJECT){
			g2.setColor(new Color(50,50,50,200));
			g2.fillRect(this.bounds.x+2, this.bounds.y+2, this.bounds.width, this.bounds.height);
			g2.setColor(new Color(200,200,200,255));
			g2.fillRect(this.bounds.x, this.bounds.y, this.bounds.width, this.bounds.height);
			
			g2.setColor(Color.WHITE);
			//g2.drawString(term.term, (int)(cx-str_b.getCenterX()), (int)(cy-str_b.getCenterY()));
			g2.setFont(new Font("arial",0,12));
			//g2.drawString(term.term, (int)(cx-0), (int)(cy-0));
		}else if(args.channel == RenderChannel.PREOBJECT){
			g2.setColor(new Color(50,50,50));
			for(int i=0;i<related.size();i++){
				WordObject wo = related.get(i);
				int ccx = (int)wo.bounds.getCenterX();
				int ccy = (int)wo.bounds.getCenterY();
				int mid_x = (int)(ccx+cx)/2;
				int mid_y = (int)(ccy+cy)/2;
				g2.fillOval(mid_x-5,mid_y-5, 10, 10);
				g2.drawLine((int)bounds.getCenterX(), (int)bounds.getCenterY(), ccx, ccy);
			}
			
		}

	}

}
