package at.ac.tuwien.infosys.aic13.analyze.html;

import java.util.ArrayList;

/**
 * Base List element.
 */
public class ListElement extends Element{

	/**
	 * True if this list is ordered.
	 */
	private boolean ordered;
	
	/**
	 * List items.
	 */
	private ArrayList<ListItemElement> items;
	
	/**
	 * Create a new list element.
	 * @param ordered
	 */
	public ListElement(boolean ordered) {
		this.ordered = ordered;
		if(ordered)
			this.name = "ol";
		else
			this.name = "ul";
		items = new ArrayList<ListItemElement>();
	}
	
	/**
	 * Get list item at index.
	 * @return
	 */
	public ListItemElement getItem(int index){
		return items.get(index);
	}
	
	
	public void addItem(ListItemElement item){
		items.add(item);
	}
	
	/**
	 * Get ordered state of list.
	 * @return True if ordered.
	 */
	public boolean ordered(){
		return ordered;
	}
	
	/**
	 * Get list length.
	 * @return The length.
	 */
	public int length(){
		return items.size();
	}
	
	@Override
	public String toString(){
		String res = "";
		for(int i=0;i<items.size();i++){
			if(i!=0)
				res+="\n";
			if(ordered)
				res+=(i+1)+":";
			res+= items.get(i).toString();
		}
		return res;
	}

	@Override
	public ArrayList<LinkToken> getAllLinks() {
		ArrayList<LinkToken> lt = new ArrayList<LinkToken>();
		for(int i=0;i<items.size();i++)
			lt.addAll(items.get(i).getAllLinks());
		return lt;
	}

	@Override
	public void flatten(ArrayList<TextElement> list) {
		for(int i=0;i<items.size();i++)
			items.get(i).flatten(list);
	}
	
}
