package at.ac.tuwien.infosys.aic13.classify;

import java.util.Arrays;
import java.util.LinkedList;

public class SenticSentiment {

	private String word;
	private LinkedList<String> semantic = new LinkedList<String>();
	
	private float pleasantness;
	private float attention;
	private float sensitivity;
	private float aptitude;
	private float polarity;
	
	public float[] getSentiment(){
		return new float[]{pleasantness, attention, sensitivity, aptitude, polarity};
	}
	
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public LinkedList<String> getSemantics() {
		return semantic;
	}
	public void addSemantic(String sem) {
		this.semantic.add(sem);
	}
	public float getPleasantness() {
		return pleasantness;
	}
	public void setPleasantness(float pleasantness) {
		this.pleasantness = pleasantness;
	}
	public float getAttention() {
		return attention;
	}
	public void setAttention(float attention) {
		this.attention = attention;
	}
	public float getSensitivity() {
		return sensitivity;
	}
	public void setSensitivity(float sensitivity) {
		this.sensitivity = sensitivity;
	}
	public float getAptitude() {
		return aptitude;
	}
	public void setAptitude(float aptitude) {
		this.aptitude = aptitude;
	}
	public float getPolarity() {
		return polarity;
	}
	public void setPolarity(float polarity) {
		this.polarity = polarity;
	}
	@Override
	public String toString() {
		
		String[] arr = new String[semantic.size()];
		
		return "SenticSentiment [word=" + word + ", semantics=" + Arrays.toString(semantic.toArray(arr))
				+ ", pleasantness=" + pleasantness + ", attention=" + attention
				+ ", sensitivity=" + sensitivity + ", aptitude=" + aptitude
				+ ", polarity=" + polarity + "]";
	}
	
}
