package at.ac.tuwien.infosys.aic13.preprocess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import twitter4j.Status;
import at.ac.tuwien.infosys.aic13.db.DBWrapperFactory;
import at.ac.tuwien.infosys.aic13.preprocess.snowball.SnowballStemmer;
import at.ac.tuwien.infosys.aic13.preprocess.snowball.englishStemmer;

public class Main {

	public static void main(String[] args) throws IOException {
		new Main();
	}
	public Main() throws IOException {

		HashSet<String> stopwords = createStopWordSet();
		HashSet<String> allowedwords = createAllowedWordsSet();

		List<? extends Status> results = DBWrapperFactory.getInstance().getAll();
		int count = 0;

		for(Status cs: results){
			count++;
			if(count > 100) break;
			//			else if(count > 200) break;

//			String str = preprocess(cs.getText());
//			str = removeStopwords(stopwords, str);
//			str = stem(str, allowedwords);

			System.out.println("\""+cs.getUser().getName()+"\": "+cs.getText());
//			System.out.println(str);
//			System.out.println("------");

		}

	}
	private boolean isAdvertisment(Status cs) {

		String[] sp = cs.getText().split(" ");
		String firstWord = sp[0].toLowerCase();

		return firstWord.startsWith("buy") ||
				firstWord.startsWith("enter") ||
				cs.getUserMentionEntities().length >= 4 ||
				sp[sp.length-1].startsWith("http");

	}

	private HashSet<String> createAllowedWordsSet() {
		HashSet<String> words = new HashSet<String>();
		words.add("google");
		return words;
	}
	/*
	 * Removes "RT ", links, @names and #-symbols.
	 */
	private String preprocess(String res) {
		res = res.startsWith("RT ")? res.substring(3) : res;

		StringBuilder builder = new StringBuilder();
		Scanner sc = new Scanner(res);
		while(sc.hasNext()){
			String cur = sc.next();
			if(!cur.startsWith("http") && !cur.startsWith("@")){
				if(cur.startsWith("#"))
					builder.append(cur.substring(1)+" ");
				else 
					builder.append(cur+" ");
			}
		}
		sc.close();
		return builder.substring(0, builder.length()-1);
	}
	private String stem(String str, HashSet<String> allowedWords) {

		StringBuilder builder = new StringBuilder();
		Scanner sc = new Scanner(str);
		SnowballStemmer stemmer = (SnowballStemmer) new englishStemmer();

		while(sc.hasNext()){
			String in = sc.next();
			stemmer.setCurrent(in);
			if(!allowedWords.contains(in.toLowerCase())){
				stemmer.stem();
			}
			builder.append(stemmer.getCurrent()+" ");
		}
		sc.close();
		return builder.substring(0, builder.length()-1);
	}
	public HashSet<String> createStopWordSet() throws IOException{
		HashSet<String> stopWordSet = new HashSet<String>();
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(getClass().getResourceAsStream("/stop.en.txt")));

		String in;
		while((in = reader.readLine()) != null){
			stopWordSet.add(in);
			if(in.contains("'")){
				stopWordSet.add(in.replace("'", "")); //don't -> dont
			}
		}
		reader.close();
		return stopWordSet;
	}

	public String removeStopwords(HashSet<String> stopwords, String str) {

		StringBuilder b = new StringBuilder();
		Scanner sc = new Scanner(str);
		while(sc.hasNext()){
			String in = sc.next();
			if(!stopwords.contains(in))
				b.append(in+" ");
		}
		sc.close();
		return b.toString();
	}

}
