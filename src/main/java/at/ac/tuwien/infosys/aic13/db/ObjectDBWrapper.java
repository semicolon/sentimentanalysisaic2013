package at.ac.tuwien.infosys.aic13.db;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import twitter4j.Status;

import com.google.common.base.Stopwatch;

/*package*/ class ObjectDBWrapper implements DBWrapper, AutoCloseable {

	private EntityManager em;
	private EntityTransaction tx;
	
	private final Query findStatusWithTextQuery;
	
	private final Logger log = LoggerFactory.getLogger(getClass());

	/*package*/ ObjectDBWrapper()
	{
		//Start server with:
		//cd ../objectdb-2.5.3_03/bin
		//java -cp objectdb.jar com.objectdb.Server start
		
		final Properties properties = new Properties();
		try
		{
			InputStreamReader is = new InputStreamReader(getClass().getResourceAsStream("/objectdb.properties"));
			properties.load(is);
		} catch (IOException e)
		{
			log.error("Unable to load objectdb.properties", e);
			throw new RuntimeException(e);
		}
		
		String twitterodbLocation = "";
		try {
			twitterodbLocation = (new ClassPathResource("twitter.odb")).getFile().getAbsolutePath();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		twitterodbLocation = properties.getProperty("ConnectionString", twitterodbLocation);
		
		log.info("Loading database from " + twitterodbLocation);
		
		EntityManagerFactory emf =
				Persistence.createEntityManagerFactory(twitterodbLocation, properties);
		em = emf.createEntityManager();
		em.getMetamodel().entity(StatusImpl.class);
		em.getMetamodel().entity(UserImpl.class);
		
		tx = em.getTransaction();
		tx.begin();
		
		
		final String queryString = "SELECT status FROM StatusImpl status WHERE (lower(status.text) LIKE lower(:filter)) "
				+ "OR (lower(status.user.screenName) = lower(:filter)) OR (lower(status.user.name) = lower(:filter)) "
				+ "ORDER BY status.createdAt desc";
		findStatusWithTextQuery = em.createQuery(queryString, StatusImpl.class);
	}
	
	private Calendar nextCommit = Calendar.getInstance();
	@Override
	public void save(StatusImpl status, boolean commitImmediately)
	{
		em.merge(status);
		
		// Only commit every 10 seconds for performance reasons
		// Only works if we continue to get tweets. i.e. if we stop getting
		// tweets alltogether or kill the listener, we lose (up to) 10 seconds
		// of the last tweets. It might be better with a timer, but that would
		// require synchronisation.
		
		if (commitImmediately || Calendar.getInstance().after(nextCommit)) 
		{
			commit();
			
			nextCommit = Calendar.getInstance();
			nextCommit.add(Calendar.SECOND, 10);
		}
	}

	@Override
	public void finalize()
	{
		close();
	}
	
	private void commit()
	{
//		log.debug("Flushing");
//		em.flush();
		tx.commit();
		em.clear(); //Tell JPA it doesn't need to track the objects anymore
		tx.begin();
	}

	private <T> List<T> listAll(final Class<T> clazz)
	{
		final CriteriaBuilder builder = em.getCriteriaBuilder();

		CriteriaQuery<T> query = builder.createQuery(clazz);
		final Root<T> c = query.from(clazz);
		query = query.select(c);
		return em.createQuery(query).getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Status> getAllStatusWithText(String filter, int limit) {
		findStatusWithTextQuery.setParameter("filter", "%" + filter +"%");
		findStatusWithTextQuery.setMaxResults(limit);
		Stopwatch stopwatch = new Stopwatch().start();
		final List<?> results = findStatusWithTextQuery.getResultList();
		stopwatch.stop();
		log.trace("Answered query for \"" + filter + "\" " + " within " + stopwatch + " (" + results.size() + " results)");
		return (List<Status>) results;
	}

	@Override
	public List<? extends Status> getAll(){
		return listAll(StatusImpl.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> getStatusFromDatabaseByHashtagCount(int hashtagCount, int limit) {

		final String q = "SELECT status.text FROM StatusImpl status WHERE "
						+ "status.text.length() - status.text.replace(\"#\", \"\").length() = :htc";
		Query qu = em.createQuery(q, String.class);
		qu.setParameter("htc", hashtagCount);	
		qu.setMaxResults(limit);
		return (List<String>) qu.getResultList();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Status> getRandomStatuses(int limit) {
		final String q = "SELECT status FROM StatusImpl status";
		Query qu = em.createQuery(q, Status.class);
		qu.setMaxResults(limit);
		return (List<Status>) qu.getResultList();
	}

	@Override
	public void flush()
	{
		commit();
	}

	@Override
	public void close() {
		flush();
		em.close();
	}
}
