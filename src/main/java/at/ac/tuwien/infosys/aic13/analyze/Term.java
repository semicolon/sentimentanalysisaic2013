package at.ac.tuwien.infosys.aic13.analyze;

import java.util.ArrayList;

/**
 * Term.
 */
public class Term {

	public static final int gAdverb = (1<<0); 
	public static final int gVerb = (1<<1);
	public static final int gNoun = (1<<2);
	public static final int gAdjective = (1<<3);
	public static final int gConjunction = (1<<4);
	public static final int gPronoun = (1<<5);
	public static final int gPreposition = (1<<6);
	// Part of speech.
	public static final int gInterjection = (1<<7);	
	public static final int gPlural = (1<<8);
	public static final int gInitialism = (1<<9);
	public static final int gSymbol = (1<<10);
	
	
	public static final int cVulgar = (1<<0);
	public static final int cSlang = (1<<1);
	public static final int cOffensive = (1<<2);
	
	
	public static final String gABAdverb = "A"; 
	public static final String gABVerb = "V";
	public static final String gABNoun = "N";
	public static final String gABAdjective = "a";
	public static final String gABConjunction = "c";
	public static final String gABPronoun = "P";
	public static final String gABPreposition = "p";
	public static final String gABInterjection = "I";
	public static final String gABPlural = "m";
	public static final String gABInitialism = "i";
	

	public static final String gSAdverb = "Adverb"; 
	public static final String gSVerb = "Verb";
	public static final String gSNoun = "Noun";
	public static final String gSAdjective = "Adjective";
	public static final String gSConjunction = "Conjunction";
	public static final String gSPronoun = "Pronoun";
	public static final String gSPreposition = "Preposition";
	public static final String gSInterjection = "Interjection";
	public static final String gSPlural = "Plural";
	public static final String gSInitialism = "Initialism";
	
	/**
	 * The term's string.
	 */
	public String term;

	/**
	 * Term composition.
	 */
	public ArrayList<String> composition;
	
	/**
	 * Additional properties of the term.
	 */
	public long properties;
	
	/**
	 * String containing the base word.
	 * Only valid if term is gPlural and gNoun.
	 */
	public String plural_of;

	/**
	 * The plural of the word.
	 */
	public String plural;
	
	/**
	 * Only valid if verb.
	 */
	public String present_participle_of;

	/**
	 * The present participle of term.
	 */
	public String present_participle;
	
	/**
	 * Only valid if verb.
	 */
	public String past_participle_of;

	/**
	 * The past participle of the term.
	 */
	public String past_participle;
	
	/**
	 * Comparative term.
	 */
	public String comparative;

	/**
	 * Comparative of.
	 */
	public String comparative_of;
	
	/**
	 * Superlative term.
	 */
	public String superlative;
	
	/**
	 * Superlative of.
	 */
	public String superlative_of;
	
	/**
	 * Term's class.
	 */
	public long tClass;

	/**
	 * Create a new term.
	 */
	public Term(String term, long tClass){
		this.term = term;
		this.tClass = tClass;
	}

	public String getTermType(){
		String res = "";
		ArrayList<String> sep = new ArrayList<String>();
		if((tClass&gAdverb)!=0)
			sep.add(gABAdverb);
		if((tClass&gVerb)!=0)
			sep.add(gABVerb);
		if((tClass&gNoun)!=0)
			sep.add(gABNoun);
		if((tClass&gAdjective)!=0)
			sep.add(gABAdjective);
		if((tClass&gConjunction)!=0)
			sep.add(gABConjunction);
		if((tClass&gPronoun)!=0)
			sep.add(gABPronoun);
		if((tClass&gPreposition)!=0)
			sep.add(gABPreposition);
		if((tClass&gInterjection)!=0)
			sep.add(gABInterjection);
		if((tClass&gPlural)!=0)
			sep.add(gABPlural);
		if((tClass&gInitialism)!=0)
			sep.add(gABInitialism);
		
		if(sep.size() != 0){
			res+=sep.get(0);
			for(int i=1;i<sep.size();i++)
				res+=" "+sep.get(i);
		}
		
		return res;
	}

	public String toLine(){
		String tt = term+"|"+getTermType();
		if(composition!=null){
			if(composition.size()==2){
				//tt+="("+composition.get(0)+" + "+composition.get(1)+")";
			}
		}
		if(plural_of != null)
			tt+="|plo:"+plural_of;
		if(plural != null)
			tt+="|pl:"+plural;
		if(present_participle_of != null)
			tt+="|ppo:"+present_participle_of;
		if(past_participle_of != null)
			tt+="|papo:"+past_participle_of;
		if(present_participle != null)
			tt+="|pp:"+present_participle;
		if(past_participle != null)
			tt+="|pap:"+past_participle;
		if(comparative != null)
			tt+="|c:"+comparative;
		if(superlative != null)
			tt+="|s:"+superlative;
		if(superlative_of != null)
			tt+="|so:"+superlative_of;
		if(comparative_of != null)
			tt+="|co:"+comparative_of;
		return tt;
	}
	
	@Override
	public String toString(){
		String tt = term+"["+getTermType()+"]";
		if(composition!=null){
			if(composition.size()==2){
				tt+="("+composition.get(0)+" + "+composition.get(1)+")";
			}
		}
		if(present_participle_of != null)
			tt+="\n   present_part_of:"+present_participle_of;
		if(past_participle_of != null)
			tt+="\n   past_part_of:"+past_participle_of;
		if(plural_of != null)
			tt+="\n   plural_of:"+plural_of;
		if(plural != null)
			tt+="\n   plural:"+plural;
		if(present_participle != null)
			tt+="\n   present_part:"+present_participle;
		if(past_participle != null)
			tt+="\n   past_part:"+past_participle;
		if(comparative != null && superlative != null)
			tt+="\n   comparative:"+comparative+", superlative:"+superlative;
		if(superlative_of != null)
			tt+="\n   super_of:"+superlative_of;
		if(comparative_of != null)
			tt+="\n   compa_of:"+comparative_of;
		
		if((properties & cVulgar)!=0)
			tt+="\n   VULGAR";
		if((properties & cOffensive)!=0)
			tt+="\n   OFFENSIVE";
		if((properties & cSlang)!=0)
			tt+="\n   SLANG";
		return tt;
	}

}
