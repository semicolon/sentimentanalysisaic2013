package at.ac.tuwien.infosys.aic13.classifier;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import at.ac.tuwien.infosys.aic13.analyze.Main;
import at.ac.tuwien.infosys.aic13.web.Preprocessor.Preprocessor;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;

public class WekaClassifier implements Serializable{

	
	private static final long serialVersionUID = 539728411811295588L;
	private Classifier _cl;
	private Instances _train;
	private Instances _test;

	/*package*/ WekaClassifier() throws Exception {

		Resource train = new ClassPathResource("train1.arff");
		Resource test = new ClassPathResource("test1.arff");

		// correctDuplicateAttributes(train);
		System.out.println("INIT TRAIN DATA");
		DataSource source_train = new DataSource(correctDuplicateAttributes(train));
		System.out.println("INIT TEST DATA");
		DataSource source_test = new DataSource(correctDuplicateAttributes(test));
		
		
		_train = source_train.getDataSet();
		_test = source_test.getDataSet();

		_cl = new NaiveBayes();
	}

	/**
	 * Train Classifier
	 */
	public void trainClassifier() {

		System.out.println("TRAIN START");
		if (_train.classIndex() == -1)
			_train.setClassIndex(_train.numAttributes() - 1);
		try {
			_cl.buildClassifier(_train);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("TRAIN FINISHED");
		// evaluate classifier and print some statistics
		try {
		//	evaluateClassifier();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    /**
     * Evaluate Classifier
     * @throws Exception
     */
	public void evaluateClassifier() throws Exception {
		// evaluate classifier and print some statistics
		if (_test.classIndex() == -1)
			_test.setClassIndex(_test.numAttributes() - 1);
		Evaluation eval = new Evaluation(_train);
		eval.evaluateModel(_cl, _test);
		System.out.println(eval.toSummaryString("\nResults\n======\n", false));
		System.out.println(eval.toMatrixString());
	}
    
	
	public int classify(final String input, Preprocessor preprocessor)
			throws Exception {

		
		String processedInput = preprocessor.preprocessDocument(input);		
//		System.out.print("\"" + input + "\" processed to \"" + processedInput + "\", scored: ");
		
		StringTokenizer string_processed = new StringTokenizer(processedInput, " ");		
		
		Instances unlabelled = new Instances(_train, 1);
		Instance inst = new Instance(unlabelled.numAttributes());
		// load unlabelled data
		inst.setDataset(unlabelled);
		int j = 0;
		while (j < unlabelled.numAttributes()) {
			inst.setValue(j, "0");
			j++;
		}
		while (string_processed.hasMoreTokens()) {
			String tmp = string_processed.nextToken();
			if (unlabelled.attribute(tmp) != null)
				inst.setValue(unlabelled.attribute(tmp), "1");
		}
		unlabelled.add(inst);

		// set class attribute
		unlabelled.setClassIndex(unlabelled.numAttributes() - 1);

		// create copy
		Instances labeled = new Instances(unlabelled);

		// label instances
		for (int i = 0; i < unlabelled.numInstances(); i++) {
			double clsLabel = _cl.classifyInstance(unlabelled.instance(i));
			labeled.instance(i).setClassValue(clsLabel);
		}
		
		//get sentiment value from 0 to 1
		return Integer.parseInt(labeled.instance(0).stringValue(unlabelled.numAttributes() - 1))/4;
	}

	/**
	 * Helper Method to remove duplicate Attributes from sets
	 * @param resource
	 * @return
	 * @throws IOException
	 */
	private static InputStream correctDuplicateAttributes(Resource resource) throws IOException
	{
		final HashMap<String, Integer> duplicateMap = new HashMap<String, Integer>();

		// creates new tmp file
		final File tmpFile = File.createTempFile("aic13-", "-" + resource.getFilename() + ".tmp");
		tmpFile.deleteOnExit();
		
		try (final PrintWriter writer = new PrintWriter(new FileWriter(tmpFile), true); 
			 final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream())))
		{
			String strLine;
			int c = 1;
			while ((strLine = bufferedReader.readLine()) != null)
			{
				StringTokenizer stringTokenizer = new StringTokenizer(strLine, " ");
				String toWrite = strLine;

				if (stringTokenizer.nextToken().equals("@attribute"))
				{
					String attribute = stringTokenizer.nextToken();

					Integer duplicateValue = duplicateMap.get(attribute);

					if (duplicateValue == null)
					{
						duplicateMap.put(attribute, 1);
					}
					else if (duplicateValue >= 1)
					{
						duplicateValue++;
						duplicateMap.put(attribute, duplicateValue);

						if (!stringTokenizer.hasMoreTokens())
						{
							System.out.println("Failure: " + attribute + "  " + String.valueOf(c));
						}

						toWrite = "@attribute " + attribute + "_" + (duplicateValue.toString()) + " " + stringTokenizer.nextToken();
					}
				}

				writer.println(toWrite);
				c++;
			}

			InputStream newInputStream = new FileInputStream(tmpFile);
			return newInputStream;
		}
	}
    /**
     * Print Hashmap
     * @param hm
     */
	public static void printMap(HashMap<String, Integer> hm) {

		for (String key : hm.keySet()) {
			Integer value = hm.get(key);
			if (value > 1) {
				System.out.println(key + ": " + hm.get(key));
			}

		}
	}
	
	/*
	 * Select subset of features
	 */
	public void selectFeaturesSubset(int number) throws Exception {
	    
		System.out.println("SELECT FEATURE SUBSET START: " + String.valueOf(number));
		
		weka.filters.supervised.attribute.AttributeSelection filter = new weka.filters.supervised.attribute.AttributeSelection();
	    
		InfoGainAttributeEval eval = new InfoGainAttributeEval();
	    Ranker search = new Ranker();
	    search.setNumToSelect(number);
	    filter.setEvaluator(eval);
	    filter.setSearch(search);
	    filter.setInputFormat(_train);
	    Instances newData = Filter.useFilter(_train, filter);
	    Instances newData_test = Filter.useFilter(_test, filter);
	    _train = newData;
	    _test = newData_test;
	    System.out.println("SELECT FEATURE SUBSET END");
	    
	}
}
