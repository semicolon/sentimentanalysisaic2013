package at.ac.tuwien.infosys.aic13.analyze.gui;


/**
 * Base interaction class.
 */
public abstract class Interaction {

	public Interaction(){
	}
	
	public void onMouseDown(InteractionMouseEventArgs arg){};
	
	public void onMouseUp(InteractionMouseEventArgs arg){};
	
	public void onMouseMove(InteractionMouseEventArgs arg){};
	
}
