package at.ac.tuwien.infosys.aic13.classify;

import java.util.LinkedList;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SenticClassifier {
	
	private static final Logger log = LoggerFactory.getLogger(SenticClassifier.class);
	private static SenticDatabase db = SenticDatabase.instance();
	
	public static float[] classify(String tweet){
		
		String[] sp = tweet.split(" ");
		LinkedList<SenticSentiment> words = new LinkedList<SenticSentiment>();
		
		float[] results = new float[5];
		
		for(String s: sp){
			SenticSentiment ss = db.getWord(s.toLowerCase());
			if(ss != null){
//				log.trace("FOUND "+s+": "+print(ss.getSentiment()));
				words.add(ss);
				float[] f = ss.getSentiment();
				for(int i = 0; i < f.length; i++){
					if(Math.abs(f[i]) > Math.abs(results[i])){
						results[i] = f[i];
					}
//					if(f[i] > 0){
//						sums[i] += Math.sqrt(f[i]);	
//					} else {
//						sums[i] += (-1)*Math.sqrt((-1)*f[i]);
//					}
				}
			}
		}
		
		/*Mean of single words*/
//		for(int i = 0; i < results.length; i++){
//			results[i] /= words.size();
//		}
		return results;
	}

	public static void main(String[] args){
		
		String tweet = "Microsoft hits iPad Air in new holiday-themed ads geared towards family usage";
		tweet = "UCLA game design students in learning how to use feedback from players, use this link";
		tweet = "fuck this shit";
		tweet = "i like to eat apple pie ;)";
		tweet = "RT @theweeknd: wow just now seeing o2 arena is sold out??? what a way to end the kiss land tour!! love my fans, xo ...";
		
		float[] result = classify(tweet);
		System.out.println("\nRESULT: "+print(result));
		
	}
	
	public static String print(float[] arr){
		String s = String.format(Locale.ENGLISH, "pleasantness: %.3f, "
				+ "attention: %.3f, sensitivity: %.3f, aptitude: %.3f, polarity: %.3f", 
				arr[0], arr[1], arr[2], arr[3], arr[4]);
		
		double count = 0;
		for(int i = 0; i < arr.length; i++){
			count += Math.signum(arr[i]);
		}
		if(Math.signum(count) < 0)
			return "0 - "+s;
		return "1 - "+s;
	}
	
}
