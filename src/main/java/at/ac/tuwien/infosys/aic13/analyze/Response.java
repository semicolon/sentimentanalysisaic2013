package at.ac.tuwien.infosys.aic13.analyze;

import javax.net.ssl.HttpsURLConnection;

/**
 * Html connection response.
 */
public class Response {

	/**
	 * The reponse message.
	 */
	public String msg;

	/**
	 * The error message in case of an error.
	 */
	public String error_msg;

	/**
	 * The https response code.
	 */
	public int response_code;

	/**
	 * Create a new response object.
	 */
	public Response(){
	}

	/**
	 * Check if the reponse is OK.
	 * @return
	 */
	public boolean isOk(){
		return response_code == HttpsURLConnection.HTTP_OK;
	}
	
}
