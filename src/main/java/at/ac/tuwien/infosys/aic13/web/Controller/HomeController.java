package at.ac.tuwien.infosys.aic13.web.Controller;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletContext;

import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ResourceServlet;

import com.google.common.base.Stopwatch;

import twitter4j.FilterQuery;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import at.ac.tuwien.infosys.aic13.classifier.SVMClassifierBuilder;
import at.ac.tuwien.infosys.aic13.classifier.WekaClassifier;
import at.ac.tuwien.infosys.aic13.classify.SenticClassifier;
import at.ac.tuwien.infosys.aic13.db.DBWrapper;
import at.ac.tuwien.infosys.aic13.db.DBWrapperFactory;
import at.ac.tuwien.infosys.aic13.db.StatusImpl;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.ClassifyingAdapterSentic;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.ClassifyingAdapterWeka;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.DBStatusAdapter;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.DebuggingStatusAdapter;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.LocalFilter;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.PrintingStatusAdapter;
import at.ac.tuwien.infosys.aic13.util.Search;
import at.ac.tuwien.infosys.aic13.util.ThumbnailDTO;
import at.ac.tuwien.infosys.aic13.util.ThumbnailManager;
import at.ac.tuwien.infosys.aic13.web.Preprocessor.Preprocessor;

/**
 * Handles requests for the application home page.
 */
@Controller 
public class HomeController{

	private static Date nextStreamStart;
	
	@Autowired
	private  ServletContext servletContext;

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy '-' HH:mm");

	private static WekaClassifier wekaClassifier;
	
	private static Set<String> keywords  = new TreeSet<String>();
	private static FilterQuery filterQuery = new FilterQuery();
	private static TwitterStream twitterStream;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String analyseBySearchValue (
			@RequestParam(value="searchValue", required=false) String searchValue,
			@RequestParam(value="showThumbnails", required=false) Boolean showThumbnails,
			@RequestParam(value="classifier", required=false) String classifier,
			Locale locale, Model model) throws Exception {
		
		if(searchValue == null || searchValue.equals(""))
			return "home";
		
		if(classifier == null)
			classifier = "sentic";
		if(!classifier.equals("sentic") && !classifier.equals("weka"))
			classifier = "sentic";
	
		if(twitterStream == null)
		{
			twitterStream = new TwitterStreamFactory().getInstance();
			twitterStream.addListener(new DebuggingStatusAdapter());
			twitterStream.addListener(new DBStatusAdapter());
			//twitterStream.addListener(new PrintingStatusAdapter());
			filterQuery.language(new String[]{"en"});
		}
		
		if(keywords.add(searchValue))
		{			
			//only restart stream once a minute, to avoid rate limiting
			//TODO as timer?
			if(nextStreamStart == null || nextStreamStart.after(new Date()))
			{
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MINUTE, 1);
				nextStreamStart = cal.getTime();
				twitterStream.shutdown();
				filterQuery.track(keywords.toArray(new String[keywords.size()]));
				twitterStream.filter(filterQuery);
			}
		}
		
		if(log.isInfoEnabled())
		{
			Iterator<String> keywordIter = keywords.iterator();
			String info = "(Will be) tracking " + keywords.size() + " keywords: " + keywordIter.next();
			while(keywordIter.hasNext())
			{
				info += ", " + keywordIter.next();
			}
			log.info(info);
			System.out.println(info);
		}
		

		
		//Download latest tweets

		List<Status> tweets = null;
		DBWrapper db = DBWrapperFactory.getInstance();
		try {
			Query query = new Query();
			query.setLang("en");
			query.setQuery(searchValue);
			query.setCount(1000);

			QueryResult result;
			result = TwitterFactory.getSingleton().search(query);
			System.out.println();
			tweets = result.getTweets();
			log.debug("Found " + tweets.size() + " tweets online");
			Iterator<Status> it = tweets.iterator();
			while(it.hasNext())
			{
				Status stat = it.next();
				if(!LocalFilter.useTweet(stat))
				{
					it.remove();
				}
			}

			log.debug("Found " + tweets.size() + " valid tweets online");
			//Store latest tweets in db
			for (Status status : tweets)
			{
				if (status.isRetweet())
					db.save(StatusImpl.fromStatus(status.getRetweetedStatus()), true);
				else
					db.save(StatusImpl.fromStatus(status), true);
			}
		} catch(TwitterException e){
			System.out.println("propably rate limited! "+e.getMessage());
		}

		tweets = db.getAllStatusWithText(searchValue, 1000); 
		log.debug("Found " + tweets.size() + " tweets total");

		Preprocessor preprocessor = new Preprocessor();		

		String resultString = null;
		
		ArrayList<Float> results = new ArrayList<Float>(10);

		if(classifier.equals("weka")){

			WekaClassifier wc = SVMClassifierBuilder.prepareClassifier();	
			Stopwatch classifierStopwatch = new Stopwatch().start();
			Mean meanWeka = new Mean();

			for(int i = 0; i < tweets.size(); i++)
			{
				Status status = tweets.get(i);

				int wekaResult = wc.classify(status.getText(), preprocessor);
				meanWeka.increment(wekaResult);
				results.add((float) wekaResult);

				if(i < 25)
				{
					System.out.println("-----------------------------------------------------");
					System.out.println(status.getUser().getScreenName() + ":\n" + status.getText());
					System.out.println(preprocessor.preprocessDocument(status.getText()));
					System.out.println();
					System.out.println("Bag of words (Weka):\t" + wekaResult);
				}
			}
			System.out.println(tweets.size() + " tweets analysed in " + classifierStopwatch);
			System.out.println(String.format(Locale.ENGLISH, "Avg (weka): %.3f", meanWeka.getResult()));
			resultString = String.format(Locale.ENGLISH, "%d tweets analysed.\nWeka: %.3f",
					tweets.size(), meanWeka.getResult());
		}

		else if(classifier.equals("sentic")) {

			double[][] sentic2Results = new double[5][tweets.size()];

			for(int i = 0; i < tweets.size(); i++)
			{
				Status status = tweets.get(i);
				float[] sentic2Result = SenticClassifier.classify((status.getText()));
				for(int j = 0; j < 5; j++)
				{
					sentic2Results[j][i] = sentic2Result[j];
				}
				
				float res = 0f;
				for(int j = 0; j < sentic2Result.length; j++){
					res += sentic2Result[j]+1;
				}
				res /= 10;
				results.add(res);

				if(i < 25)
				{
					System.out.println("-----------------------------------------------------");
					System.out.println(status.getUser().getScreenName() + ":\n" + status.getText());
					System.out.println(preprocessor.preprocessDocument(status.getText()));
					System.out.println();
					System.out.println("Bag of words (Sentic2):\t" + SenticClassifier.print(sentic2Result));
				}
			}
			Mean avg = new Mean();
			float[] senticResult = new float[]{
					(float)avg.evaluate(sentic2Results[0]),
					(float)avg.evaluate(sentic2Results[1]),
					(float)avg.evaluate(sentic2Results[2]),
					(float)avg.evaluate(sentic2Results[3]),
					(float)avg.evaluate(sentic2Results[4]),
			};
			float res = 0f;
			for(int i = 0; i < senticResult.length; i++){
				res += senticResult[i];
			}
			res /= senticResult.length;
			//Now res is between -1 and 1
			res += 1;
			res /= 2; 
			//Now res is between 0 and 1

			System.out.printf("Avg (Sentic2): %.3f\n",res);
			resultString = String.format(Locale.ENGLISH, "%d tweets analysed.\nSentic: %.3f",
					tweets.size(),res);
		}

		model.addAttribute("resultString", resultString);
		
		
		List<String> li = new LinkedList<String>();

		for(int i = 0; i < 25 && i < tweets.size(); i++){
			StringBuilder builder = new StringBuilder();
			String userScreenName = tweets.get(i).getUser().getScreenName();
			builder.append("<div style=\"background-color: #");
			if(results.get(i) < 0.2)
				builder.append("F78181"); 
			else if (results.get(i) < 0.4)
				builder.append("F7BE81"); 
			else if (results.get(i) < 0.6)
				builder.append("F3F781"); 
			else if (results.get(i) < 0.8)
				builder.append("BEF781");
			else 
				builder.append("81F781"); 
			
			builder.append("; color:#");
			if(results.get(i) < 0.2)
				builder.append("610B0B"); 
			else if (results.get(i) < 0.4)
				builder.append("61380B"); 
			else if (results.get(i) < 0.6)
				builder.append("5E610B"); 
			else if (results.get(i) < 0.8)
				builder.append("38610B");
			else 
				builder.append("0B610B"); 
			
			builder.append("\">");
			builder.append("<a href=\"https://twitter.com/"+userScreenName+"\" target=\"_blank\">@"+userScreenName+"</a>: ");
			builder.append(tweets.get(i).getText());
			builder.append(String.format(" - %.3f", results.get(i)));
			builder.append("</div>");
			li.add(builder.toString());
		}
		model.addAttribute("tweets", li);
		return "home";

	}
}
