package at.ac.tuwien.infosys.aic13.analyze.html;

import java.util.ArrayList;

/**
 * Base element.
 */
public abstract class Element {
	
	/**
	 * Elements name.
	 */
	public String name;
	
	/**
	 * Ctor.
	 */
	public Element(){
	}
	
	/**
	 * Ctor.
	 * @param name
	 */
	public Element(String name){
		this.name = name;
	}
	
	/**
	 * Flatten element into a single text element.
	 * @return The flattened text element.
	 */
	public abstract void flatten(ArrayList<TextElement> list);
	
	public abstract ArrayList<LinkToken> getAllLinks();
	
}
