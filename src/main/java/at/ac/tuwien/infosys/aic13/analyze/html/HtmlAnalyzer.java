package at.ac.tuwien.infosys.aic13.analyze.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.htmlcleaner.ContentNode;
import org.htmlcleaner.TagNode;

/**
 * Simple HTML analyzer.
 */
public class HtmlAnalyzer {


	/**
	 * Create a new html analyzer.
	 */
	public HtmlAnalyzer(){
	}

	public TextElement getPureText(Object node){
		ArrayList<Object> oos = new ArrayList<Object>();
		oos.add(node);
		return getPureText(oos);
	}
	
	/**
	 * Convert elements to plain text.
	 * @param elements
	 * @return The resulting text element.
	 */
	public TextElement getPureText(List elements){
		TextElement te = new TextElement();
		return getPureText(te,elements);
	}

	public TextElement getPureText(TextElement te, List elements){
		getPureTextRec(null, te, elements);
		return te;
	}

	private void getPureTextRec(String act_link, TextElement te, List elems){
		for(int i = 0;i<elems.size();i++){
			Object elem = elems.get(i);
			if(elem instanceof ContentNode){
				ContentNode cn = (ContentNode)elem;
				String cont = cn.getContent().toString();
				if(act_link == null)
					te.addText(cont);
				else
					te.addLink(cont,act_link);
			}else if(elem instanceof TagNode){
				TagNode tn = (TagNode)elem;
				String link = null;
				if(tn.getName().equals("a")){
					// Link element.
					link = tn.getAttributeByName("href");
				}else{/* IGNORE other tags */}
				getPureTextRec(link, te, tn.getAllChildren());
			}
		}
	}


	public DefListElement getDefinitionList(TagNode dl){
		DefListElement le = null;

		String name = dl.getName();
		if(name.equals("dl")){
			// unordered list.
			le = new DefListElement();
		}else{
			// Not a valid list element.
			return null;
		}

		// Get list items.
		List chs = dl.getAllChildren();
		for(int i=0;i<chs.size();i++){
			Object ch = chs.get(i);
			if(ch instanceof TagNode){
				TagNode tn = (TagNode)ch;
				if(tn.getName().equals("dd") || tn.getName().equals("dt"))
					le.addItem(getDefItem(tn));
			}
		}

		return le;
	}

	public DefListItemElement getDefItem(TagNode def){
		DefListItemElement di = null;

		if(def.getName().equals("dd")){
			di = new DefListItemElement(false);
		}else if(def.getName().equals("dt")){
			di = new DefListItemElement(true);
		}else{
			return null;
		}
		ArrayList<Element> elems = getContent(def.getAllChildren());
		di.addContent(elems);
		return di;
	}

	public ListElement getList(TagNode list){
		ListElement le = null;

		String name = list.getName();
		if(name.equals("ul")){
			// unordered list.
			le = new ListElement(false);
		}else if(name.equals("ol")){
			// ordered list.
			le = new ListElement(true);
		}else{
			// Not a valid list element.
			return null;
		}

		// Get list items.
		List chs = list.getElementListByName("li", false);
		for(int i=0;i<chs.size();i++){
			le.addItem(getListItem((TagNode)chs.get(i)));
		}

		return le;
	}

	public ListItemElement getListItem(TagNode li){
		ListItemElement it = new ListItemElement();

		if(!li.getName().equals("li"))
			return null; // Not a valid list item.

		ArrayList<Element> elems = getContent(li.getAllChildren());
		it.addContent(elems);
		return it;
	}


	public ArrayList<Element> getContent(List elements){
		ArrayList<Element> res = new ArrayList<Element>(); 
		TextElement active_text = null;

		for(int i = 0;i<elements.size();i++){
			Object elem = elements.get(i);
			if(elem instanceof ContentNode){
				ContentNode cn = (ContentNode)elem;
				String cont = cn.getContent().toString();
				if(!cont.trim().equals("")){
					if(active_text == null){
						active_text = new TextElement();
					}
					active_text.addText(cont);
				}
			}else if(elem instanceof TagNode){
				TagNode node = (TagNode)elem;
				String name = node.getName();
				boolean stop_text = true;
				if(name.equals("p")){
					// Paragraph.
					// Only text.
					ParagraphElement pg = new ParagraphElement();
					TextElement te = getPureText(node.getAllChildren());
					pg.addContent(te);
					res.add(pg);
				}else if(name.equals("ol") || name.equals("ul")){
					// List
					ListElement li = getList(node);
					res.add(li);
				}else if(name.equals("dl")){
					// Definition list.
					DefListElement li = getDefinitionList(node);
					res.add(li);
				}else{
					// Flatten to plain text.
					List<Object> nl = new ArrayList<Object>();
					if(active_text == null){
						active_text = new TextElement();
					}
					getPureText(active_text,node.getAllChildren());
					stop_text = false;
				}

				if(stop_text){
					if(active_text != null){
						// Filter empty text passages.
						String tt = active_text.toString();
						if(!tt.trim().equals("")){
							res.add(active_text);
						}
					}
					active_text = null;
				}
			}
		}


		if(active_text != null){
			// Filter empty text passages.
			String tt = active_text.toString();
			if(!tt.trim().equals("")){
				res.add(active_text);
			}
		}

		return res;
	}


	/**
	 * Convert to text and hyperlink description.
	 */
	public static String cvtTxtHype(ArrayList<Element> elems){
		// Flatten elements into pure text|hyperlink structure.
		ArrayList<TextElement> ts = new ArrayList<TextElement>();
		for(int i=0;i<elems.size();i++){
			Element el = elems.get(i);
			el.flatten(ts);
		}
		HashMap<Integer, String> links = new HashMap<Integer, String>();
		String str = "";
		int str_pos = 0;
		for(int j=0;j<ts.size();j++){
			if(j!=0)
				str+="\n";
			TextElement el = ts.get(j);
			for(int k = 0;k<el.getContent().size();k++){
				TextToken tt = el.getContent().get(k);
				if(tt instanceof LinkToken){
					LinkToken lt = (LinkToken)tt;
					str+=tt.text;
					links.put(str_pos, lt.link);
					str_pos+=tt.text.length();
				}else{
					str+=tt.text;
					str_pos+=tt.text.length();
				}
			}
		}
		return str;
	}


	/**
	 * Find pattern in list of elements.
	 */
	public static ArrayList<TextElement> findPattern(String pat, ArrayList<Element> elems){
		ArrayList<TextElement> matches = new ArrayList<TextElement>();
		// Flatten elements into text sections.
		ArrayList<TextElement> ts = new ArrayList<TextElement>();
		for(int i=0;i<elems.size();i++){
			elems.get(i).flatten(ts);
		}

		// Find pattern in text sections.
		for(int i=0;i<ts.size();i++){
			TextElement te = ts.get(i);
			String str = te.toString();
			if(str.contains(pat)){
				matches.add(te);
			}
		}
		return matches;
	}

	/**
	 * Get content of node.
	 * @param node
	 */
	public ArrayList<Element> getContent(TagNode node){
		ArrayList<Object> ls = new ArrayList<Object>();
		ls.add(node);
		return getContent(ls);
	}

}
