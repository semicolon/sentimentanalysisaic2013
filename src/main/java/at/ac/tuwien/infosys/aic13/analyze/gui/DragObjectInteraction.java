package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * Drag object interaction.
 */
public class DragObjectInteraction extends Interaction{

	private Point init_pos;
	
	private Canvas canvas;
	
	private ArrayList<SceneObject> objects;
	
	private ArrayList<Point2D.Float> opos;
	
	/**
	 * CTor.
	 */
	public DragObjectInteraction(Canvas canvas, ArrayList<SceneObject> objs, Point pos){
		this.canvas = canvas;
		this.init_pos = pos;
		this.objects = objs;
		this.opos = new ArrayList<Point2D.Float>();
		for(int i=0;i<objs.size();i++)
			opos.add(new Point2D.Float(objs.get(i).bounds.x,objs.get(i).bounds.y));
	}
	
	@Override
	public void onMouseDown(InteractionMouseEventArgs arg){
		
	}
	
	@Override
	public void onMouseUp(InteractionMouseEventArgs arg){
		
	}
	
	@Override
	public void onMouseMove(InteractionMouseEventArgs arg){
		
		Point np = arg.me.getPoint();

		float iz = 1f/arg.canvas.getActiveCamera().getZoom();
		
		float dx = (np.x-init_pos.x)*iz;
		float dy = (np.y-init_pos.y)*iz;

		for(int i=0;i<objects.size();i++)
			objects.get(i).setTranslation(opos.get(i).x+dx, opos.get(i).y+dy);
		
		canvas.invalidate();
		canvas.repaint();
	}
	
}
