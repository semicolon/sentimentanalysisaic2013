package at.ac.tuwien.infosys.aic13.analyze;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class VocabularyEngine {

	/**
	 * Collected terms.
	 */
	public  HashMap<String, Term> terms;

	/**
	 * Collected synonyms.
	 */
	public  HashMap<String, SynonymData> synonyms;

	/**
	 * The wiki crawler.
	 */
	private WikiCrawler wc;

	/**
	 * The thesaurus crawler.
	 */
	private ThesaurusCrawler tc;

	private PrintWriter wpw;

	private File word_file;

	private PrintWriter spw;

	private File syn_file;

	/**
	 * Ctor.
	 */
	public VocabularyEngine(){
		terms = new HashMap<String, Term>();
		synonyms = new HashMap<String, SynonymData>();
		wc = new WikiCrawler();
		tc = new ThesaurusCrawler();
	}

	/**
	 * load 
	 */
	public void loadWords(InputStream is){

		String line = null;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			while((line = br.readLine())!=null){
				String[] toks = line.split("\\|");
				if(toks.length>0){
					long ct = 0;
					String word = toks[0];
					// Parse Description
					if(toks.length >1){
						String[] descr = toks[1].split(" ");
						for(int i=0;i<descr.length;i++){
							if(descr[i].equals("A")){
								ct|= Term.gAdverb;
							}else if(descr[i].equals("V")){
								ct|= Term.gVerb;
							}else if(descr[i].equals("N")){
								ct|= Term.gNoun;
							}else if(descr[i].equals("a")){
								ct|= Term.gAdjective;
							}else if(descr[i].equals("c")){
								ct|= Term.gConjunction;
							}else if(descr[i].equals("P")){
								ct|= Term.gPronoun;
							}else if(descr[i].equals("p")){
								ct|= Term.gPreposition;
							}else if(descr[i].equals("I")){
								ct|= Term.gInterjection;
							}else if(descr[i].equals("m")){
								ct|= Term.gPlural;
							}else if(descr[i].equals("i")){
								ct|= Term.gInitialism;
							}
						}
					}
					Term t = new Term(word,ct);
					for(int i=2;i<toks.length;i++){
						String[] ss = toks[i].split(":");
						if(ss.length == 2){
							String cat = ss[0];

							if(cat.equals("plo")){
								t.plural_of = ss[1];
							}else if(cat.equals("pl")){
								t.plural = ss[1];
							}else if(cat.equals("ppo")){
								t.present_participle_of = ss[1];
							}else if(cat.equals("papo")){
								t.past_participle_of = ss[1];
							}else if(cat.equals("pp")){
								t.present_participle = ss[1];
							}else if(cat.equals("pap")){
								t.past_participle = ss[1];
							}else if(cat.equals("c")){
								t.comparative = ss[1];
							}else if(cat.equals("s")){
								t.superlative = ss[1];
							}else if(cat.equals("co")){
								t.comparative_of = ss[1];
							}else if(cat.equals("so")){
								t.superlative_of = ss[1];
							}
						}
					}
					terms.put(t.term, t);
				}
			}
			br.close();
		} catch (IOException e) {
		}
	}

	/**
	 * Load synonym from file.
	 */
	public void loadSynonyms(InputStream is){
		String line = null;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			while((line = br.readLine())!=null){
				String[] toks = line.split("\\|");
				if(toks.length > 0){
					SynonymData sd = new SynonymData();
					String word = toks[0];
					sd.term = word;
					// Read synonyms.
					String[] syns = toks[2].split("\\;");
					for(int i=0;i<syns.length;i++){
						String[] wr = syns[i].split("\\#");
						int rel = 0;
						if(wr.length == 2)
							rel = Integer.parseInt(wr[1]);
						if(wr.length>0){
							sd.addSynonym(wr[0], rel);
						}
					}
					if(toks.length==4){
						// Read antonyms.
						String[] ants = toks[3].split("\\;");
						for(int i=0;i<ants.length;i++){
							String[] wr = ants[i].split("\\#");
							int rel = 0;
							if(wr.length == 2)
								rel = Integer.parseInt(wr[1]);
							if(wr.length>0){
								sd.addAntonym(wr[0], rel);
							}
						}
					}
					synonyms.put(sd.term, sd);
				}
			}
			br.close();
		} catch (IOException e) {
		}
	}

	/**
	 * Tokenize given text.
	 * @param text
	 * @return
	 */
	private String[] tokenize(String text){
		String[] res = text.split("\\.| |\\(|\\)|\\,|\\;|\\'|\\!|\\?|\\´|\\`|\\‘|\\’|\\/|\\n");
		return res;
	}


	/**
	 * Get synonyms and antonyms for given word.
	 * @param word
	 */
	public void getSynonymsFromWord(String word){
		if(synonyms.containsKey(word))
			return; // Nothing to do.
		SynonymData sd = tc.searchTerm(word);
		if(sd.synonyms.size() != 0){
			try {
				syn_file = new File("/home/stofde/java_code/TextAnalyze/src/syns.txt");
				spw = new PrintWriter(new BufferedWriter(new FileWriter(syn_file, true)));
				spw.println(sd.toLine());
				spw.flush();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			synonyms.put(word, sd);
		}
	}

	public void getWords(String[] words){
		Queue<String> qs = new LinkedList<String>();

		for(int i=0;i<words.length;i++)
			qs.add(words[i]);

		while(qs.size() != 0){
			String t = qs.poll();
			t = t.toLowerCase().trim();
			if(t.equals(""))
				continue;
			System.out.println("search:"+t+" left["+qs.size()+"]");
			if(!terms.containsKey(t)){
				// New term.
				t = t.toLowerCase();
				Term ts = wc.searchTerm(t);
				if(ts != null){

					try {
						word_file = new File("/home/stofde/java_code/TextAnalyze/src/words.txt");
						wpw = new PrintWriter(new BufferedWriter(new FileWriter(word_file, true)));
						wpw.println(ts.toLine());
						wpw.flush();
						wpw.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}

					// Term found.
					// Search synonyms.
					getSynonymsFromWord(ts.term);
					
					
					if(ts.plural_of != null){
						if(!terms.containsKey(ts.plural_of)){
							//qs.add(ts.plural_of);
						}
					}

					if(ts.comparative_of != null){
						if(!terms.containsKey(ts.comparative_of)){
							//qs.add(ts.comparative_of);
						}
					}

					if(ts.superlative_of != null){
						if(!terms.containsKey(ts.superlative_of)){
							//qs.add(ts.superlative_of);
						}
					}

					if(ts.present_participle_of != null){
						if(!terms.containsKey(ts.present_participle_of)){
							qs.add(ts.present_participle_of);
						}
					}

					if(ts.past_participle_of != null){
						if(!terms.containsKey(ts.past_participle_of)){
							qs.add(ts.past_participle_of);
						}
					}

					if(ts.present_participle != null){
						if(!terms.containsKey(ts.present_participle)){
							//qs.add(ts.present_participle);
						}
					}

					if(ts.past_participle != null){
						if(!terms.containsKey(ts.past_participle)){
							//qs.add(ts.past_participle);
						}
					}



					// Check composistion.
					if(ts.composition != null){
						// Filter prefix and suffix.
						String s1 = ts.composition.get(0);
						String s2 = ts.composition.get(1);

						int s1_type = 0;
						int s2_type = 0;

						if(s1.startsWith("-")){
							// s1 suffix fail
							s1_type = 1;
						}else if(s1.endsWith("-")){
							// s1 prefix.
							s1_type = 2;
						}

						if(s2.startsWith("-")){
							// s2 suffix.
							s2_type = 1;
						}else if(s2.endsWith("-")){
							// s2 prefix fail.
							s2_type = 2;
						}

						if(s1_type == 0){
							// new term.
							qs.add(s1);
						}
						if(s2_type == 0){
							// new term.
							qs.add(s2);
						}
					}

					terms.put(t, ts);
				}else{
					// term not found.
				}
			}
		}

		if(wpw != null)
			wpw.close();
		if(spw != null)
			spw.close();
	}

	/**
	 * Extract and validate words form given text.
	 * @param text The text to extract from.
	 */
	public void getWordsFromText(String text){
		// Split text into tokens.
		String[] toks = tokenize(text);
		// TODO: validate tokens.
		getWords(toks);
	}

}
