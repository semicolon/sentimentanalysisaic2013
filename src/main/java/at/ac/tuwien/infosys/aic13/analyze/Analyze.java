package at.ac.tuwien.infosys.aic13.analyze;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Analyze {

	public static void main(String[] args){
		Analyze a = new Analyze();
		double c1 = a.calcDiceCoff("reference","rampage",4);
		double c2 = a.calcDiceCoff("reference","referencing",4);
		System.out.println(c1+" "+c2);
	}

	/**
	 * Create analyzer.
	 */
	public Analyze(){
	}


	public HashMap<String, Long> buildMultigram(String w, int gram){
		HashMap<String, Long> grams = new HashMap<String, Long>();
		int start = (-gram)+1;
		while(start<w.length()){
			byte[] bs = new byte[gram];
			for(int i=0;i<gram;i++){
				if((start+i) < 0)
					bs[i] = 0x00;
				else if((start+i) >= w.length())
					bs[i] = 0x00;
				else
					bs[i] = (byte)w.charAt(start+i);
			}
			String s = new String(bs);
			if(grams.containsKey(s)){
				grams.put(s, grams.get(s)+1);
			}else{
				grams.put(s, 1L);
			}
			start++;
		}
		return grams;
	}



	public double calcDiceCoff(String w1, String w2, int gram){
		HashMap<String, Long> m1 = buildMultigram(w1,gram);
		HashMap<String, Long> m2 = buildMultigram(w2,gram);
		Iterator<String> vals = m1.keySet().iterator();
		ArrayList<String> inter = new ArrayList<String>();
		while(vals.hasNext()){
			String term = vals.next();
			if(m2.containsKey(term)){
				inter.add(term);
			}
		}
		return (2*inter.size())/((double)(m1.size()+m2.size()));
	}

	/**
	 * Get term id.
	 * @param str
	 * @return
	 */
	public long getTermId(String str){
		long id = 0;
		for(int i=0;i<str.length();i++){
			char c = str.charAt(i); // 16 bit.
			int cvt = (int)c;
			id|=(cvt<<(i*8));
		}
		return id;
	}

}
