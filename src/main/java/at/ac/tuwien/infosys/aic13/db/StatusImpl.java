package at.ac.tuwien.infosys.aic13.db;

import java.util.Date;

import javax.jdo.annotations.Index;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import twitter4j.GeoLocation;
import twitter4j.HashtagEntity;
import twitter4j.MediaEntity;
import twitter4j.Place;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.SymbolEntity;
import twitter4j.URLEntity;
import twitter4j.User;
import twitter4j.UserMentionEntity;

@Entity
public class StatusImpl implements Status
{
	private static final long serialVersionUID = 7548618898682727465L;

	public static StatusImpl fromStatus(Status orig)
	{
		if (orig == null)
			return null;

		// return convert(orig, StatusImpl.class);
		final StatusImpl status = new StatusImpl();
		status.contributorsIDs = orig.getContributors();
		status.setCreatedAt(orig.getCreatedAt());
		status.setCurrentUserRetweetId(orig.getCurrentUserRetweetId());
		status.setFavoriteCount(orig.getFavoriteCount());
		status.setFavorited(orig.isFavorited());
		status.setGeoLocation(orig.getGeoLocation());
		status.setHashtagEntities(orig.getHashtagEntities());
		status.setId(orig.getId());
		status.setInReplyToScreenName(orig.getInReplyToScreenName());
		status.setInReplyToStatusId(orig.getInReplyToStatusId());
		status.setInReplyToUserId(orig.getInReplyToUserId());
		status.setIsoLanguageCode(orig.getIsoLanguageCode());
		status.setMediaEntities(orig.getMediaEntities());
		status.setPlace(orig.getPlace());
		status.setPossiblySensitive(orig.isPossiblySensitive());
		status.setRetweetCount(orig.getRetweetCount());
		status.setRetweeted(orig.isRetweeted());
		status.setRetweetedStatus(orig.getRetweetedStatus());
		status.setSource(orig.getSource());
		status.setSymbolEntities(orig.getSymbolEntities());
		status.setText(orig.getText());
		status.setTruncated(orig.isTruncated());
		status.urlEntities = orig.getURLEntities();
		status.setUser(orig.getUser());
		status.setUserMentionEntities(orig.getUserMentionEntities());
		return status;
	}

	private long[] contributorsIDs;
	
	@Index
	private Date createdAt;

	private long currentUserRetweetId = -1L;
	private int favoriteCount;
	@Transient
	private GeoLocation geoLocation = null;
	@Transient
	private HashtagEntity[] hashtagEntities;
	@Id
	private long id;
	private String inReplyToScreenName;
	private long inReplyToStatusId;
	private long inReplyToUserId;
	private boolean isFavorited;
	private String isoLanguageCode;
	private boolean isPossiblySensitive;
	private boolean isRetweeted;
	private boolean isTruncated;
	@Transient
	private MediaEntity[] mediaEntities;

	@Transient
	private Place place = null;

	// this field should be int in theory, but left as long for the serialized
	// form compatibility - TFJ-790
	private long retweetCount;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private StatusImpl retweetedStatus;
	private String source;
	@Transient
	private SymbolEntity[] symbolEntities;
	
	@Index
	private String text;
	@Transient
	private URLEntity[] urlEntities;
	@ManyToOne(cascade=CascadeType.MERGE, fetch=FetchType.EAGER, optional=false)
	private UserImpl user;

	@Transient
	private UserMentionEntity[] userMentionEntities;

	@Override
	public int compareTo(Status that)
	{
		if(that == null)
			throw new NullPointerException();

		long delta = this.id - that.getId();
		if (delta < 0)
		{
			return -1;
		}
		else if (delta > 1)
		{
			return 1;
		}
		return 0;
	}


	@Override
	public int getAccessLevel()
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long[] getContributors()
	{
		return contributorsIDs;
	}

	public long[] getContributorsIDs()
	{
		return contributorsIDs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date getCreatedAt()
	{
		return this.createdAt;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getCurrentUserRetweetId()
	{
		return currentUserRetweetId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFavoriteCount()
	{
		return favoriteCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GeoLocation getGeoLocation()
	{
		return geoLocation;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HashtagEntity[] getHashtagEntities()
	{
		return hashtagEntities;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getId()
	{
		return this.id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInReplyToScreenName()
	{
		return inReplyToScreenName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getInReplyToStatusId()
	{
		return inReplyToStatusId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getInReplyToUserId()
	{
		return inReplyToUserId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getIsoLanguageCode()
	{
		return isoLanguageCode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaEntity[] getMediaEntities()
	{
		return mediaEntities;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Place getPlace()
	{
		return place;
	}

	@Override
	public RateLimitStatus getRateLimitStatus()
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getRetweetCount()
	{
		return (int) retweetCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Status getRetweetedStatus()
	{
		return retweetedStatus;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getSource()
	{
		return this.source;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SymbolEntity[] getSymbolEntities()
	{
		return symbolEntities;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getText()
	{
		return this.text;
	}

	public URLEntity[] getUrlEntities()
	{
		return urlEntities;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public URLEntity[] getURLEntities()
	{
		return urlEntities;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public User getUser()
	{
		return user;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserMentionEntity[] getUserMentionEntities()
	{
		return userMentionEntities;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isFavorited()
	{
		return isFavorited;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPossiblySensitive()
	{
		return isPossiblySensitive;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRetweet()
	{
		return retweetedStatus != null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRetweeted()
	{
		return isRetweeted;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRetweetedByMe()
	{
		return currentUserRetweetId != -1L;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isTruncated()
	{
		return isTruncated;
	}

	public void setContributorsIDs(long[] contributorsIDs)
	{
		this.contributorsIDs = contributorsIDs;
	}

	public void setCreatedAt(Date createdAt)
	{
		this.createdAt = createdAt;
	}

	public void setCurrentUserRetweetId(long currentUserRetweetId)
	{
		this.currentUserRetweetId = currentUserRetweetId;
	}

	public void setFavoriteCount(int favoriteCount)
	{
		this.favoriteCount = favoriteCount;
	}

	public void setFavorited(boolean isFavorited)
	{
		this.isFavorited = isFavorited;
	}

	public void setGeoLocation(GeoLocation geoLocation)
	{
		this.geoLocation = geoLocation;
	}

	public void setHashtagEntities(HashtagEntity[] hashtagEntities)
	{
		this.hashtagEntities = hashtagEntities;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public void setInReplyToScreenName(String inReplyToScreenName)
	{
		this.inReplyToScreenName = inReplyToScreenName;
	}

	public void setInReplyToStatusId(long inReplyToStatusId)
	{
		this.inReplyToStatusId = inReplyToStatusId;
	}

	public void setInReplyToUserId(long inReplyToUserId)
	{
		this.inReplyToUserId = inReplyToUserId;
	}

	public void setIsoLanguageCode(String isoLanguageCode)
	{
		this.isoLanguageCode = isoLanguageCode;
	}

	public void setMediaEntities(MediaEntity[] mediaEntities)
	{
		this.mediaEntities = mediaEntities;
	}

	public void setPlace(Place place)
	{
		this.place = place;
	}

	public void setPossiblySensitive(boolean isPossiblySensitive)
	{
		this.isPossiblySensitive = isPossiblySensitive;
	}

	public void setRetweetCount(long retweetCount)
	{
		this.retweetCount = retweetCount;
	}

	public void setRetweeted(boolean isRetweeted)
	{
		this.isRetweeted = isRetweeted;
	}

	public void setRetweetedStatus(Status retweetedStatus)
	{
		this.retweetedStatus = StatusImpl.fromStatus(retweetedStatus);
	}

	public void setRetweetedStatus(StatusImpl retweetedStatus)
	{
		this.retweetedStatus = retweetedStatus;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public void setSymbolEntities(SymbolEntity[] symbolEntities)
	{
		this.symbolEntities = symbolEntities;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public void setTruncated(boolean isTruncated)
	{
		this.isTruncated = isTruncated;
	}

	public void setUrlEntities(URLEntity[] urlEntities)
	{
		this.urlEntities = urlEntities;
	}

	public void setUser(User user)
	{
		this.user = UserImpl.fromStatus(user);
	}

	public void setUser(UserImpl user)
	{
		this.user = user;
	}

	public void setUserMentionEntities(UserMentionEntity[] userMentionEntities)
	{
		this.userMentionEntities = userMentionEntities;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if(other == null)
			return false;
		
		if(!(other instanceof Status))
			return false;

		Status otherStatus = (Status) other;
		return otherStatus.getId() == id;
	}
	
	@Override
	public int hashCode()
	{
		return Long.valueOf(id).hashCode();
	}

	@Override
	public String toString()
	{
		return "@" + getUser().getScreenName() + ": " + getText();
	}

	// @Override
	// public String toString() {
	// return "StatusJSONImpl{" +
	// "createdAt=" + createdAt +
	// ", id=" + id +
	// ", text='" + text + '\'' +
	// ", source='" + source + '\'' +
	// ", isTruncated=" + isTruncated +
	// ", inReplyToStatusId=" + inReplyToStatusId +
	// ", inReplyToUserId=" + inReplyToUserId +
	// ", isFavorited=" + isFavorited +
	// ", isRetweeted=" + isRetweeted +
	// ", favoriteCount=" + favoriteCount +
	// ", inReplyToScreenName='" + inReplyToScreenName + '\'' +
	// ", geoLocation=" + geoLocation +
	// ", place=" + place +
	// ", retweetCount=" + retweetCount +
	// ", isPossiblySensitive=" + isPossiblySensitive +
	// ", isoLanguageCode=" + isoLanguageCode +
	// ", contributorsIDs=" + contributorsIDs +
	// ", retweetedStatus=" + retweetedStatus +
	// ", userMentionEntities=" + (userMentionEntities == null ? null :
	// Arrays.asList(userMentionEntities)) +
	// ", urlEntities=" + (urlEntities == null ? null :
	// Arrays.asList(urlEntities)) +
	// ", hashtagEntities=" + (hashtagEntities == null ? null :
	// Arrays.asList(hashtagEntities)) +
	// ", mediaEntities=" + (mediaEntities == null ? null :
	// Arrays.asList(mediaEntities)) +
	// ", currentUserRetweetId=" + currentUserRetweetId +
	// ", user=" + user +
	// '}';
	// }
}
