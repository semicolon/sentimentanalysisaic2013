package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;

import javax.swing.JPanel;

/**
 * Canvas.
 */
public class Canvas extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener{


	public enum RenderChannel{FIRST,PREOBJECT,OBJECT,POSTOBJECT,INTERFACE};


	private static final long serialVersionUID = 1L;

	/**
	 * The active document.
	 */
	private VisDocument active_document;

	/**
	 * The active camera.
	 */
	private Camera active_camera;

	/**
	 * The active interaction.
	 */
	private Interaction active_interaction;

	/**
	 * List of selected objects.
	 */
	private ArrayList<SceneObject> selected_objects;

	/**
	 * The active graphics objects.
	 * Only valid during the main drawing operation.
	 */
	private Graphics graphics;

	/**
	 * CTor.
	 */
	public Canvas(){
		registerEvents();
		// Create default camera.
		this.active_camera = new Camera();
		this.selected_objects = new ArrayList<SceneObject>();
	}

	/**
	 * Register canvas events.
	 */
	protected void registerEvents(){
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addMouseWheelListener(this);
	}

	/**
	 * Get active camera.
	 */
	public Camera getActiveCamera(){
		return active_camera;
	}

	/**
	 * Set the active interacion.
	 * @param ai the new active interaction.
	 */
	public void setActiveInteraction(Interaction ai){
		this.active_interaction = ai;
	}
	
	/**
	 * Select object.
	 * @param obj
	 * @param clear If true, clear current selection.
	 */
	public void selectObject(SceneObject obj, boolean clear){
		if(clear){
			for(int i =0;i<selected_objects.size();i++){
				selected_objects.get(i).setSelected(false);
			}
			selected_objects.clear();
		}
		selected_objects.add(obj);
	}

	/**
	 * Set the active document.
	 * @param doc
	 */
	public void setActiveDocument(VisDocument doc){
		this.active_document = doc;
	}

	/**
	 * Get the active document.
	 * @return The document.
	 */
	public VisDocument getActiveDocument(){
		return active_document;
	}

	/**
	 * Render background.
	 */
	protected void renderBackground(){
		graphics.setColor(Color.BLACK);

		graphics.drawLine(-100, 0, 100, 0);
		graphics.drawLine(0, -100, 0, 100);
	}

	/**
	 * Render object onto canvas.
	 */
	protected void renderObjects(){
		if(active_document == null)
			return; // nothing to render.

		CanvasRenderArgs args = new CanvasRenderArgs(this, graphics, RenderChannel.FIRST);
		ArrayList<SceneObject> sos = active_document.getObjects();
		for(int i=sos.size()-1;i>=0;i--){
			sos.get(i).render(args);
		}

		args = new CanvasRenderArgs(this, graphics, RenderChannel.PREOBJECT);
		for(int i=sos.size()-1;i>=0;i--){
			sos.get(i).render(args);
		}

		args = new CanvasRenderArgs(this, graphics, RenderChannel.OBJECT);
		for(int i=sos.size()-1;i>=0;i--){
			sos.get(i).render(args);
		}

		args = new CanvasRenderArgs(this, graphics, RenderChannel.POSTOBJECT);
		for(int i=sos.size()-1;i>=0;i--){
			sos.get(i).render(args);
		}
	}

	/**
	 * Render canvas.
	 */
	public void render(){
		// Clear screen.

		Graphics2D g2 = (Graphics2D)graphics;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		graphics.setColor(Color.GRAY);
		graphics.fillRect(0, 0, this.getWidth(), this.getHeight());

		active_camera.push(graphics);
		renderBackground();
		renderObjects();
		active_camera.pop(graphics);
	}


	@Override
	public void paint(Graphics g){
		this.graphics = g;
		render();
		this.graphics = null; // Invalidate graphics object.
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(active_document != null){
			Point2D.Float pp = active_camera.getPoC(e.getPoint());
			SceneObject so = active_document.getObjectAtPoint(pp.x, pp.y);
			if(so != null){
				// Check for mouse interaction.
				if(so.onMouseDown(new CanvasMouseEventArgs(this, e, active_camera.getPoC(e.getPoint())))) // Object handles mouse event.
					return;

				// Check if object is selectable.
				if(so.isSelectable()){
					if(!so.getSelected())
						selectObject(so, true);

					active_interaction = new DragObjectInteraction(this, selected_objects, e.getPoint());
					return;
				}
			}
		}

		// Canvas interaction.
		active_interaction = new CanvasInteraction(this, e.getPoint());
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(active_interaction != null){
			InteractionMouseEventArgs args = new InteractionMouseEventArgs(this, e);
			active_interaction.onMouseMove(args);
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent arg0) {
		float az = active_camera.getZoom();

		float add_z = 0;
		float mul = 0;
		if(arg0.getWheelRotation() <0 ){
			add_z = az*1.5f;
			mul = 1.5f-1f;
		}else{
			add_z = az*(1/1.5f);
			mul = -1/3f;
		}

		active_camera.setZoom(add_z);

		Point pt2 = arg0.getPoint();
		Point2D.Float pt = active_camera.getPoC(arg0.getPoint());
		Point2D.Float pos = active_camera.getPosition();

		float dx = (pos.x-pt2.x)*(mul);
		float dy = (pos.y-pt2.y)*(mul);

		active_camera.translate(dx, dy);

		invalidate();
		repaint();
	}

}
