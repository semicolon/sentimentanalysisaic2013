package at.ac.tuwien.infosys.aic13.web.Controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Stopwatch;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import at.ac.tuwien.infosys.aic13.classifier.SVMClassifierBuilder;
import at.ac.tuwien.infosys.aic13.classifier.WekaClassifier;
import at.ac.tuwien.infosys.aic13.classify.SenticClassifier;
import at.ac.tuwien.infosys.aic13.db.DBWrapper;
import at.ac.tuwien.infosys.aic13.db.DBWrapperFactory;
import at.ac.tuwien.infosys.aic13.db.StatusImpl;
import at.ac.tuwien.infosys.aic13.exception.BadRequestException;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.LocalFilter;
import at.ac.tuwien.infosys.aic13.util.ResultObject;
import at.ac.tuwien.infosys.aic13.util.Search;
import at.ac.tuwien.infosys.aic13.web.Preprocessor.Preprocessor;

@Controller
@RequestMapping("api")
public class RestController {
  
	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy '-' HH:mm");

	private static WekaClassifier wekaClassifier;
	
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	 @RequestMapping("getstatus/{id}")
     @ResponseBody
     public Status getStatusFromDatabaseById(@PathVariable String id) throws IOException {
		 return Search.getStatusFromDatabaseById(id); 
     }
	 
	 @RequestMapping("getbyhashtag/{hashtagCount}")
     @ResponseBody
     public List<String> getStatusFromDatabaseByHashtagCount(@PathVariable String hashtagCount) throws IOException {
		 return DBWrapperFactory.getInstance().getStatusFromDatabaseByHashtagCount(Integer.parseInt(hashtagCount), 50);
     }
	 
	 @RequestMapping("getsentiment")
     @ResponseBody
     public ResultObject getSentiment(@RequestParam(value="searchterm") String searchValue, 
    		 						  @RequestParam(value="classifier") String classifier) throws Exception {
		 
	
		
		if(searchValue == null || searchValue.equals(""))
			throw new BadRequestException("search term is needed");
		
		if(classifier == null || classifier.equals(""))
			throw new BadRequestException("classifier is needed (weka, sentic)");

		//Download latest tweets		
		List<Status> tweets = null;
		DBWrapper db = DBWrapperFactory.getInstance();
		try {
			Query query = new Query();
			query.setLang("en");
			query.setQuery(searchValue);
			query.setCount(1000); 
		
			QueryResult result;
			result = TwitterFactory.getSingleton().search(query);
			System.out.println();
			tweets = result.getTweets();
			log.debug("Found " + tweets.size() + " tweets online");
			Iterator<Status> it = tweets.iterator();
			while(it.hasNext())
			{
				Status stat = it.next();
				if(!LocalFilter.useTweet(stat))
				{
					it.remove();
				}
			}
		
			log.debug("Found " + tweets.size() + " valid tweets online");
			//Store latest tweets in db
			for (Status status : tweets)
			{
				System.out.println("Saving: " + status.getText());
				if (status.isRetweet())
					db.save(StatusImpl.fromStatus(status.getRetweetedStatus()), true);
				else
					db.save(StatusImpl.fromStatus(status), true);
			}
		} catch(TwitterException e){
			System.out.println("propably rate limited! "+e.getMessage());
		}
		
		tweets = db.getAllStatusWithText(searchValue, 1000);
		log.debug("Found " + tweets.size() + " tweets total");
		
		Preprocessor preprocessor = new Preprocessor();		
		
		ResultObject ro = new ResultObject();
		ro.searchTerm = searchValue;
		
		if(classifier.equals("weka")){

			WekaClassifier wc = SVMClassifierBuilder.prepareClassifier();	
			Stopwatch classifierStopwatch = new Stopwatch().start();
			Mean meanWeka = new Mean();

			for(int i = 0; i < tweets.size(); i++)
			{
				Status status = tweets.get(i);

				int wekaResult = wc.classify(status.getText(), preprocessor);
				meanWeka.increment(wekaResult);
			}
			
			classifierStopwatch.stop();
			
			System.out.println(tweets.size() + " tweets analysed in " + classifierStopwatch);
			System.out.println(String.format(Locale.ENGLISH, "Avg (weka): %.3f", meanWeka.getResult()));
			
			ro.classifier = classifier;
			ro.val = meanWeka.getResult();
			ro.numTweets = tweets.size();
			ro.time = classifierStopwatch.elapsed(TimeUnit.MILLISECONDS);
			
		}

		else if(classifier.equals("sentic")) {

			double[][] sentic2Results = new double[5][tweets.size()];
			Stopwatch classifierStopwatch = new Stopwatch().start();
			for(int i = 0; i < tweets.size(); i++)
			{
				Status status = tweets.get(i);
				float[] sentic2Result = SenticClassifier.classify((status.getText()));
				for(int j = 0; j < 5; j++)
				{
					sentic2Results[j][i] = sentic2Result[j];
				}
			}
			Mean avg = new Mean();
			float[] senticResult = new float[]{
					(float)avg.evaluate(sentic2Results[0]),
					(float)avg.evaluate(sentic2Results[1]),
					(float)avg.evaluate(sentic2Results[2]),
					(float)avg.evaluate(sentic2Results[3]),
					(float)avg.evaluate(sentic2Results[4]),
			};
			float res = 0f;
			for(int i = 0; i < senticResult.length; i++){
				res += senticResult[i];
			}
			res /= senticResult.length;
			//Now res is between -1 and 1
			res += 1;
			res /= 2; 
			//Now res is between 0 and 1

			System.out.printf("Avg (Sentic2): %.3f\n",res);
			
			classifierStopwatch.stop();
			
			ro.classifier = classifier;
			ro.val = res;
			ro.numTweets = tweets.size();
			ro.time = classifierStopwatch.elapsed(TimeUnit.MILLISECONDS);
		}

		return ro; 
     }
}
