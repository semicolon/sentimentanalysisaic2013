package at.ac.tuwien.infosys.aic13.analyze.html;

import java.util.ArrayList;

/**
 * Text element.
 */
public class TextElement extends Element{

	private ArrayList<TextToken> content;
	
	/**
	 * Ctor.
	 */
	public TextElement(){
		this.content = new ArrayList<TextToken>();
	}
	
	public void append(TextElement te){
		content.addAll(te.getContent());
	}
	
	/**
	 * Add plain text to content.
	 * @param text
	 */
	public void addText(String text){
		content.add(new TextToken(text));
	}
	
	/**
	 * Add text with link to content.
	 * @param text
	 * @param link
	 */
	public void addLink(String text, String link){
		content.add(new LinkToken(text, link));
	}
	
	/**
	 * Get text content.
	 * @return
	 */
	public ArrayList<TextToken> getContent(){
		return content;
	}
	
	@Override
	public String toString(){
		String res = "";
		for(int i=0;i<content.size();i++){
			res+=content.get(i).toString();
		}
		return res;
	}

	@Override
	public ArrayList<LinkToken> getAllLinks() {
		ArrayList<LinkToken> lt = new ArrayList<LinkToken>();
		for(int i=0;i<content.size();i++)
			if(content.get(i) instanceof LinkToken)
				lt.add((LinkToken)content.get(i));
		return lt;
	}

	@Override
	public void flatten(ArrayList<TextElement> list) {
		list.add(this);
	}
	
}
