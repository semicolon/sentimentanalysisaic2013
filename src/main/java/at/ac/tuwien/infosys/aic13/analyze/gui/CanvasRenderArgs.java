package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.Graphics;

/**
 * Canvas render arguments.
 */
public class CanvasRenderArgs {

	/**
	 * The canvas.
	 */
	public Canvas canvas;
	
	/**
	 * The active graphics object.
	 */
	public Graphics graphics;
	
	public Canvas.RenderChannel channel;
	
	/**
	 * Ctor.
	 * @param canvas
	 */
	public CanvasRenderArgs(Canvas canvas, Graphics graphics, Canvas.RenderChannel channel){
		this.canvas = canvas;
		this.graphics = graphics; 
		this.channel = channel;
	}
	
}
