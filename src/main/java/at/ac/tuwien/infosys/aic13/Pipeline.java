package at.ac.tuwien.infosys.aic13;

public class Pipeline {

	//////
	//INIT
	//////
	
		//Create dictionaries by crawling 
		//Create database with prefetched tweets by using twitterAPI+ObjectDB
			//Werbung, News, Spam vor dem Speichern filtern
		
		//EITHER
			//Weka: Train classifier auf Basis ganzer Tweets
				//Trainingsfile (attributes (kommt vor/kommt nicht vor) und data (tweet + bewertung)) erstellen
				//Classifier trainieren
				//Challenge: ganzen Tweet aufgrund eines einzelnen Attributes als schlecht bewerten??
		//OR
			//Use file of already classified words (eg. SentiWord).
	
	
	//////
	//RUN
	//////
	
		//Customer enters search term
			
		//Receive relevant tweets for search term
			//Search in ObjectDB
			//Search in latest tweets via TwitterAPI
		
		//FOR EACH tweet
		
			//Preprocess

				//Stemming, Stopword Removal (optional weil graph reduction das schon kann)
				//Reduction of relevant words to synonyms (graph reduction)
					//Trainingsset muss auf genau diese Worte angepasst sein
				
	
			//EITHER Classify tweet based on relevant words using Weka
			//OR Classify with help of wordfile
				
		//Aggregate the scores from each tweet (mean)
			
		
}
