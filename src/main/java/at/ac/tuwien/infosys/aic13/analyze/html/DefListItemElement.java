package at.ac.tuwien.infosys.aic13.analyze.html;

import java.util.ArrayList;

/**
 * List item element.
 */
public class DefListItemElement extends Element{

	private boolean declaration;
	
	/**
	 * Content of the item.
	 */
	private ArrayList<Element> content;
	
	/**
	 * Create new list item.
	 */
	public DefListItemElement(boolean declaration) {
		if(declaration)
			this.name = "dt";
		else
			this.name = "dd";
		this.declaration = declaration;
		content = new ArrayList<Element>();
	}
	
	public boolean isDeclaration(){
		return declaration;
	}
	
	public void setDeclaration(boolean declaration){
		this.declaration = declaration;
	}
	
	/**
	 * Get item content.
	 * @return
	 */
	public ArrayList<Element> getContent(){
		return content;
	}
	
	/**
	 * Add content to list.
	 * @param cont
	 */
	public void addContent(Element cont){
		content.add(cont);
	}
	
	public void addContent(ArrayList<Element> conts){
		content.addAll(conts);
	}
	
	@Override
	public String toString(){
		String res ="";
		if(declaration)
			res="[dec]";
		else
			res="[def]";
		for(int i=0;i<content.size();i++){
			res+=content.get(i).toString();
		}
		return res;
	}

	@Override
	public ArrayList<LinkToken> getAllLinks() {
		ArrayList<LinkToken> lt = new ArrayList<LinkToken>();
		for(int i=0;i<content.size();i++)
			lt.addAll(content.get(i).getAllLinks());
		return lt;
	}

	@Override
	public void flatten(ArrayList<TextElement> list) {
		for(int i=0;i<content.size();i++)
			content.get(i).flatten(list);
	}

}
