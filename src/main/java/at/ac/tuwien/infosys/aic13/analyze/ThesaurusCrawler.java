package at.ac.tuwien.infosys.aic13.analyze;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import at.ac.tuwien.infosys.aic13.analyze.html.Element;
import at.ac.tuwien.infosys.aic13.analyze.html.HtmlAnalyzer;
import at.ac.tuwien.infosys.aic13.analyze.html.ListElement;
import at.ac.tuwien.infosys.aic13.analyze.html.ListItemElement;
import at.ac.tuwien.infosys.aic13.analyze.html.TextElement;
import at.ac.tuwien.infosys.aic13.analyze.html.TextToken;

/**
 * Wikipedia crawler.
 */
public class ThesaurusCrawler {

	public static String url = "http://thesaurus.com/browse/";

	private HtmlAnalyzer analyzer;

	/**
	 * Search term.
	 * @param term The term to search for.
	 * @return
	 */
	public SynonymData searchTerm(String term){
		URL search_url = null;
		Response resp = null;
		try {
			search_url = new URL(url+term);
			HttpURLConnection con = (HttpURLConnection)search_url.openConnection();
			con.setRequestProperty("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.66 Safari/537.36");
			resp = getContent(con); 
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		}

		if(resp != null){
			if(resp.isOk() && resp.msg != null){
				return extractData(term, resp.msg);
			}else{
				/* Error has occured, check error stream */
				if(resp.error_msg != null){
					analyzer = new HtmlAnalyzer();

					HtmlCleaner html = new HtmlCleaner();
					CleanerProperties props = html.getProperties();
					TagNode node = html.clean(resp.error_msg);

					ArrayList<Object> ls = new ArrayList<Object>();
					ls.add(node);
					TextElement te = analyzer.getPureText(ls);
					String str = te.toString();
					if(str.contains("Wiktionary does not yet have an entry for")){
						return null;
					}
				}
			}
		}
		return null;
	}


	public void traverseList(ListElement ls){
		for(int i=0;i<ls.length();i++){
			ListItemElement li = ls.getItem(i);
			ArrayList<Element> cont = li.getContent();
			for(int j = 0;j<cont.size();j++){
				Element el = cont.get(j);
				if(el instanceof TextElement){
					TextElement tel = (TextElement)el;
					ArrayList<TextToken> token = tel.getContent();
					String first_entry = null;
					for(int k = 0;k<token.size();k++){
						String data = token.get(k).getText();
						if(!data.trim().equals("")){
							first_entry = data.trim();
							break;
						}
					}
					if(first_entry != null){
						System.out.println("syno:"+first_entry);
					}
				}
			}
		}
	}

	/**
	 * Filter data category.
	 * @param dc
	 * @return returns relvancy index.
	 */
	public int filterDC(String dc){
		if(dc == null)
			return 0;
		int rel_dc = 0;
		String[] toks = dc.split("\\;|\\&|\\,");
		// Find relevant entry.
		for(int i=0;i<toks.length;i++){
			String t = toks[i].trim();
			if(t.startsWith("relevant")){
				String[] rel = t.split("relevant-");
				if(rel.length<2)
					continue; // Invalid entry.
				String rel_num = rel[1];
				try{
				rel_dc = Integer.parseInt(rel_num);
				}catch(NumberFormatException e){/* Not a valid number*/}
			}
		}
		return rel_dc;
	}
	
	/**
	 * Extract data from given content.
	 * @param term
	 * @param content
	 * @return
	 */
	public SynonymData extractData(String term, String content){
		analyzer = new HtmlAnalyzer();

		SynonymData syn = new SynonymData();
		syn.term = term;

		HtmlCleaner html = new HtmlCleaner();
		CleanerProperties props = html.getProperties();
		TagNode node = html.clean(content);

		List ls = node.getElementListByAttValue("class", "filters", true, true);
		for(int i=0;i<ls.size();i++){
			TagNode tn = (TagNode)ls.get(i);
			String id = tn.getAttributeByName("id");
			List sds = tn.getElementListByAttValue("class", "synonym-description", true, true);
			if(sds.size() != 0){
				TagNode sd = (TagNode)sds.get(0);
				List l1 = sd.getElementListByName("em", true);
				List l2 = sd.getElementListByName("strong", true);
				if(l1.size() == 1 && l2.size() == 1){
					// Found description.
					TextElement cat = analyzer.getPureText(l1);
					TextElement tt = analyzer.getPureText(l2);
				}
				// Find relevancy list.
				List rls = tn.getElementListByAttValue("class", "relevancy-list", true, true);
				if(rls.size() != 0){
					TagNode rl = (TagNode)rls.get(0);

					List items = rl.getElementListByName("li", true);
					for(int j = 0;j<items.size();j++){
						TagNode item = (TagNode)items.get(j);
						// Get link and first span.
						List ll = item.getElementListByName("a", true);
						if(ll.size() != 0){
							TagNode link = (TagNode)ll.get(0);
							String dc = link.getAttributeByName("data-category");
							int rel_c = filterDC(dc);
							// Get word.
							List word = link.getElementListByName("span", true);
							if(word.size() != 0){
								TextElement te = analyzer.getPureText(word.get(0));
								syn.addSynonym(te.toString().trim(),rel_c);
							}
						}
					}
					/*
					ArrayList<Element> elems = analyzer.getContent(rl.getAllChildren());
					for(int j=0;j<elems.size();j++){
						Element el = elems.get(0);
						if(el instanceof ListElement){
							traverseList((ListElement)el);
						}
					}*/
				}
			}
		}

		// Get antonyms.
		List anto = node.getElementListByAttValue("class", "container-info antonyms", true, true);
		if(anto.size() != 0){
			TagNode atn = (TagNode)anto.get(0);
			List items = atn.getElementListByName("li", true);
			for(int j = 0;j<items.size();j++){
				TagNode item = (TagNode)items.get(j);
				// Get link and first span.
				List ll = item.getElementListByName("a", true);
				if(ll.size() != 0){
					TagNode link = (TagNode)ll.get(0);
					String dc = link.getAttributeByName("data-category");
					int rel_c = filterDC(dc);
					//System.out.println(dc);
					// Get word.
					List word = link.getElementListByName("span", true);
					if(word.size() != 0){
						TextElement te = analyzer.getPureText(word.get(0));
						syn.addAntonym(te.toString().trim(),rel_c);
					}
				}
			}
		}
		return syn;
	}
	/**
	 * Get content of connection.
	 * @param con The connection.
	 * @return
	 */
	private Response getContent(HttpURLConnection con){

		Response resp = new Response();

		String res = null;
		String err_str = null;

		if(con!=null){
			boolean error = false;
			try {
				// Get reponse code.
				resp.response_code = con.getResponseCode();

				res = "";
				InputStream ins = con.getInputStream();
				int len;
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] buffer = new byte[900000];
				while (-1 != (len = ins.read(buffer))) {
					bos.write(buffer, 0, len);
					String temp = new String(buffer,0,len);
					res+=temp;
				}
			} catch (IOException e) {
				error = true;
				res = null;
			}

			// Read error stream.
			if(error){
				err_str = "";
				try {
					InputStream err = con.getErrorStream();
					int len;
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					byte[] buffer = new byte[4096];
					while (-1 != (len = err.read(buffer))) {
						bos.write(buffer, 0, len);
						String temp = new String(buffer,0,len);
						err_str+=temp;
					}
				}catch (IOException e) {
				}
			}
		}
		resp.msg = res;
		resp.error_msg = err_str;
		return resp;
	}

}
