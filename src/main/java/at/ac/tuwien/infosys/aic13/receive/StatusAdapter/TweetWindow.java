package at.ac.tuwien.infosys.aic13.receive.StatusAdapter;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import twitter4j.Status;
import twitter4j.StatusAdapter;

public class TweetWindow extends StatusAdapter implements AutoCloseable
{

	private long counter = 0;
	private JFrame frame;

	public TweetWindow(final Object notifier)
	{
		frame = new JFrame("Nothing yet...");
		frame.setAlwaysOnTop(true);
		frame.setSize(new Dimension(800, 00));
		// frame.setType(Type.UTILITY);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
		
		frame.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosed(WindowEvent _)
			{
				synchronized (notifier)
				{
					notifier.notify();
				}
			}
		});
	}

	@Override
	public void onStatus(Status status)
	{
		if (!LocalFilter.useTweet(status))
			return;

		if (status.isRetweet())
		{
			status = status.getRetweetedStatus();
		}

		counter++;

		frame.setTitle("Tweet #" + counter + " by " + status.getUser().getScreenName() + ": " + status.getText());
	}

	@Override
	public void close()
	{
		frame.setVisible(false);
		frame.dispose();
	}
}
