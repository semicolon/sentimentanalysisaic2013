package at.ac.tuwien.infosys.aic13.receive.StatusAdapter;

import twitter4j.StallWarning;
import twitter4j.StatusAdapter;
import twitter4j.internal.logging.Logger;


public class DebuggingStatusAdapter extends StatusAdapter {
	private final Logger log = Logger.getLogger(getClass());
	
    @Override
    public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
    	log.warn("TrackLimitationNotice " + numberOfLimitedStatuses);
    }

    @Override
    public void onException(Exception ex) {
    	log.error(null, ex);
    }

    @Override
    public void onStallWarning(StallWarning warning) {
    	log.warn("Stall: " + warning);
    }
}
