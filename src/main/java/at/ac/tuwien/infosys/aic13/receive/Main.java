package at.ac.tuwien.infosys.aic13.receive;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.tools.javac.util.List;

import twitter4j.FilterQuery;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.ClassifyingAdapterSentic;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.ClassifyingAdapterWeka;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.DBStatusAdapter;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.DebuggingStatusAdapter;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.PrintingStatusAdapter;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.TweetWindow;

public class Main
{
	private static final Logger log = LoggerFactory.getLogger(Main.class);

	private static final boolean filter = true;

	private static final String[] languages = new String[] { "en" };
	private static String[] keywords;
	
	private final Object notifier = new Object();
	
	public static ArrayList<String> generateBrandsNameList() throws IOException{
		
		ArrayList<String> list = new ArrayList<String>();
		InputStreamReader is = new InputStreamReader(Main.class.getResourceAsStream("/brandsTop500.txt"));
		
		//brands with too many false positives
		final Set<String> ignoreList = new HashSet<String>();
		ignoreList.addAll(List.from(new String[]{"AIM", "BT", "AA", "TOTAL", "BUDGET", "SHELL", "ORANGE", "BMI", "RSA", "MONSTER", "FIRST"}));
		
		BufferedReader reader = new BufferedReader(is);
	
		String brand;
		while((brand = reader.readLine())!= null && list.size() <= 398){
			if(ignoreList.contains(brand))
				continue;
			
			list.add(brand);
			String s  = brand.replace("'", "")
					.replace(" ", "")
					.replace(".", "") 
					.replace("é", "e").replace("è", "e").replace("ë", "e");
			if(!brand.equals(s)){
				list.add(s);
			}
		}
		return list;
	}

	public static void main(String[] args) throws IOException
	{
		ArrayList<String> list = generateBrandsNameList();
		keywords = new String[list.size()];
		keywords = list.toArray(keywords);

		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e)
		{
			log.warn("Unable to set LookAndFeel", e);
		}

		new Main();
	}

	public Main()
	{
		try (final DBStatusAdapter dbAdapter = new DBStatusAdapter(); 
			 final TweetWindow tweetWindow = new TweetWindow(notifier);)
		{
			TwitterStream twitterStream = new TwitterStreamFactory().getInstance();

			twitterStream.addListener(new PrintingStatusAdapter());
			twitterStream.addListener(new ClassifyingAdapterWeka());
			twitterStream.addListener(new ClassifyingAdapterSentic());
			twitterStream.addListener(new DebuggingStatusAdapter());
			twitterStream.addListener(dbAdapter);
			twitterStream.addListener(tweetWindow);

			if (filter)
			{
				FilterQuery filterQuery = new FilterQuery();
				filterQuery.language(languages);
				filterQuery.track(keywords);

				twitterStream.filter(filterQuery);

				String info = "Tracking " + keywords.length + " keywords: " + keywords[0];
				for (int i = 1; i < keywords.length; i++)
				{
					info += ", " + keywords[i];
				}
				info += "\nin the language(s) " + languages[0];
				for (int i = 1; i < languages.length; i++)
				{
					info += ", " + languages[i];
				}

				log.info(info);
			}
			else
			{
				log.info("Sampling");
				twitterStream.sample();
			}

			log.info("Press enter to stop.");

			ConsoleInputListener.listen(notifier);

			synchronized (notifier)
			{
				try
				{
					notifier.wait();
				} catch (InterruptedException _)
				{}
			}

			log.info("Shutting down...");
			twitterStream.shutdown();
		}
	}
}
