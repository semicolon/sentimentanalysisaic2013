package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

public class CanvasMouseEventArgs {

	/**
	 * The canvas.
	 */
	public Canvas canvas;
	
	/**
	 * The mouse event.
	 */
	public MouseEvent me;
	
	/**
	 * The point on canvas.
	 */
	public Point2D.Float poc;
	
	/**
	 * CTor.
	 */
	public CanvasMouseEventArgs(Canvas canvas, MouseEvent me, Point2D.Float poc){
		this.canvas = canvas;
		this.me = me;
		this.poc = poc;
	}
	
}
