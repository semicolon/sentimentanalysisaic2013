package at.ac.tuwien.infosys.aic13.receive.StatusAdapter;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Status;
import twitter4j.StatusAdapter;
import at.ac.tuwien.infosys.aic13.classifier.SVMClassifierBuilder;
import at.ac.tuwien.infosys.aic13.classifier.WekaClassifier;
import at.ac.tuwien.infosys.aic13.web.Preprocessor.Preprocessor;

public class ClassifyingAdapterWeka extends StatusAdapter
{
	private final Logger log = LoggerFactory.getLogger(getClass());
	private final WekaClassifier classifier;
	private final Preprocessor preprocessor;

	public ClassifyingAdapterWeka()
	{		
		classifier = SVMClassifierBuilder.prepareClassifier();		
		
		try
		{
			preprocessor = new Preprocessor();
		} catch (final IOException e)
		{
			throw new RuntimeException("Unable to init preprocessor", e);
		}
	}

	@Override
	public void onStatus(Status status)
	{
		if (!LocalFilter.useTweet(status))
			return;

		try
		{
			System.out.println("   Weka: " + classifier.classify(status.getText(), preprocessor));
		} catch (Exception e)
		{
			log.error("unable to classify status " + status.getId(), e);
		}
	}
}
