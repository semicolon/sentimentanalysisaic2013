package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.event.MouseEvent;

/**
 * Interaction event args.
 */
public class InteractionMouseEventArgs {

	/**
	 * Mouse event.
	 */
	public MouseEvent me;
	
	/**
	 * The canvas.
	 */
	public Canvas canvas;
	
	/**
	 * Ctor.
	 */
	public InteractionMouseEventArgs(Canvas canvas, MouseEvent me){
		this.canvas = canvas;
		this.me = me;
	}
	
}
