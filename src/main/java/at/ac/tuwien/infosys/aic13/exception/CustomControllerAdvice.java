package at.ac.tuwien.infosys.aic13.exception;

import java.io.IOException;
import java.util.Iterator;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class CustomControllerAdvice {

	private static final Logger logger = LoggerFactory
			.getLogger(CustomControllerAdvice.class);

	/**
	 * Catch IOException and redirect to a 'personal' page.
	 */
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<Object> handleNotFoundException(NotFoundException ex) {

		return new ResponseEntity<Object>(new RestErrorResponse(HttpStatus.NOT_FOUND.value(), ex.getMessage()), HttpStatus.NOT_FOUND);
	}

	
	@ExceptionHandler(BadRequestException.class) 
	public ResponseEntity<Object> handleForbiddenException(BadRequestException ex) {

		return new ResponseEntity<Object>(new RestErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage()), HttpStatus.BAD_REQUEST);
	}


}
