package at.ac.tuwien.infosys.aic13.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.codec.binary.Base64;

import twitter4j.Status;

public class StatusSerializerDeserializer {
	public static String serialize(Status src) {
		try{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(src);
			oos.close();
			return Base64.encodeBase64String(baos.toByteArray());
		}
		catch(IOException e){
			e.printStackTrace();
			return null;
		}
	}
	
	public static Status deserialize(String element){
		try{
			byte[] arr = Base64.decodeBase64(element);
			ObjectInputStream o = new ObjectInputStream(new ByteArrayInputStream(arr));
			Status s = (Status) o.readObject();
			o.close();
			return s;
		}
		catch(IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}