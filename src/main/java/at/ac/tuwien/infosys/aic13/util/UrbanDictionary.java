package at.ac.tuwien.infosys.aic13.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class UrbanDictionary {
	
	private static JsonParser parser = new JsonParser();
	
	public static LinkedList<String> getTags(String searchTerm) throws MalformedURLException, IOException {
		String link = "http://api.urbandictionary.com/v0/define?term="+searchTerm;
		InputStreamReader r = new InputStreamReader((new URL(link)).openStream());
		JsonObject o = parser.parse(r).getAsJsonObject();
		JsonArray arr = o.get("tags").getAsJsonArray();
		LinkedList<String> result = new LinkedList<String>();
		for(int i = 0; i<arr.size(); i++){
			result.add(arr.get(i).getAsString());
		}
		return result;
	}
	
	public static void main(String[] args) throws MalformedURLException, IOException{
		
		String term = "sausagefest";
		
		
		for(String s: getTags(term)){
			System.out.print(s+" ");
		}
	}

}
