package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.Point;
import java.awt.geom.Point2D;

/**
 * Drag graph object interaction.
 */
public class DragGraphObjectInteraction extends Interaction{

	private Point init_pos;
	
	private Canvas canvas;
	
	private SceneObject word;
	
	private Point2D.Float opos;
	
	private WordGraphObject wgo;
	
	/**
	 * CTor.
	 */
	public DragGraphObjectInteraction(Canvas canvas, WordGraphObject obj, SceneObject sco, Point pos){
		this.canvas = canvas;
		this.init_pos = pos;
		this.word = sco;
		this.wgo = obj;
		opos = new Point2D.Float(sco.bounds.x,sco.bounds.y);
	}
	
	@Override
	public void onMouseDown(InteractionMouseEventArgs arg){
		
	}
	
	@Override
	public void onMouseUp(InteractionMouseEventArgs arg){
		
	}
	
	@Override
	public void onMouseMove(InteractionMouseEventArgs arg){
		
		Point np = arg.me.getPoint();

		float iz = 1f/arg.canvas.getActiveCamera().getZoom();
		
		float dx = (np.x-init_pos.x)*iz;
		float dy = (np.y-init_pos.y)*iz;

		word.setTranslation(opos.x+dx, opos.y+dy);
		
		canvas.invalidate();
		canvas.repaint();
	}
	
}
