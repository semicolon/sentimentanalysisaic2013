package at.ac.tuwien.infosys.aic13.db;

import java.util.List;

import twitter4j.Status;

public interface DBWrapper
{
	public void flush();

	public List<? extends Status> getAll();

	public List<Status> getAllStatusWithText(String filter, int limit);

	public List<Status> getRandomStatuses(int limit);

	public List<String> getStatusFromDatabaseByHashtagCount(int hashtagCount, int limit);

//	public void save(List<? extends Status> statusList);

	public void save(StatusImpl status, boolean commitImmediately);

}
