package at.ac.tuwien.infosys.aic13.receive.StatusAdapter;

import twitter4j.Status;
import twitter4j.StatusAdapter;

public class PrintingStatusAdapter extends StatusAdapter
{
	@Override
	public void onStatus(Status status) {
		if(LocalFilter.useTweet(status))
			System.out.println(status.getUser().getScreenName() + ": " + status.getText().trim());
	}
}
