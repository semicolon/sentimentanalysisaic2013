package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Visualizer document.
 */
public class VisDocument {

	/**
	 * The object map.
	 */
	protected HashMap<Long, SceneObject> object_map;
	
	/**
	 * List of objects.
	 */
	protected ArrayList<SceneObject> objects;
	
	/**
	 * CTor.
	 */
	public VisDocument(){
		this.objects = new ArrayList<SceneObject>();
		this.object_map = new HashMap<Long, SceneObject>();
	}
	
	/**
	 * Add object to document.
	 * @param so
	 */
	public void addObject(SceneObject so){
		if(object_map.containsKey(so.getInstanceId())){
			System.out.println("Object with id:"+so.getInstanceId()+" already in document");
			return;
		}
		
		object_map.put(so.getInstanceId(), so);
		objects.add(so);
	}
	
	/**
	 * Get object at given point.
	 * @return
	 */
	public SceneObject getObjectAtPoint(float x, float y){
		for(int i=objects.size()-1;i>=0;i--){
			if(objects.get(i).contains(x, y))
				return objects.get(i); 
		}
		return null;
	}
	
	/**
	 * Get document objects.
	 * @return
	 */
	public ArrayList<SceneObject> getObjects(){
		return objects;
	}
	
	/**
	 * Get object by instance id.
	 * @param inst_id
	 * @return
	 */
	public SceneObject getObject(long inst_id){
		return object_map.get(inst_id);
	}
	
	
}
