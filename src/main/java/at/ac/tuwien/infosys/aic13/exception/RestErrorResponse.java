package at.ac.tuwien.infosys.aic13.exception;


/**
 * Get the users details for the 'personal' page
 */
public class RestErrorResponse {
	
	int status;
	
	String message;
	
	
	public RestErrorResponse(int status, String message) {
		
		this.status = status;
		this.message = message;
	}


	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	
}
