package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import at.ac.tuwien.infosys.aic13.analyze.SynonymData;
import at.ac.tuwien.infosys.aic13.analyze.Term;
import at.ac.tuwien.infosys.aic13.analyze.gui.Canvas.RenderChannel;


/**
 * Word graph object.
 */
public class WordGraphObject extends SceneObject{

	/**
	 * The term.
	 */
	private ArrayList<WordObject> words;

	private HashMap<String,WordObject> words_map;

	private HashMap<String, SynonymData> synonyms;

	/**
	 * CTor.
	 * @param term
	 */
	public WordGraphObject(){
	}

	public ArrayList<WordObject> getTerms(){
		return words;
	}

	/**
	 * Set graphs data.
	 * @param words
	 * @param synonyms
	 */
	public void setData(ArrayList<Term> ws, HashMap<String, SynonymData> synonyms){

		try{
			this.synonyms = synonyms;

			words_map = new HashMap<String, WordObject>();

			Random rand = new Random(2342342);

			// Relayout.
			ArrayList<WordObject> wos = new ArrayList<WordObject>();
			for(int i=0;i<ws.size();i++){
				WordObject wo = new WordObject(ws.get(i));
				wo.setBounds(new Rectangle(20,20,40,20));
				wo.translate(rand.nextFloat()*10000, rand.nextFloat()*10000);
				wo.setInstanceId(1889201);
				wos.add(wo);
				words_map.put(ws.get(i).term, wo);
			}

			this.words = wos;

			// Find placement of words with synonyms.
			for(int j=0;j<ws.size();j++){
				Term t = ws.get(j);
				WordObject wt = words_map.get(t.term);
				SynonymData sd = synonyms.get(t.term);
				if(sd != null){
					// Synonyms found.
					for(int i=0;i<sd.synonyms.size();i++){
						WordObject wo = words_map.get(sd.synonyms.get(i).synonym); 
						if(wo != null){
							wt.addRelated(wo);
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	@Override
	public boolean contains(float x, float y){
		for(int i=0;i<words.size();i++){
			WordObject term = words.get(i);
			boolean b = term.contains(x, y); 
			if(b)
				return true;
		}
		return false;
	}

	@Override
	public boolean onMouseDown(CanvasMouseEventArgs arg){

		for(int i=words.size()-1;i>=0;i--){
			WordObject wo = words.get(i);
			if(wo.contains(arg.poc.x,arg.poc.y)){
				arg.canvas.setActiveInteraction(new DragGraphObjectInteraction(arg.canvas,this, wo, arg.me.getPoint()));
				return true;
			}
		}

		return false;
	}

	@Override
	public void layout(){
	}

	@Override
	public void render(CanvasRenderArgs args){
		Graphics2D g2 = (Graphics2D)args.graphics;

		if(words != null){
			for(int i = 0;i<words.size();i++){
				WordObject term = words.get(i);
				if(term!= null)
					term.render(args);
			}
		}

		if(args.channel == RenderChannel.OBJECT){
			/*
			for(Entry<String,WordObject> e: words.entrySet()){
				WordObject term = e.getValue();
				term.render(args);
			}*/
		}else if(args.channel == RenderChannel.PREOBJECT){
		}
	}

	@Override
	public boolean isSelectable(){
		return false;
	}

}