package at.ac.tuwien.infosys.aic13.db;

public class DBWrapperFactory {
	
	private static DBWrapper instance;
	
	public static synchronized DBWrapper getInstance()
	{
		if(instance == null)
			instance = new ObjectDBWrapper();
		return instance;
	}
}
