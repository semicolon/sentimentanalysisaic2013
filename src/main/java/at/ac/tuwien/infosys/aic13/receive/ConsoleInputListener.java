package at.ac.tuwien.infosys.aic13.receive;

import java.io.IOException;

public class ConsoleInputListener
{
	public static void listen(final Object notifier)
	{
		final Thread t = new Thread(new Runnable() {
			@Override
			public void run()
			{
				try
				{
					System.in.read();
				} catch (IOException _)
				{}

				synchronized (notifier)
				{
					notifier.notify();
				}
			}
		});
		t.setDaemon(true);
		t.start();
	}
}
