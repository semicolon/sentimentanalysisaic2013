package at.ac.tuwien.infosys.aic13.classify;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SenticDatabase {

	private static SenticDatabase instance;
	private HashMap<String, SenticSentiment> words;

	/*http://sentic.net/senticnet-2.0.zip*/
	private SenticDatabase() throws ParserConfigurationException, SAXException, IOException{

		this.words = new HashMap<String, SenticSentiment>();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		InputStream is = getClass().getResourceAsStream("/senticnet2.rdf.xml");
		Document document = builder.parse(is);

		NodeList list = document.getDocumentElement().getChildNodes();
		for(int i = 0; i < list.getLength(); i++){

			Node n = list.item(i);
			if(n.getNodeType() == Node.ELEMENT_NODE && n.getNodeName().equals("rdf:Description")){
				Element desc = (Element) n;
				NodeList elems = desc.getChildNodes();

				SenticSentiment sent = new SenticSentiment();

				for(int j = 0; j < elems.getLength(); j++){

					Node elem = elems.item(j);

					if(elem.getNodeName().equals("semantics")){
						Element d = (Element) elem;
						String[] sp = d.getAttribute("rdf:resource").split("/");
						sent.addSemantic(sp[sp.length-1].replace("_", " "));
					}

					if(elem.getTextContent().trim().equals("")) continue;

					if(elem.getNodeName().equals("text")){
						sent.setWord(elem.getTextContent());
					}
					if(elem.getNodeName().equals("pleasantness")){
						sent.setPleasantness(Float.parseFloat(elem.getTextContent()));
					}
					if(elem.getNodeName().equals("attention")){
						sent.setAttention(Float.parseFloat(elem.getTextContent()));
					}
					if(elem.getNodeName().equals("sensitivity")){
						sent.setSensitivity(Float.parseFloat(elem.getTextContent()));
					}
					if(elem.getNodeName().equals("aptitude")){
						sent.setAptitude(Float.parseFloat(elem.getTextContent()));
					}
					if(elem.getNodeName().equals("polarity")){
						sent.setPolarity(Float.parseFloat(elem.getTextContent()));
					}
				}
				words.put(sent.getWord(), sent);
			}
		}

//		for(Map.Entry<String, SenticSentiment> me: words.entrySet()){
//			
//			String currentWord = me.getKey();
//			
//			for(Map.Entry<String, SenticSentiment> me2: words.entrySet()){
//			
//				SenticSentiment currentSentiment = me2.getValue();
//				
//				if(currentSentiment.getSemantics().contains(currentWord)){
//					me.getValue().addSemantic(currentSentiment.getWord());
//				}
//				
//			}
//			
//		}
//		
	}

	public static SenticDatabase instance(){
		if(instance == null){
			try {
				instance = new SenticDatabase();
			} catch (ParserConfigurationException | SAXException | IOException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	public LinkedList<String> getSemantics(String word) {
		LinkedList<String> result = new LinkedList<String>();
		SenticSentiment sent = words.get(word);
		if(sent != null){
			result.addAll(sent.getSemantics());
		}
		return result;
	}

	public SenticSentiment getWord(String s){
		return words.get(s);
	}

}
