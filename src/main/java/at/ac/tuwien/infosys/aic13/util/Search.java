package at.ac.tuwien.infosys.aic13.util;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import at.ac.tuwien.infosys.aic13.db.DBWrapperFactory;


@Component
public class Search {
	
	public static synchronized Status getStatusFromDatabaseById(String id) throws IOException{
		//return ObjectDBWrapper.getStatus(id);
		//TODO
		throw new UnsupportedOperationException("todo");
	}
	
	public static synchronized List<Status> getAllStatusWithText(String text, int limit) throws IOException
	{
		return DBWrapperFactory.getInstance().getAllStatusWithText(text, limit);
	}
	
//	public static synchronized LinkedList<CustomStatus> queryDatabase(String query, int count) throws IOException{
//		
//		return null;
//		String function = String.format("function (doc) { if (doc.status.text.indexOf('%s') !== -1) { emit(null, doc); }}", query);
//		ViewResults resultAdHoc = CouchDBWrapper.adhoc(function);
//		LinkedList<CustomStatus> statuslist = new LinkedList<CustomStatus>();
//		
//		for(Document d: resultAdHoc.getResults()){
//			CustomStatus cs = new CustomStatus(d.getId());
//			statuslist.add(cs);
//			if(statuslist.size() == count){
//				break;
//			}
//	    }
		
//		ViewResults results = CouchDBWrapper.getAllDocuments();
//		
//		LinkedList<CustomStatus> statuslist = new LinkedList<CustomStatus>();
//		
//		for(Document d: results.getResults()){
//			CustomStatus cs = new CustomStatus(d.getId());
//			if(cs.getText().contains(query)){
//				statuslist.add(cs);
//			}
//			if(statuslist.size() == count){
//				break;
//			}
//	    }
//		return statuslist;
//	}
	
	public static synchronized LinkedList<Status> queryTwitter(String query, int count) throws IOException, TwitterException{
		
		Twitter twitter = TwitterFactory.getSingleton();
		Query q = new Query(query);
		q.setLang("en");
		q.setCount(count);
		QueryResult result = twitter.search(q);
		
		LinkedList<Status> res = new LinkedList<Status>();
		for(Status s: result.getTweets()){
			res.add(s);
		}
		return res;	
	}
}
