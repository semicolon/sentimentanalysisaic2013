package at.ac.tuwien.infosys.aic13.analyze;

/**
 * Synonym or antonym entry.
 */
public class SynoEntry {

	/**
	 * The synonym.
	 */
	public String synonym;
	
	/**
	 * The synonym relevance.
	 * 0: not known.
	 * Higher numbers mean higher relevance.
	 */
	public int relevance;
	
	/**
	 * CTor.
	 */
	public SynoEntry(String term, int relevance){
		this.synonym = term;
		this.relevance = relevance;
	}
	
	public String toString(){
		return synonym + "("+relevance+")";
	}
	
}
