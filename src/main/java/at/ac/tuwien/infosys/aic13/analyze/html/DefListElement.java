package at.ac.tuwien.infosys.aic13.analyze.html;

import java.util.ArrayList;

/**
 * Definition List element.
 */
public class DefListElement extends Element{

	/**
	 * List items.
	 */
	private ArrayList<DefListItemElement> items;
	
	/**
	 * Create a new list element.
	 * @param ordered
	 */
	public DefListElement() {
		this.name = "dl";
		items = new ArrayList<DefListItemElement>();
	}
	
	/**
	 * Get list item at index.
	 * @return
	 */
	public DefListItemElement getItem(int index){
		return items.get(index);
	}
	
	
	public void addItem(DefListItemElement item){
		items.add(item);
	}
	
	/**
	 * Get list length.
	 * @return The length.
	 */
	public int length(){
		return items.size();
	}
	
	@Override
	public String toString(){
		String res = "";
		for(int i=0;i<items.size();i++){
			if(i!=0)
				res+="\n";
			res+="["+i+"]:"+items.get(i).toString();
		}
		return res;
	}

	@Override
	public ArrayList<LinkToken> getAllLinks() {
		ArrayList<LinkToken> lt = new ArrayList<LinkToken>();
		for(int i=0;i<items.size();i++)
			lt.addAll(items.get(i).getAllLinks());
		return lt;
	}

	@Override
	public void flatten(ArrayList<TextElement> list) {
		for(int i=0;i<items.size();i++)
			items.get(i).flatten(list);
	}
	
}
