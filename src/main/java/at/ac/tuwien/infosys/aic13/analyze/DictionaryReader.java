package at.ac.tuwien.infosys.aic13.analyze;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Dictionary reader.
 */
public class DictionaryReader {

	public static HashMap<String, Term> dictionary;
	
	
	public DictionaryReader(){
	}
	
	
	public static String[] readWL5k(InputStream is){
		ArrayList<String> wws = new ArrayList<String>();
		dictionary = new HashMap<String, Term>();
		int plural_c = 0;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while((line = br.readLine())!=null){
				line = line.trim();
				wws.add(line);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String[] rr = new String[wws.size()];
		wws.toArray(rr);
		return rr;
	}
	
	/**
	 * Read dictionary from path.
	 * @param path The path.
	 */
	public static void read(String path){
		
		dictionary = new HashMap<String, Term>();
		int plural_c = 0;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			String line = null;
			while((line = br.readLine())!=null){
				String[] ts = line.split("\\|");
				String tokens[] = ts[0].split("\t");
				if(tokens.length <2)
					continue;
				// System.out.println(tokens[0]+"::"+tokens[1]);
				long rt = parseWordType(tokens[0],tokens[1]);
				Term t = new Term(tokens[0], rt);
				if((t.tClass &Term.gPlural)!=0){
					continue;
				}
				dictionary.put(tokens[0], t);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Parse word type.
	 */
	public static long parseWordType(String word, String type){
		
		long rt = 0;
		
		for(int i=0;i<type.length();i++){
			char c = type.charAt(i);
			// N	Noun
			if(c == 'N'){rt|=Term.gNoun;}
			// P	Plural
			else if(c == 'p'){rt|=Term.gPlural;}
			// h	Noun Phrase
			else if(c == 'h'){rt|=Term.gNoun;}
			// V	Verb (usu participle)
			else if(c == 'V'){rt|=Term.gVerb;}
			// t	Verb (transitive)
			else if(c == 't'){rt|=Term.gVerb;}
			// i	Verb (intransitive)
			else if(c == 'i'){rt|=Term.gVerb;}
			// A	Adjective
			else if(c == 'A'){rt|=Term.gAdjective;}
			// v	Adverb
			else if(c == 'v'){rt|=Term.gAdverb;}
			// C	Conjunction
			else if(c == 'C'){rt|=Term.gConjunction;}
			// P	Preposition
			else if(c == 'P'){rt|=Term.gPreposition;}
			// !	Interjection
			else if(c == '!'){rt|=Term.gInterjection;}
			// r	Pronoun
			else if(c == 'r'){rt|=Term.gPronoun;}
			// D	Definite Article
			else if(c == 'D'){}
			// I	Indefinite Article
			else if(c == 'I'){}
			// o	Nominative
			else if(c == 'o'){}
			else{
				System.out.println("invalid type("+c+") word:\""+word+"\"");
			}
		}
		return rt;
	}
	
}
