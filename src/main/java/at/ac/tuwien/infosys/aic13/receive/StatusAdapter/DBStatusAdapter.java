package at.ac.tuwien.infosys.aic13.receive.StatusAdapter;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import twitter4j.Status;
import twitter4j.StatusAdapter;
import at.ac.tuwien.infosys.aic13.db.DBWrapper;
import at.ac.tuwien.infosys.aic13.db.DBWrapperFactory;
import at.ac.tuwien.infosys.aic13.db.StatusImpl;

public class DBStatusAdapter extends StatusAdapter implements
		AutoCloseable {

	private final Logger log = Logger.getLogger(getClass());
	private static final int BUFFERWARNLEVEL = 30;

	private final BlockingQueue<Status> buffer;
	private final Thread bufferThread;

	public DBStatusAdapter() {
		buffer = new LinkedBlockingQueue<Status>();

		bufferThread = new Thread(new BufferClearRunnable());
		bufferThread.setPriority(Thread.MAX_PRIORITY);
		bufferThread.setName("Bufferthread");
		bufferThread.start();
	}

	@Override
	public void onStatus(Status status) {
		if(!LocalFilter.useTweet(status))
			return;
		
		if (status.isRetweet()) {
			status = status.getRetweetedStatus();
		}

		buffer.add(status);

		int buffersize = buffer.size();
		if (buffersize > BUFFERWARNLEVEL)
			log.warn("Buffer size is " + buffersize);
	}

	private class BufferClearRunnable implements Runnable {
		private final Logger log = Logger.getLogger(getClass());
		private final DBWrapper db = DBWrapperFactory.getInstance();

		@Override
		public void run() {
			log.trace(this.getClass().getSimpleName() + " started");
			while (!Thread.interrupted()) {
				try {
					Status status = buffer.take();
					StatusImpl stat = StatusImpl.fromStatus(status);
					db.save(stat, false);
				} catch (final InterruptedException _) {
					Thread.currentThread().interrupt();
				}
			}
			
			Status status = buffer.poll();
			while (status != null)
			{
				StatusImpl stat = StatusImpl.fromStatus(status);
				db.save(stat, true);
				status = buffer.poll();
			}
			
			db.flush();
			log.trace(this.getClass().getSimpleName() + " stopped");
		}
	}

	@Override
	public void close()
	{
		bufferThread.interrupt();
	}
}
