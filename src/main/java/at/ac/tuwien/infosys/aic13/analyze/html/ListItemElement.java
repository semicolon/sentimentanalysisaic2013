package at.ac.tuwien.infosys.aic13.analyze.html;

import java.util.ArrayList;

/**
 * List item element.
 */
public class ListItemElement extends Element{

	/**
	 * Content of the item.
	 */
	private ArrayList<Element> content;
	
	/**
	 * Create new list item.
	 */
	public ListItemElement() {
		super("li");
		content = new ArrayList<Element>();
	}
	
	/**
	 * Get item content.
	 * @return
	 */
	public ArrayList<Element> getContent(){
		return content;
	}
	
	/**
	 * Add content to list.
	 * @param cont
	 */
	public void addContent(Element cont){
		content.add(cont);
	}
	
	public void addContent(ArrayList<Element> conts){
		content.addAll(conts);
	}
	
	@Override
	public String toString(){
		String res = "";
		for(int i=0;i<content.size();i++){
			//res+=content.get(i)+";";
			res+=content.get(i)+"||";
		}
		return res;
	}

	@Override
	public ArrayList<LinkToken> getAllLinks() {
		ArrayList<LinkToken> lt = new ArrayList<LinkToken>();
		for(int i=0;i<content.size();i++)
			lt.addAll(content.get(i).getAllLinks());
		return lt;
	}

	@Override
	public void flatten(ArrayList<TextElement> list) {
		for(int i=0;i<content.size();i++)
			content.get(i).flatten(list);
	}

}
