package at.ac.tuwien.infosys.aic13.analyze.gui;

import java.awt.Dimension;

import javax.swing.JFrame;

/**
 * Data visualizer.
 */
public class DataVisualizer extends JFrame{

	private static final long serialVersionUID = 1L;

	/**
	 * The canvas.
	 */
	private Canvas canvas;
	
	/**
	 * CTor.
	 */
	public DataVisualizer(){
		this.canvas = new Canvas();
		this.setPreferredSize(new Dimension(800,600));
		this.setSize(800,600);
		this.add(canvas);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public Canvas getCanvas(){
		return canvas;
	}
	
}
