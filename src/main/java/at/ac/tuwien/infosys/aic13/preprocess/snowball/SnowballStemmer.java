
package at.ac.tuwien.infosys.aic13.preprocess.snowball;

public abstract class SnowballStemmer extends SnowballProgram {
    public abstract boolean stem();
};
