package at.ac.tuwien.infosys.aic13.analyze;

import java.util.ArrayList;

public class SynonymData {

	/**
	 * The term.
	 */
	public String term;
	
	/**
	 * Class of the term.
	 */
	public int tClass;
	
	/**
	 * List of synonyms.
	 */
	public ArrayList<SynoEntry> synonyms;
	
	/**
	 * List of antonyms.
	 */
	public ArrayList<SynoEntry> antonyms;

	public SynonymData(){
		synonyms = new ArrayList<SynoEntry>();
		antonyms = new ArrayList<SynoEntry>();
	}
	
	/**
	 * Add synonym. 
	 * @param syno
	 */
	public void addSynonym(String syno, int relevance){
		synonyms.add(new SynoEntry(syno,relevance));
	}
	
	/**
	 * Add antonym.
	 * @param anto
	 */
	public void addAntonym(String anto, int relevance){
		antonyms.add(new SynoEntry(anto,relevance));
	}

	
	public String toLine(){
		String res = term+"| |";
		if(synonyms.size() != 0){
			res+=synonyms.get(0).synonym+"#"+synonyms.get(0).relevance;
			for(int i = 1;i<synonyms.size();i++)
				res+=";"+synonyms.get(i).synonym+"#"+synonyms.get(i).relevance;
		}
		res+="|";
		if(antonyms.size() != 0){
			res+=antonyms.get(0).synonym+"#"+antonyms.get(0).relevance;
			for(int i = 1;i<antonyms.size();i++)
				res+=";"+antonyms.get(i).synonym+"#"+antonyms.get(i).relevance;
		}
		return res;
	}
	
	
	@Override
	public String toString(){
		String str = term;
		str+="\nsynonyms:";
		for(int i=0;i<synonyms.size();i++)
			str+=synonyms.get(i).synonym+"["+synonyms.get(i).relevance+"],";
		str+="\nantonyms:";
		for(int i=0;i<antonyms.size();i++)
			str+=antonyms.get(i).synonym+"["+antonyms.get(i).relevance+"],";
		return str;
	}
	
}
