package at.ac.tuwien.infosys.aic13.analyze;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import at.ac.tuwien.infosys.aic13.analyze.gui.DataVisualizer;
import at.ac.tuwien.infosys.aic13.analyze.gui.VisDocument;
import at.ac.tuwien.infosys.aic13.analyze.gui.WordGraphObject;

public class Main {

	public static void traverseWord(String word,int depth){

		System.out.println("SEARCH:"+word+"["+depth+"]");

		String[] pos_words = new String[]{
				"absolutely",
				"adorable",
				"accepted",
				"acclaimed",
				"accomplish",
				"accomplishment",
				"achievement",
				"action",
				"active",
				"admire",
				"adventure",
				"affirmative",
				"affluent",
				"agree",
				"agreeable",
				"amazing",
				"angelic",
				"appealing",
				"approve",
				"aptitude",
				"attractive",
				"awesome",

				"beaming",
				"beautiful",
				"believe",
				"beneficial",
				"bliss",
				"bountiful",
				"bounty",
				"brave",
				"bravo",
				"brilliant",
				"bubbly",

				"calm",
				"celebrated",
				"certain",
				"champ",
				"champion",
				"charming",
				"cheery",
				"choice",
				"classic",
				"classical",
				"clean",
				"commend",
				"composed",
				"congratulation",
				"constant",
				"cool",
				"courageous",
				"creative",
				"cute",

				"dazzling",
				"delight",
				"delightful",
				"distinguished",
				"divine",

				"earnest",
				"easy",
				"ecstatic",
				"effective",
				"effervescent",
				"efficient",
				"effortless",
				"electrifying",
				"elegant",
				"enchanting",
				"encouraging",
				"endorsed",
				"energetic",
				"energized",
				"engaging",
				"enthusiastic",
				"essential",
				"esteemed",
				"ethical",
				"excellent",
				"exciting",
				"exquisite",

				"fabulous",
				"fair",
				"familiar",
				"famous",
				"fantastic",
				"favorable",
				"fetching",
				"fine",
				"fitting",
				"flourishing",
				"fortunate",
				"free",
				"fresh",
				"friendly",
				"fun",
				"funny",

				"generous",
				"genius",
				"genuine",
				"giving",
				"glamorous",
				"glowing",
				"good",
				"gorgeous",
				"graceful",
				"great",
				"green",
				"grin",
				"growing",

				"handsome",
				"happy",
				"harmonious",
				"healing",
				"healthy",
				"hearty",
				"heavenly",
				"honest",
				"honorable",
				"honored",
				"hug",

				"idea",
				"ideal",
				"imaginative",
				"imagine",
				"impressive",
				"independent",
				"innovate",
				"innovative",
				"instant",
				"instantaneous",
				"instinctive",
				"intuitive",
				"intellectual",
				"intelligent",
				"inventive",

				"jovial",
				"joy",
				"jubilant",

				"keen",
				"kind",
				"knowing",
				"knowledgeable",

				"laugh",
				"legendary",
				"light",
				"learned",
				"lively",
				"lovely",
				"lucid",
				"lucky",
				"luminous",

				"marvelous",
				"masterful",
				"meaningful",
				"merit",
				"meritorious",
				"miraculous",
				"motivating",
				"moving",

				"natural",
				"nice",
				"novel",
				"now",
				"nurturing",
				"nutritious",

				"okay",
				"one-hundred percent",
				"open",
				"optimistic",

				"paradise",
				"perfect",
				"phenomenal",
				"pleasurable",
				"plentiful",
				"pleasant",
				"poised",
				"polished",
				"popular",
				"positive",
				"powerful",
				"prepared",
				"pretty",
				"principled",
				"productive",
				"progress",
				"prominent",
				"protected",
				"proud",

				"quality",
				"quick",
				"quiet",

				"ready",
				"reassuring",
				"refined",
				"refreshing",
				"rejoice",
				"reliable",
				"remarkable",
				"resounding",
				"respected",
				"restored",
				"reward",
				"rewarding",
				"right",
				"robust",

				"safe",
				"satisfactory",
				"secure",
				"seemly",
				"simple",
				"skilled",
				"skillful",
				"smile",
				"soulful",
				"sparkling",
				"special",
				"spirited",
				"spiritual",
				"stirring",
				"stupendous",
				"stunning",
				"success",
				"successful",
				"sunny",
				"super",
				"superb",
				"supporting",
				"surprising",

				"terrific",
				"thorough",
				"thrilling",
				"thriving",
				"tops",
				"tranquil",
				"transforming",
				"transformative",
				"trusting",
				"truthful",

				"unreal",
				"unwavering",
				"up",
				"upbeat",
				"upright",
				"upstanding",

				"valued",
				"vibrant",
				"victorious",
				"victory",
				"vigorous",
				"virtuous",
				"vital",
				"vivacious",

				"wealthy",
				"welcome",
				"well",
				"whole",
				"wholesome",
				"willing",
				"wonderful",
				"wondrous",
				"worthy",
				"wow",

				"yes",
				"yummy",

				"zeal",
				"zealous",
		};


		String[] neg_words = new String[]{
				"abysmal",
				"adverse",
				"alarming",
				"angry",
				"annoy",
				"anxious",
				"apathy",
				"appalling",
				"atrocious",
				"awful",

				"bad",
				"banal",
				"barbed",
				"belligerent",
				"bemoan",
				"beneath",
				"boring",
				"broken",

				"callous",
				"can't",
				"clumsy",
				"coarse",
				"cold",
				"cold-hearted",
				"collapse",
				"confused",
				"contradictory",
				"contrary",
				"corrosive",
				"corrupt",
				"crazy",
				"creepy",
				"criminal",
				"cruel",
				"cry",
				"cutting",

				"dead",
				"decaying",
				"damage",
				"damaging",
				"dastardly",
				"deplorable",
				"depressed",
				"deprived",
				"deformed",
				"deny",
				"despicable",
				"detrimental",
				"dirty",
				"disease",
				"disgusting",
				"disheveled",
				"dishonest",
				"dishonorable",
				"dismal",
				"distress",
				"don't",
				"dreadful",
				"dreary",

				"enraged",
				"eroding",
				"evil",

				"fail",
				"faulty",
				"fear",
				"feeble",
				"fight",
				"filthy",
				"foul",
				"frighten",
				"frightful",

				"gawky",
				"ghastly",
				"grave",
				"greed",
				"grim",
				"grimace",
				"gross",
				"grotesque",
				"gruesome",
				"guilty",

				"haggard",
				"hard",
				"hard-hearted",
				"harmful",
				"hate",
				"hideous",
				"homely",
				"horrendous",
				"horrible",
				"hostile",
				"hurt",
				"hurtful",

				"icky",
				"ignore",
				"ignorant",
				"ill",
				"immature",
				"imperfect",
				"impossible",
				"inane",
				"inelegant",
				"infernal",
				"injure",
				"injurious",
				"insane",
				"insidious",
				"insipid",

				"jealous",
				"junky",

				"lose",
				"lousy",
				"lumpy",

				"malicious",
				"mean",
				"menacing",
				"messy",
				"misshapen",
				"missing",
				"misunderstood",
				"moan",
				"moldy",
				"monstrous",

				"naive",
				"nasty",
				"naughty",
				"negate",
				"negative",
				"never",
				"no",
				"nobody",
				"nondescript",
				"nonsense",
				"not",
				"noxious",

				"objectionable",
				"odious",
				"offensive",
				"old",
				"oppressive",

				"pain",
				"perturb",
				"pessimistic",
				"petty",
				"plain",
				"poisonous",
				"poor",
				"prejudice",

				"questionable",
				"quirky",
				"quit",

				"reject",
				"renege",
				"repellant",
				"reptilian",
				"repulsive",
				"repugnant",
				"revenge",
				"revolting",
				"rocky",
				"rotten",
				"rude",
				"ruthless",

				"sad",
				"savage",
				"scare",
				"scary",
				"scream",
				"severe",
				"shoddy",
				"shocking",
				"sick",
				"sickening",
				"sinister",
				"slimy",
				"smelly",
				"sobbing",
				"sorry",
				"spiteful",
				"sticky",
				"stinky",
				"stormy",
				"stressful",
				"stuck",
				"stupid",
				"substandard",
				"suspect",
				"suspicious",

				"tense",
				"terrible",
				"terrifying",
				"threatening",

				"ugly",
				"undermine",
				"unfair",
				"unfavorable",
				"unhappy",
				"unhealthy",
				"unjust",
				"unlucky",
				"unpleasant",
				"upset",
				"unsatisfactory",
				"unsightly",
				"untoward",
				"unwanted",
				"unwelcome",
				"unwholesome",
				"unwieldy",
				"unwise",
				"upset",

				"vice",
				"vicious",
				"vile",
				"villainous",
				"vindictive",

				"wary",
				"weary",
				"wicked",
				"woeful",
				"worthless",
				"wound",

				"yell",
				"yucky",

				"zero",
		};

		for(int i=0;i<neg_words.length;i++){
			if(word.equals(neg_words[i])){
				System.out.println("FOUND NEG["+neg_words[i]+"]["+depth+"]");
			}
		}

		for(int i=0;i<pos_words.length;i++){
			if(word.equals(pos_words[i])){
				System.out.println("FOUND POS["+pos_words[i]+"]["+depth+"]");
			}
		}

		ArrayList<String> words2S = new ArrayList<String>();
		words2S.add(word);


		WikiCrawler wc = new WikiCrawler();
		for(int i=0;i<words2S.size();i++){
			Term t = wc.searchTerm(words2S.get(i));
			if(t == null){
				System.out.println("term not found");
			}else{
				//System.out.println(t);
			}
		}

		ArrayList<SynoEntry> new_words = new ArrayList<SynoEntry>();

		ThesaurusCrawler tc = new ThesaurusCrawler();
		for(int i=0;i<words2S.size();i++){
			SynonymData t = tc.searchTerm(words2S.get(i));
			if(t == null){
				System.out.println("term not found");
			}else{
				new_words = t.synonyms;
				for(int k=0;k<neg_words.length;k++){
					for(int j=0;j<t.synonyms.size();j++){
						if(t.synonyms.get(j).equals(neg_words[k])){
							System.out.println("FOUND NEG["+neg_words[k]+"]["+depth+"]");
						}
					}
				}
				for(int k=0;k<pos_words.length;k++){
					for(int j=0;j<t.synonyms.size();j++){
						if(t.synonyms.get(j).equals(pos_words[k])){
							System.out.println("FOUND POS["+pos_words[k]+"]["+depth+"]");
						}
					}
				}
				if(depth<2){
					for(int j=0;j<t.synonyms.size();j++){
						traverseWord(t.synonyms.get(j).synonym,depth+1);
					}
				}
			}
		}
		words2S.clear();
		for(int i=0;i<new_words.size();i++)
			words2S.add(new_words.get(i).synonym);
	}

	public static void main(String[] args){

		//traverseWord("faggoting",0);

		VocabularyEngine ve = new VocabularyEngine();
		ve.loadWords(Main.class.getResourceAsStream("/words.txt"));
		ve.loadSynonyms(Main.class.getResourceAsStream("/syns.txt"));
		String[] wws = DictionaryReader.readWL5k(Main.class.getResourceAsStream("/wl_5k.txt"));
		//ve.getWords(wws);

		HashMap<String, HashSet<String>> syno_map = new HashMap<String, HashSet<String>>();

		HashSet<String> ss = new HashSet<String>();

		ArrayList<Term> ws = new ArrayList<Term>();

		int cc = 0;
		// Find words which are not traversed.
		for(int i=0;i<wws.length;i++){
			String ww = wws[i];
			SynonymData sd = ve.synonyms.get(ww);
			if(i>4000 && i<4100){
				if(ve.terms.containsKey(ww)){
					ws.add(ve.terms.get(ww));
				}
			}
			if(sd != null){
				for(int j=0;j<sd.synonyms.size();j++){
					SynoEntry ent  = sd.synonyms.get(j);
					if(ent.relevance > 0){
						HashSet<String> mm = syno_map.get(ent.synonym);

						if(mm == null){
							mm = new HashSet<String>();
							syno_map.put(ent.synonym, mm);
						}else{
						}
						if(!mm.contains(ww))
							mm.add(ww);
					}
				}
			}
		}

		for(Entry<String,HashSet<String>> e : syno_map.entrySet()){
			String w = e.getKey();
			HashSet<String> hs = e.getValue();
			if(hs.size() > 16){
				/*System.out.println(w+" "+hs.size());
				System.out.print("    ");
				for(String ent : hs){
					System.out.print(ent+",");
				}
				System.out.println("");*/
			}else{
				//System.out.println(w+" "+hs.size());
			}
		}

		DataVisualizer dvis = new DataVisualizer();
		VisDocument doc = new VisDocument();

		WordGraphObject wgo = new WordGraphObject();

		wgo.setData(ws, ve.synonyms);

		doc.addObject(wgo);

		dvis.getCanvas().setActiveDocument(doc);

		dvis.setVisible(true);
		
		ArrayList<SynoEntry> list = ve.synonyms.get("flat").synonyms;
		ArrayList<SynoEntry> list1 = new ArrayList<SynoEntry>();
		ArrayList<SynoEntry> list2 = new ArrayList<SynoEntry>();
		ArrayList<SynoEntry> list3 = new ArrayList<SynoEntry>();
		for(SynoEntry s: list){
			if(s.relevance == 1)
				list1.add(s);
			else if(s.relevance == 2){
				list2.add(s);
			}
			else {
				list3.add(s);
			}
		}
		System.out.println(Arrays.toString(list1.toArray()));
		System.out.println(Arrays.toString(list2.toArray()));
		System.out.println(Arrays.toString(list3.toArray()));
		
		System.out.println("done");

		//ve.getWordsFromText("relates configure caller");

		/*
		ve.getWordsFromText("Parametricism makes urbanism and urban order compatible with radically liberal market processes. Large scale city planning receded during the 1970s and since then urbanism as a discourse, discipline, and profession has all but disappeared. The disappearance of urbanism coincides with the crisis of Modernism which can be interpreted as the way in which the crisis of the Fordist planned economy manifested itself within architecture. The bankruptcy of Modernist planning gave way everywhere to the same visual chaos of laissez faire urban expansion under the auspices of stylistic pluralism and the anti-method of collage. However, in the last 10 years innovative urbanism re-emerged under the banner of ‘Parametric Urbanism’, developing the conceptual, formal and computational resources for forging a complex, variegated urban order on the basis of parametric logics that allow it to adapt to dynamic market forces. The global convergence and maturation of Parametricist design research implies that this style of urbanism is ready to go mainstream and impact the global built environment by re-establishing strong urban orders and identities on the basis of its adaptive and evolutionary heuristics."
							+"The 50 core years of architectural modernism (1925 – 1975) were also the golden era of urbanism. During this period the advanced industrial nations urbanized on a massive scale. This was also the era of Fordism, i.e. the era of mechanical mass production and the era of the planned/mixed economy. The state dominated much of the city building via big public investments in infrastructure, social housing, schools, hospitals, universities etc. This made large scale, long term physical planning possible. In Western Europe energy, utilities, broadcasting, railways, as well as many large scale industries had been nationalized. This further enhanced the feasibility of large scale, long term urban planning. The most congenial societal context for modernist urbanism existed within the socialist block with its centrally planned economy. Socialism delivered the logical conclusion of the tendencies of the era, rolling out the technological achievements of the era in a predictable, centrally planned manner, literally delivering the uniform consumption standard made possible by Fordist mass production to every member of society. Consequently, we find the fullest expression of modernist urbanism in the Eastern Block.  Civilization evolved further. The crisis of Fordism, Post-fordist restructuring, the neo-liberal turn in economic policy (privatization, deregulation), and the collapse of the Eastern Block system all coincide with the crisis of modernism in architecture and urbanism. The long accumulated expertise of modern architecture was bankrupt. Postmodernism, Deconstructivism and Folding prepared the ground for Parametricism but did not deliver viable, generalizable strategies for the re-emergence of urbanism. The ongoing, global urban expansion had to proceed without the guidance of the discipline. In the meantime Parametricism developed viable (but yet barely tested) strategies under the banner of ‘Parametric Urbanism’."
							+"Parametricism is ready to be pushed into the mainstream, to finally allow the avant-garde design research of the last 20 years to impact the global built environment, just like Modernism did in the 20th century. A part of this broader mission is the task to push parametric urbanism forward as urbanism’s chance to re-emerge as a viable alternative to the prevailing, spontaneous ‘garbage spill’ mode of urban development. The computational, organisational and compositional resources of parametric urbanism have matured to the point where urban visions can be rendered that compel by projecting the richness of contemporary life processes into a complex variegated urban order that produces urban identities. However, the question arises whether these urban visions are realistic. Can parametric urbanism go mainstream? The question might be posed whether the degree of order that parametric urbanism aspires to can be sustained within the contemporary dynamic and unpredictable societal environment."
							+"This question can be generalized: Is urbanism at all possible in the face of free market dynamism? If we approach this question on the basis of the empirical evidence of the last 30 years  -  that is since the neo-liberal turn in the world economy -  the answer is decisively negative. About 30 years ago modern urbanism vanished. The developmental focus switched to the revitalization and refurbishment of historical centres. When urban expansion returned it took the form of the above mentioned ‘garbage spill’ mode of urban development. In the developing world  - which experienced a massive urbanisation process in the last 30 years  -  we witnessed a mixture of (retrograde) modernism and the same ‘garbage spill’ mode of urban development that was unleashed in the developed world since the neo-liberal turn. This laissez fair mode of urban expansion produced everywhere a disorienting visual chaos, an isotropic ‘white noise’ without the chance to create urban identities. Although this result is dissatisfying, it makes no sense for architects to attack the neo-liberal turn and call for state intervention to rescue urbanism. The unleashing of market forces cannot be reversed. The regime of Fordism/socialism that delivered the living standards of the 1970s cannot deliver 21st century productivity and living standards. The task of architectural discourse is to reinvent and re-adapt architecture and urbanism under progressing societal (socio-economic, technological and political) conditions, rather than demanding the reversal of socio-economic and political developments."
							+"If urbanism was premised on top down planning within a planned/mixed economy, can it continue to exist or re-emerge in the absence planning, within a society that allows for the free play of market forces? In short, the question is: Can there be a free market urbanism, an urbanism without planning? We are moving here from the empirical domain into the domain of theoretical speculation: Can there be a bottom up urbanism that produces urban order, coherence and urban identity without planning?  The thesis of this paper states that this is becoming possible today. What is required here is first of all a hegemonic style, and moreover a style that is able to deliver a legible order via local rules, without imposing an overarching global order."
							+"The discussion of this question might be structured along the lines of architecture’s lead distinction of form versus function, i.e. the question has both a functional and a formal dimension and ultimately concerns the establishment of systematic relations between forms and functions. The functional side of urban order concerns the efficient spatial distribution of society’s divers programs, i.e. the spatial ordering of society’s manifold activities. Modern planning handled this task via land use plans adhering to the principle of mono-functional zoning. Post-modern planning – to the extent that planning still exists -  preferred mixed use zoning. In both cases it is the state authorities that impose the programmatic order of the city. The question arises here whether the state authorities have the relevant information and sufficient information processing capacity to make rational, efficient decisions about the allocation of land resources. The same historical experience that casts doubt on the ability of central planning to deliver an efficient allocation of economic resources in general casts doubt in the particular case of the allocation of land resources. The increasing social complexity and dynamism of Post-fordist network society poses an insurmountable complexity barrier for all central planning efforts.1 This complexity barrier cannot be conquered by ramping up demographic research and economic forecasting ect.2 Instead the assumption promoted here is that the market  - unencumbered by land use constraints -  effects a more efficient allocation, allocating each parcel of land to its most highly valued uses. Perhaps society should allow the market to discover the most productive mix and arrangement of land uses, a distribution that garners synergies3 and maximizes overall value. The market process is an evolutionary process that operates via mutation (trial and error), selection and reproduction. It is self-correcting, self-regulation, leading to a self-organized order. Thus we might presume that the land use allocation and thus the programmatic dimension of the urban and architectural order is to be determined by architecture’s private clients.4"
							+"The precise spatial organisation and morphological articulation of the urban order is the task of architecture."
							+"With the demise of modernism the architectural means of organisation and articulation have proliferated and a pluralism of styles has replaced the coherence of Modernism, including Postmodernism, Late (High Tech) Modernism, Neo-classicism, Deconstructivism, Minimalism etc. This proliferation of architectural means of organisation and articulation was initially a step forward in comparison to the relative poverty of the means of Modernism. The relative monotony of the Modernist city is no longer an adequate expression of the diversity, complexity and dynamism of contemporary, metropolitan society. However, the increase in versatility implied a loss of legible order. This proliferation of styles, together with the liberalisation of planning rules like FAR and height limits etc., produced the garbage spill mode of development described above."
							+"To the extent that the current pluralism of styles contributes to the lack of urban order and identity we might presume that a new hegemonic global style might alleviate this current condition of visual chaos. But this is not all. I would like to argue that neither a hegemonic Postmodernism, nor a hegemonic Deconstructivism could overcome the visual chaos that allows the proliferation of differences to collapse into global sameness (white noise). Both Postmodernism and Deconstructivism operate via collage, i.e. via the unconstrained agglomeration of differences. Only Parametricism has the capacity to combine an increase in complexity with a simultaneous increase in order, via the principles of lawful differentiation and the systematic correlation. As indicated above, my theoretical assumption here is that the free market in land resources produces a global programmatic order with meaningful/efficient distributions and adjacencies. However, this programmatic order is invisible, hidden within the visual chaos generated by the unconstrained pluralism of styles and the collage process of architectural composition. Under the auspices of Parametricism a spatio-morphological visual order that is able to reveal and articulate the underlying programmatic order emerges through the rigorous (computationally operationalized) application of parametric rules that systematically map positional and morphological differences and similitudes onto programmatic differences and similitudes.5 Or where morphological differentiation of the urban fabric is initially just a speculative (non-specific) diversification of the urban offering (e.g. the size differentiation of urban blocks in ZHA’s One North master-plan in Singapore),  it’s differential take up within the development market and finally its differential appropriation in the end user market creates a post facto mapping of programme to form. Whether prospectively or retro-spectively programmed, the navigable formal law of differentiation will make the programmatic differentiation navigable, at least to the extent that the positional and morphological differences make a difference in systematically biasing the final programmatic designation/appropriation."
							+"Master-planning continues to exist on the level of large private land holdings that are gathered via market processes in order to realize/capitalize the potential positional synergies that are inherent in urban renewal/development. The parametric set up of such private master-plans implies that any marketed product mix remains provisional and can be re-calibrated during the design process that coincides with the pre-sale and pre-letting process. As such developments are usually phased, this re-calibration process can continue during construction. Moreover, it is most important to note that the order envisioned within the paradigm of Parametricism does not rely on overarching figures of order that need to be completed in order to become effective, as was the case with Baroque or Beaux Arts master-plans, neither does Parametricist order rely on the uniform repetition of patterns as Modernist urbanism does. In contrast to Baroque or Beaux Arts master-plans, Parametricist compositions are inherently open ended (incomplete) compositions. Their order is relational rather than geometric. They establish order and orientation via the lawful differentiation of fields, via vectors of transformation, as well as via contextual affiliations and subsystem correlations. This neither requires the completion of a figure, nor  - in contrast to Modernist master-plans -  the uniform repetition of a pattern. There are always many (in principle infinitely many) creative ways to transform, to affiliate, to correlate. The paradigm delivers an unprecedented versatility. However, this does not imply that anything goes as in the garbage can mode of agglomeration. The heuristic principles (taboos and dogmas) of Parametricism are to be adhered to at any moment, with respect to any design move or design decision. The design process explores a radically constrained design world, the design world of Parametricism. However, this design world is in itself already an infinitely rich universe of new possibilities. Thus we can afford to exclude some (already explored) regions of the totality of design possibilities, and yet remain super flexible and versatile in our responses to the dynamism of market forces. Only under this condition  - under the condition of a hegemonic architectural paradigm/style -  can the discipline ascertain that a flexible, dynamic, robust and legible urban order (with many unique local identities) has a chance to emerge against the prevailing global default condition of the garbage can mode of urban development. Such a hegemonic style cannot be prescribed top down. It can only emerge bottom up within the autopoiesis (discourse) of architecture. The efforts of many creative hands and voices must converge to make this happen. Such a convergence of creative forces is already happening in the avant-garde. The task is now to push Parametricism into the mainstream, to allow the autopoiesis of architecture to once more impact the global built environment."
							+"A first glimpse of this impact can be witnessed in Singapore, on account of the One North master-plan designed by Zaha Hadid Architects in 2001. This master-plan continues to evolve and adapt as execution proceeds. That a strong urban identity is being forged here should be evident. The scheme draws the divers, pre-existing urban contexts into a new, continuously differentiated order. All incoming roads are taken up into a soft grid that mediates the otherwise incongruent urban directionalities of the context. The contextual affiliations and continuities with the different adjacent urban patterns as well as the (initially non-specific) internal differentiation of the urban fabric result in field logics that can be navigated along legible vectors of transformation. The correlation of block heights with plot sizes turns the urban elevation into a legible graph of the distribution of spatial depths. This complex, variegated order remains open to parametric re-calibrations in response to shifting market demands, without corrupting its relational ordering logic. Its order is robust and inherently open ended, without ever losing its unmistakeable identity. This is a master-plan without an ultimate end state. The particularities of its future states remain unpredictable. But as long as the participating architects adhere to its abstract relational principles and buy into its heuristics of forging continuities and correlations, a strong urban order/identity survives as it evolves.");
		 */
		if(true)
			return;

		String text_to_search = "procrastination";

		String[] words2S = text_to_search.split(" ");

		//String[] words2S = new String[]{"fast"};

		WikiCrawler wc = new WikiCrawler();
		for(int i=0;i<words2S.length;i++){
			Term t = wc.searchTerm(words2S[i]);
			if(t == null){
				System.out.println("term not found");
			}else{
				System.out.println(t);
			}
		}

		ThesaurusCrawler tc = new ThesaurusCrawler();
		for(int i=0;i<words2S.length;i++){
			SynonymData t = tc.searchTerm(words2S[i]);
			if(t == null){
				System.out.println("term not found");
			}else{
				System.out.println(t);
			}
		}

		//DictionaryReader.read("/home/stofde/java_code/TextAnalyze/src/part-of-speech.txt");
		//DictionaryReader.readWL5k("/home/stofde/java_code/TextAnalyze/src/wl_5k.txt");

		/*Analyze an = new Analyze();

		double best_ratio = 0;
		Term best_match = null;
		for(Entry<String,Term> e: DictionaryReader.dictionary.entrySet()){
			String term = e.getKey();
			double dc = an.calcDiceCoff(test, term, 2);
			if(dc > best_ratio){
				best_ratio = dc;
				best_match = e.getValue();
			}
		}

		System.out.println(best_match+" "+best_ratio);*/

		/*Class stemClass = null;
		try {
			stemClass = Class.forName("org.tartarus.snowball.ext.englishStemmer");
			SnowballStemmer stemmer = (SnowballStemmer) stemClass.newInstance();

			stemmer.setCurrent("reaccelerated");
			stemmer.stem();
			String s = stemmer.getCurrent();
			System.out.println(s);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}*/


	}

}
