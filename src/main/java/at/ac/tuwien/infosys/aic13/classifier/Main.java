package at.ac.tuwien.infosys.aic13.classifier;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.time.StopWatch;
import org.apache.commons.math.stat.descriptive.moment.Mean;
import org.apache.commons.math.stat.descriptive.summary.Sum;

import com.google.common.base.Stopwatch;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.TwitterFactory;
import weka.classifiers.Classifier;
import at.ac.tuwien.infosys.aic13.classify.SenticClassifier;
import at.ac.tuwien.infosys.aic13.db.DBWrapper;
import at.ac.tuwien.infosys.aic13.db.DBWrapperFactory;
import at.ac.tuwien.infosys.aic13.db.StatusImpl;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.DBStatusAdapter;
import at.ac.tuwien.infosys.aic13.receive.StatusAdapter.LocalFilter;
import at.ac.tuwien.infosys.aic13.web.Preprocessor.Preprocessor;

public class Main {

	public static void main(String[] args) throws Exception {
		String search = "microsoft";
		
		WekaClassifier wc = SVMClassifierBuilder.prepareClassifier();	
		Preprocessor preprocessor = new Preprocessor();		
		
		Query query = new Query();
		query.setLang("en");
		query.setQuery(search);
		query.setCount(100);
		
		QueryResult result = TwitterFactory.getSingleton().search(query);
		
		System.out.println();
		List<Status> tweets = result.getTweets();
		Iterator<Status> it = tweets.iterator();
		while(it.hasNext())
		{
			if(!LocalFilter.useTweet(it.next()))
				it.remove();
		}
		
		
		DBWrapper db = DBWrapperFactory.getInstance();
		for (Status status : tweets)
		{
			if (status.isRetweet())
				db.save(StatusImpl.fromStatus(status.getRetweetedStatus()), true);
			else
				db.save(StatusImpl.fromStatus(status), true);
		}
		
		
		tweets = db.getAllStatusWithText(search, 1000);
		
		Stopwatch classifierStopwatch = new Stopwatch().start();
		Mean meanWeka = new Mean();
		double[][] sentic2Results = new double[5][tweets.size()];
		
		for(int i = 0; i < tweets.size(); i++)
		{
			Status status = tweets.get(i);
			
			int wekaResult = wc.classify(status.getText(), preprocessor);
			meanWeka.increment(wekaResult);
			
			float[] sentic2Result = SenticClassifier.classify((status.getText()));
			for(int j = 0; j < 5; j++)
			{
				sentic2Results[j][i] = sentic2Result[j];
			}
			
					
			if(i < 5)
			{
				System.out.println("-----------------------------------------------------");
				System.out.println(status.getUser().getScreenName() + ":\n" + status.getText());
				System.out.println(preprocessor.preprocessDocument(status.getText()));
				System.out.println();
				System.out.println("Bag of words (Weka):\t" + wekaResult);
				System.out.println("Bag of words (Sentic2):\t" + SenticClassifier.print(sentic2Result));
			}
		}
		//weigh by followers?
		Mean avg = new Mean();
		float[] senticResult = new float[]{(float)avg.evaluate(sentic2Results[0]),
				(float)avg.evaluate(sentic2Results[1]),
				(float)avg.evaluate(sentic2Results[2]),
				(float)avg.evaluate(sentic2Results[3]),
				(float)avg.evaluate(sentic2Results[4]),
		};
		classifierStopwatch.stop();
		System.out.println("\n----------------------------------------------------------------------------------------------------------\n");
		System.out.println(tweets.size() + " tweets analysed in " + classifierStopwatch);

		System.out.println(String.format(Locale.ENGLISH, "Avg (weka): %.3f", meanWeka.getResult()));	
		
		
		float res = 0f;
		for(int i = 0; i < senticResult.length; i++){
			res += senticResult[i];
		}
		res /= senticResult.length;
		
		System.out.printf("Avg (Sentic2): %.3f\n",res);
	}
}
