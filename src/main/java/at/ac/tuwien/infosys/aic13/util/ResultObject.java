package at.ac.tuwien.infosys.aic13.util;

public class ResultObject {

	public String searchTerm;
	public String classifier;
	public Integer numTweets;
	public Long time;
	public double val;
	
}
