package at.ac.tuwien.infosys.aic13.analyze.html;

import java.util.ArrayList;

/**
 * Paragraph element.
 */
public class ParagraphElement extends Element{

	/**
	 * Paragraph content.
	 */
	private ArrayList<Element> content; 
	
	/**
	 * Ctor.
	 */
	public ParagraphElement(){
		super("p");
		content = new ArrayList<Element>();
	}
	
	/**
	 * Add content to paragraph.
	 * @param elem
	 */
	public void addContent(Element elem){
		this.content.add(elem);
	}
	
	/**
	 * Get content.
	 * @return
	 */
	public ArrayList<Element> getContent(){
		return content;
	}
	
	@Override
	public String toString(){
		String res = "";
		for(int i=0;i<content.size();i++)
			res+=content.get(i).toString();
		return res;
	}

	@Override
	public ArrayList<LinkToken> getAllLinks() {
		ArrayList<LinkToken> lt = new ArrayList<LinkToken>();
		for(int i=0;i<content.size();i++)
			lt.addAll(content.get(i).getAllLinks());
		return lt;
	}

	@Override
	public void flatten(ArrayList<TextElement> list) {
		for(int i=0;i<content.size();i++)
			content.get(i).flatten(list);
	}
	
}
