package at.ac.tuwien.infosys.aic13.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class ThumbnailManager {

	private static final Logger log = LoggerFactory.getLogger(ThumbnailManager.class);
	private static ExecutorService executor = Executors.newCachedThreadPool();

	private class ConnectionCallable implements Callable<String> {
		private String link;
		public ConnectionCallable(String link) {
			this.link = link;
		}
		@Override
		public String call() throws Exception {

			String res = null;
			InputStream is = null;
			try{
				URL url = new URL(link);
				URLConnection con = url.openConnection();
				is = con.getInputStream();

				res = con.getURL().toString();

				if(res.contains("instagram")){
					res += "media?size=t"; 
				} else if(res.contains("twitter")) {
					try {
						String id = res.split("/")[5];
						Twitter twitter = TwitterFactory.getSingleton();
						Status s = twitter.showStatus(Long.valueOf(id));
						res = s.getMediaEntities()[0].getMediaURL()+":thumb";
					} catch (ArrayIndexOutOfBoundsException e) {
//						log.trace("ArrayIndexOutOfBoundsException", e);
					}

				} else { //Add more networks here (facebook, ...)
					res = null;
				}
				is.close();
			} catch (MalformedURLException | FileNotFoundException | TwitterException e){
				//Sometimes instagram links do not work (anymore)
				//or twitter links
				//if(res != null)
//				log.trace("Failure reaching url: "+res);
				res = null;

			} catch (IOException e) {
//				log.trace("IOException", e);
			}
			finally {
				if(is != null)
					is.close();
			}

			return res;
		}
	}

	public LinkedList<ThumbnailDTO> linkToImageUrl(List<ThumbnailDTO> links){

		LinkedList<ThumbnailDTO> res = new LinkedList<ThumbnailDTO>();
		try{
			LinkedList<Future<String>> temp = new LinkedList<Future<String>>();
			for(ThumbnailDTO th: links){
				temp.add(executor.submit(new ConnectionCallable(th.getOriginalLink())));
			}
			int i = 0;
			for(Future<String> fs: temp){
				String s = fs.get();
				if(s != null){
					ThumbnailDTO th = links.get(i);
					th.setResolvedLink(s);
					res.add(th);
				}
				i++;
			}
		}
		catch(Exception e){
			log.error("Exception occured!", e);
		}

		return res;
	}

}
