package at.ac.tuwien.infosys.aic13.web.Preprocessor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

//import util.Options;

/**
 * class that preprocess a tweet
 */
@Component
public class Preprocessor {
    	
	private List<String> stopwords;
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	public Preprocessor() throws IOException
	{
	    //servletContext = webApplicationContext.getServletContext();
	    createListStopwords();
	}

	public String removeUsername(String item) {
		String result = "";
		if(item.startsWith("@"))
			result =  "USERNAME";
		else
			result = item;
		return result;
	}

	public String removeUrl(String item) {
		String result = "";
		if(item.startsWith("http://") || item.startsWith("www."))
			result = "URL";
		else
			result = item;
		return result;
	}
	
	public String replaceNewLines(String item)
	{
		String result = item;
		result = result.replace('\n', ' ');
		result = result.replace('\r', ' ');
		return result;
	}

	public String replaceEmoticons(String item) {
		String result = item;
		result = result.replaceAll(":[)]", "_SMILEHAPPY_");
		result = result.replaceAll(";[)]", "_SMILEHAPPY_");
		result = result.replaceAll(":-[)]", "_SMILEHAPPY_");
		result = result.replaceAll(";-[)]", "_SMILEHAPPY_");
		result = result.replaceAll(":d", "_SMILEHAPPY_");
		result = result.replaceAll(";d", "_SMILEHAPPY_");
		result = result.replaceAll("=[)]", "_SMILEHAPPY_");
		result = result.replaceAll("\\^_\\^", "_SMILEHAPPY_");
		result = result.replaceAll(":[(]", "_SMILESAD_");
		result = result.replaceAll(":-[(]", "_SMILESAD_");
		return result;
	}

	public String removeEmoticons(String item) {
		String result = item;
		result = result.replaceAll(":[)]", "");
		result = result.replaceAll(";[)]", "");
		result = result.replaceAll(":-[)]", "");
		result = result.replaceAll(";-[)]", "");
		result = result.replaceAll(":d", "");
		result = result.replaceAll(";d", "");
		result = result.replaceAll("=[)]", "");
		result = result.replaceAll("\\^_\\^", "");
		result = result.replaceAll(":[(]", "");
		result = result.replaceAll(":-[(]", "");
		return result;
	}
	
	
	public String removeRepeatedCharacters(String item) {
		String result = item;
		result = item.replaceAll("(.)\\1{2,100}", "$1$1");
		return result;
	}

	public String removeSymbols(String item) {
		String result = item;
		result = result.replaceAll("&quot;", "\"");
		result = result.replaceAll("&amp;", "&");
		result = result.replaceAll("&lt;", "<");
		result = result.replaceAll("&gt;", ">");
		return result;
	}

	public String recognizeLaugh(String item) {
		String result = item;
		if(result.contains("haha"))
			result = "STRLAUGH";
		else if(result.contains("ahah")) 
			result = "STRLAUGH";
		return result;
	}
	
	public String removeStopWords(String item) {
		
		String result = item;
		
		if (stopwords.contains(result.toLowerCase()))
			result = "";
			
		return result;
	}

	public String preprocessDocument(String item) {
		String result_fin = "";
		String result = replaceNewLines(item);
		StringTokenizer st1 = new StringTokenizer(result, " ,?![]");
		while (st1.hasMoreTokens()) {
			String str = st1.nextToken();
			
			result = removeUrl(str);
			//if(!_opt.isRemoveEmoticons())
				//result = replaceEmoticons(result);
			//else
			result = removeEmoticons(result);
			result = removeStopWords(result);
			
			StringTokenizer st2 = new StringTokenizer(result, ".:#)(_");
			String tmp = "";
			String tmp2 = "";
			while (st2.hasMoreTokens()) {
				tmp = st2.nextToken();
				tmp = recognizeLaugh(tmp);
				tmp2 = tmp2 + " " + removeUsername(tmp); 
			}
			result = tmp2;
			result = result.replaceAll("lu+v+", "love");
			result = removeRepeatedCharacters(result);
			//result = removeSymbols(result);
			result_fin = result_fin + result;
		}
		return result_fin;
	}

	public List<String> createListStopwords() throws IOException {
		
		log.trace("CREATE WORD LIST");
		List<String> stop = new LinkedList<String>();

		InputStream inputStream = null;
        try {
            inputStream = new ClassPathResource("stop.en.txt").getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                   
			String strLine;
			while ((strLine = bufferedReader.readLine()) != null) {
				
			 	stop.add(strLine);
			}
            
        } finally {
            if (inputStream != null) {
               inputStream.close();
            }
        }
		
        this.stopwords = stop;
        return stop;
	}
}

