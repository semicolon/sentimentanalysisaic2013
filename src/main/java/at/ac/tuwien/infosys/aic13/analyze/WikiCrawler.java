package at.ac.tuwien.infosys.aic13.analyze;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import at.ac.tuwien.infosys.aic13.analyze.html.Element;
import at.ac.tuwien.infosys.aic13.analyze.html.HtmlAnalyzer;
import at.ac.tuwien.infosys.aic13.analyze.html.LinkToken;
import at.ac.tuwien.infosys.aic13.analyze.html.ListElement;
import at.ac.tuwien.infosys.aic13.analyze.html.ListItemElement;
import at.ac.tuwien.infosys.aic13.analyze.html.TextElement;

/**
 * Wikipedia crawler.
 */
public class WikiCrawler {

	public static String url = "http://en.wiktionary.org/wiki/";

	public static String base_word_ulr = "http://en.wiktionary.org/wiki/Appendix:Basic_English_word_list";


	private HtmlAnalyzer analyzer;

	/**
	 * Search term.
	 * @param term The term to search for.
	 * @return
	 */
	public Term searchTerm(String term){
		URL search_url = null;
		Response resp = null;
		try {
			search_url = new URL(url+term);
			HttpURLConnection con = (HttpURLConnection)search_url.openConnection();
			resp = getContent(con); 
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		}

		if(resp != null){
			if(resp.isOk() && resp.msg != null){
				return extractData(term, resp.msg);
			}else{
				/* Error has occured, check error stream */
				if(resp.error_msg != null){
					analyzer = new HtmlAnalyzer();

					HtmlCleaner html = new HtmlCleaner();
					CleanerProperties props = html.getProperties();
					TagNode node = html.clean(resp.error_msg);

					ArrayList<Object> ls = new ArrayList<Object>();
					ls.add(node);
					TextElement te = analyzer.getPureText(ls);
					String str = te.toString();
					if(str.contains("Wiktionary does not yet have an entry for")){
						return null;
					}
				}
			}
		}
		return null;
	}

	public ArrayList<Category> cats;

	public Stack<Category> active_category; 

	public boolean collecting;


	public boolean partitionHtmlRec(TagNode node,int depth, String att_name, String att_value){

		List chs = node.getChildren();
		for(int i=0;i<chs.size();i++){
			boolean add = true;
			boolean follow = true;
			if(chs.get(i) instanceof TagNode){
				TagNode tn = (TagNode)chs.get(i);
				if(tn.getName().startsWith("h")){
					// Get sub element.
					TagNode[] nodes = tn.getElementsByAttValue(att_name, att_value, true, true);
					if(nodes.length != 0){
						String id = nodes[0].getAttributeByName("id");
						int dd = Integer.parseInt(tn.getName().substring(1));
						follow = false;
						if(dd == 2){
							// Language.
							while(active_category.size() != 0)
								active_category.pop();
							Category cat = new Category(id,dd,depth);
							active_category.push(cat);
							cats.add(cat);
							add = false;
						}else{
							// Category.
							while(active_category.size() != 0){
								Category top = active_category.peek();
								if(top.height>=dd)
									active_category.pop();
								else
									break;
							}
							Category cat = new Category(id,dd,depth);
							if(active_category.size() != 0){
								Category par_cat = active_category.peek();
								par_cat.sub_cats.add(cat);
							}else
								cats.add(cat);
							active_category.push(cat);
							add = false;
						}
					}
				}

				if(!follow) // Do not follow branch.
					continue;

				if(!partitionHtmlRec(tn,depth+1, att_name, att_value))
					return false;
			}
			if(add)
				if((active_category.size() != 0) && (depth == active_category.peek().found_height)){
					if(active_category.peek().cat_name.equals("Etymology")){
					}
					active_category.peek().elements.add(chs.get(i));
				}
		}
		return true;
	}


	public void findPattern(String text, String pattern){
		String[] tt = text.split(" ");
		String[] tokens = pattern.split(" ");
		for(int i=0;i<tt.length;i++){
		}
	}

	public String[] tokenizeString(String text){
		text = text.toLowerCase();
		return text.split(" |\\.|\\;|\\(|\\)|\n|\t|\\,|\\:");
	}

	public ArrayList<String> findPattern(String[] tokens, String[] pattern){
		ArrayList<String> res = new ArrayList<String>();
		ArrayList<String> temp = new ArrayList<String>();
		int cc = 0;
		for(int i=0;i<tokens.length;i++){
			String tt = tokens[i];
			if(tt.equals(""))
				continue;
			if(pattern[cc].equals("§§§")){
				temp.add(tokens[i]);
				cc++;
			}else if(tokens[i].equals(pattern[cc])){
				cc++;
			}else{
				cc=0;
			}
			if(cc == pattern.length){
				if(temp.size()!=0)
					res.addAll(temp);
				cc = 0;
			}
		}
		return res;
	}

	public ArrayList<String> getEtymComp(String text){
		ArrayList<String> tt = new ArrayList<String>();
		String[] toks = tokenizeString(text);
		boolean found_comp = false;
		int c = 0;
		for(int i=0;i<toks.length;i++){
			String aa = toks[i].trim();
			if(aa.equals("")){
				tt.clear();
				c=0;
				continue;
			}
			if(c%2 == 1){
				if(aa.startsWith("+")){
					found_comp = true;
					c=0;
				}else{
					if(found_comp)
						return tt;
					tt.clear();
					tt.add(aa);
					c=1;
				}
			}else{
				tt.add(aa);
				c=1;
				if(found_comp)
					return tt;
			}
		}
		if(tt.size() <2)
			tt.clear();
		return tt;
	}

	/**
	 * Get term info found in a category.
	 * @param elems
	 */
	public long getTermProps(ArrayList<Element> elems){
		if(elems.size() == 0)
			return 0; // No information found.

		long props = 0;

		// We are interested in categories the term is associated with.
		// Get all links and check if they refer to some predefined category.
		for(int i=0;i<elems.size();i++){
			Element elem = elems.get(i);
			if(elem instanceof ListElement){
				ListElement le = (ListElement)elem;
				for(int j=0;j<le.length();j++){
					ListItemElement lie = le.getItem(j);
					ArrayList<LinkToken> lts = lie.getAllLinks();
					for(int k=0;k<lts.size();k++){
						LinkToken lt = lts.get(k);
						if(lt.getText().equals("vulgar")){
							props |= Term.cVulgar;
						}else if(lt.getText().equals("offensive")){
							props |= Term.cOffensive;
						}else if(lt.getText().equals("slang")){
							props |= Term.cSlang;
						}
					}
				}
			}
		}
		return props;
	}

	/**
	 * 
	 * @param t
	 * @param cat
	 */
	public void extractDataFromCategory(Term t, Category cat){

		String cat_name = cat.cat_name;
		// Etymology.
		if(cat_name.startsWith("Etymology")){
			//ArrayList<Element> elems = analyzer.getContent(cat.elements);
			ArrayList<Element> elems = analyzer.getContent(cat.elements);

			String text = HtmlAnalyzer.cvtTxtHype(elems);
			ArrayList<String> comp = getEtymComp(text);
			if(comp != null){
				// found composition.
				// Fixup composistion.
				boolean reject = false;
				if(comp.size() != 2)
					reject = true;
				else{
					for(int i=0;i<comp.size();i++){
						if(comp.get(i).trim().equals("")){
							// reject.
							reject = true;
							break;
						}
					}
				}
				if(!reject && t.composition == null)
					t.composition = comp;
			}
		}else if(cat_name.startsWith("Adverb")){
			t.tClass|=Term.gAdverb;

			ArrayList<Element> elems = analyzer.getContent(cat.elements);

			if(elems.size() != 0){

				String str = elems.get(0).toString();
				String[] tt = tokenizeString(str);
				ArrayList<String> comp = findPattern(tt, new String[]{t.term,"comparative","more","§§§","superlative","most","§§§"});
				if(comp.size() == 2){
					// more|most pattern.
					t.comparative = "more";
					t.superlative = "most";
				}else{
					comp = findPattern(tt, new String[]{t.term,"comparative","§§§","superlative","§§§"});
					// er|est pattern.
					if(comp.size() == 2){
						t.comparative = comp.get(0);
						t.superlative = comp.get(1);
					}
				}
			}
		}
		else if(cat_name.startsWith("Verb")){
			t.tClass|=Term.gVerb;

			ArrayList<Element> elems = analyzer.getContent(cat.elements);

			String text = HtmlAnalyzer.cvtTxtHype(elems);
			String[] tt = tokenizeString(text);

			if(elems.size() != 0){

				ArrayList<String> comp = findPattern(tt, new String[]{"present","participle","of","§§§"});
				if(comp.size() != 0)
					t.present_participle_of = comp.get(0);

				comp = findPattern(tt, new String[]{"past","participle","of","§§§"});
				if(comp.size() != 0)
					t.past_participle_of = comp.get(0);


				comp = findPattern(tt, new String[]{"present","participle","§§§"});
				if(comp.size() != 0 && !comp.get(0).trim().equals("of"))
					t.present_participle = comp.get(0);
				comp = findPattern(tt, new String[]{"past","participle","§§§"});
				if(comp.size() != 0 && !comp.get(0).trim().equals("of"))
					t.past_participle = comp.get(0);
			}
		}
		else if(cat_name.startsWith("Noun")){
			t.tClass|=Term.gNoun;

			ArrayList<Element> elems = analyzer.getContent(cat.elements);

			t.properties |= getTermProps(elems);

			if(elems.size() != 0){

				// Check plural patterns.
				String plural = null;
				String plural_of = null;

				String str = elems.get(0).toString();
				String[] tt = tokenizeString(str);
				ArrayList<String> plur = findPattern(tt, new String[]{t.term,"plural","§§§"});
				if(plur.size() != 0){
					plural = plur.get(0);
					t.plural = plural;
				}

				if(plural == null){
					// Check plural of pattern.
					String text = HtmlAnalyzer.cvtTxtHype(elems);
					tt = tokenizeString(text);
					plur = findPattern(tt, new String[]{"plural","form","of","§§§"});
					if(plur.size() != 0){
						// plural of.
						plural_of = plur.get(0);
						t.plural_of = plural_of;
						t.tClass|=Term.gPlural;
					}
				}
			}
		}
		else if(cat_name.startsWith("Adjective")){
			t.tClass|=Term.gAdjective;


			ArrayList<Element> elems = analyzer.getContent(cat.elements);
			// Check for comparation pattern.
			if(elems.size() != 0){

				t.properties |= getTermProps(elems);

				String str = elems.get(0).toString();
				String[] tt = tokenizeString(str);
				ArrayList<String> comp = findPattern(tt, new String[]{t.term,"comparative","more","§§§","superlative","most","§§§"});
				if(comp.size() == 2){
					// more|most pattern.
					t.comparative = "more";
					t.superlative = "most";
				}else{
					comp = findPattern(tt, new String[]{t.term,"comparative","§§§","superlative","§§§"});
					// er|est pattern.
					if(comp.size() == 2){
						t.comparative = comp.get(0);
						t.superlative = comp.get(1);
					}
				}

				String text = HtmlAnalyzer.cvtTxtHype(elems);
				tt = tokenizeString(text);
				ArrayList<String>ss = findPattern(tt, new String[]{"superlative","form","of","§§§"});
				if(ss.size() != 0){
					t.superlative_of = ss.get(0);
				}
				ss = findPattern(tt, new String[]{"comparative","form","of","§§§"});
				if(ss.size() != 0){
					t.comparative_of = ss.get(0);
				}
			}
		}
		else if(cat_name.startsWith("Conjunction")){t.tClass|=Term.gConjunction;}
		else if(cat_name.startsWith("Pronoun")){t.tClass|=Term.gPronoun;}
		else if(cat_name.startsWith("Preposition")){t.tClass|=Term.gPreposition;}
		else if(cat_name.startsWith("Interjection")){t.tClass|=Term.gInterjection;}
		else if(cat_name.startsWith("Initialism")){t.tClass|=Term.gInitialism;}

		// Check for sub-categories.
		for(int j=0;j<cat.sub_cats.size();j++){
			// check if subcategories contain data.
			extractDataFromCategory(t,cat.sub_cats.get(j));
		}
	}


	/**
	 * Extract data from given content.
	 * @param term
	 * @param content
	 * @return
	 */
	public Term extractData(String term, String content){

		analyzer = new HtmlAnalyzer();

		HtmlCleaner html = new HtmlCleaner();
		CleanerProperties props = html.getProperties();
		TagNode node = html.clean(content);

		cats = new ArrayList<Category>();
		active_category = new Stack<>();
		collecting = false;

		partitionHtmlRec(node,0,"class","mw-headline");

		long term_type = 0;

		Category eng = null;
		// Find english category.
		for(int i=0;i<cats.size();i++){

			Category cat = cats.get(i);
			String cat_name = cat.cat_name;
			if(cat_name.equals("English")){
				eng = cat;
				break;
			}
		}

		Term t = new Term(term,term_type);

		if(eng != null){
			for(int i=0;i<eng.sub_cats.size();i++){
				// Find english category.
				extractDataFromCategory(t, eng.sub_cats.get(i));
			}
		}

		return t;
	}


	/**
	 * Get content of connection.
	 * @param con The connection.
	 * @return
	 */
	private Response getContent(HttpURLConnection con){

		Response resp = new Response();

		String res = null;
		String err_str = null;

		if(con!=null){
			boolean error = false;
			try {
				// Get reponse code.
				resp.response_code = con.getResponseCode();

				res = "";
				InputStream ins = con.getInputStream();
				int len;
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] buffer = new byte[4096];
				while (-1 != (len = ins.read(buffer))) {
					bos.write(buffer, 0, len);
					String temp = new String(buffer,0,len);
					res+=temp;
				}
			} catch (IOException e) {
				error = true;
				res = null;
			}

			// Read error stream.
			if(error){
				err_str = "";
				try {
					InputStream err = con.getErrorStream();
					int len;
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					byte[] buffer = new byte[4096];
					while (-1 != (len = err.read(buffer))) {
						bos.write(buffer, 0, len);
						String temp = new String(buffer,0,len);
						err_str+=temp;
					}
				}catch (IOException e) {
				}
			}
		}
		resp.msg = res;
		resp.error_msg = err_str;
		return resp;
	}


	/**
	 * Category.
	 */
	class Category{

		public String cat_name;

		public ArrayList<Object> elements;

		/**
		 * Sub categories.
		 */
		public ArrayList<Category> sub_cats;

		public int height;

		public int found_height;

		public Category(String name, int height, int found_height){
			this.cat_name = name;
			elements = new ArrayList<Object>();
			sub_cats = new ArrayList<Category>();
			this.height = height;
			this.found_height = found_height;
		}

		@Override
		public String toString(){
			return cat_name+" ["+height+"] num_chs:"+sub_cats.size();
		}
	}

}
