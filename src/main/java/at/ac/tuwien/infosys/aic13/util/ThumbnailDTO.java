package at.ac.tuwien.infosys.aic13.util;

public class ThumbnailDTO {

	private final String originalLink;
	private final String userScreenName;
	
	private String resolvedLink;
	
	public ThumbnailDTO(String originalLink, String userScreenName) {
		this.originalLink = originalLink;
		this.userScreenName = userScreenName;
	}
	public String getUserScreenName() {
		return userScreenName;
	}
	public String getResolvedLink() {
		return resolvedLink;
	}
	public void setResolvedLink(String resolvedLink) {
		this.resolvedLink = resolvedLink;
	}
	public String getOriginalLink() {
		return originalLink;
	}
	
	
}
