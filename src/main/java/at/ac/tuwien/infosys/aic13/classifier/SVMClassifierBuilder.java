package at.ac.tuwien.infosys.aic13.classifier;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;

import com.google.common.base.Stopwatch;


public class SVMClassifierBuilder
{
	private static final Logger log = Logger.getLogger(SVMClassifierBuilder.class);

	public static WekaClassifier prepareClassifier()
	{
		//creating a classifier (specifically the training) is slow
		//so we serialize it after creation and try to load it
		
		WekaClassifier classifier;
		
		final Stopwatch stopwatch = new Stopwatch();
		stopwatch.start();
		
		final String classifierLocation = System.getProperty("java.io.tmpdir") + File.separator + "aic13" + File.separator + "wekaclassifier.cache";
		try (InputStream buffer = new BufferedInputStream(new FileInputStream(classifierLocation)); ObjectInput input = new ObjectInputStream(buffer);)
		{
			classifier = (WekaClassifier) input.readObject();
			stopwatch.stop();
			log.debug("Successfully loaded classifier from disk within " + stopwatch);
		} catch (final IOException | ClassNotFoundException _)
		{
			log.debug("Unable to load classifier from disk, creating new one");

			try
			{
				classifier = new WekaClassifier();
				classifier.trainClassifier();
//				classifier.evaluateClassifier();				
				
				stopwatch.stop();
				log.debug("Creating classifier took " + stopwatch);

				// create (sub) temp dir if it doesn't exist
				new File(classifierLocation).getParentFile().mkdir();
				
				try (OutputStream buffer = new BufferedOutputStream(new FileOutputStream(classifierLocation)); ObjectOutput output = new ObjectOutputStream(buffer);)
				{
					output.writeObject(classifier);
				} catch (final IOException e1)
				{
					log.error("unable to serialize classifier", e1);
				}
			} catch (final Exception e)
			{
				throw new RuntimeException("Unable to init classifier", e);
			}
		}


		return classifier;
	}
}
