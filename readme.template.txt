# Basic operation system
Ubuntu 12.04

# Required software
we use a plain version of  Ubuntu 12.04 to evaluate your implementations
therefore you optimally list all programs, libraries, ... that are used for your implementation
the libraries should be listed as follows:

* <Name of software> - <Version number> - <URL to software package respectively apt-get install <name>>


# Run scripts
list all scripts which are required to start the program (including their order)

e.g.:
cd startup
./start-server1 -p0 parameter0
./start-server2 -p1 parameter1


# Setup script
list all scripts which are required to initialize your implementation (fill the databases with data, ...)

e.g.:
cd initialize
./fill-database


# Available features
list all features you implemented and how one can access and trigger these features
if there are any scripts, that can be used to trigger workflows, also mention them here